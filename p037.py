from primes.primelist import *

def truncates(num: int):
    num_str = str(num)
    digits = len(num_str)
    trunks = []

    for j in range(digits):
        # delete the least significant digits first
        trunks.append(int(num_str[:~j]))
        # then deleting from the most significant
        trunks.append(int(num_str[j:]))

    return list(set(trunks))

def intersection(a, b):
    return list(set(a) & set(b))

def check_prime_trunks(num, primelist: list):
    if num not in primelist:
        return False
    else:
        trunks = truncates(num)
        if len(trunks) == len(intersection(trunks, primelist)):
            return True
        else:
            return False



if __name__ == '__main__':
    pl = PrimesList(10**5).list
    count = 0

    pl2 = [p for p in pl if p >= 10]
    for p in pl2:
        if check_prime_trunks(p, pl):
            print(p)





