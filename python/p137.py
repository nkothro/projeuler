import numpy as np
import math
from fractions import Fraction

def eval_fibonacci_generating_function(x):
    # the generating function x + x**2 + 2x**3 + 3x**4 + 5x**5 + ...  
    return x/(1-x-x**2)

def invAF_is_rational(k):
    det = 5*k**2 + 2*k + 1
    if math.floor(math.sqrt(det))**2 == det:
        return True


# z = 74049690
# while z > 0:
#     if invAF_is_rational(z):
#         print(z)
#     z -= 1

def invAF(k):
    det = 5*k**2 + 2*k + 1
    num = int(-(-math.sqrt(det) + k + 1))
    denom = 2*k 
    g = math.gcd(num, denom)
    num /= g
    denom /= g
    return int(num), int(denom)

rationals = [74049690, 10803704, 1576239, 229970, 33552, 4895, 714, 104, 15, 2]

# xvals = []
# for ki in rationals:
#     n, d = invAF(ki)
#     # print(n, d)
#     xvals.append((n,d))

# lst = xvals
# up = lst[1:]
# lo = lst[:-1]

# for j in range(len(up)):
#     a0, a1 = lo[j]
#     b0, b1 = up[j]

#     # print((b0/b1)/(a0/a1))


def fibogen(upper):
    a = 0
    b = 1
    yield a
    while b < upper:
        yield b
        a, b = b, a+b

if __name__ == '__main__':
    import itertools
    upper = 10**8
    l = []
    g = fibogen(upper)
    while g:
        try:
            a,b = tuple(itertools.islice(g, 2))
            l.append(Fraction(a,b))
        except ValueError:
            break

    print(l)

    for ct, k in enumerate(l):
        print(ct, eval_fibonacci_generating_function(k))

