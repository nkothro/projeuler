import numpy as np

from eulerlib import PrimesList, prime_factorization, prime_factors_of

if __name__ == '__main__':
    upper = 10**5
    plist = PrimesList(upper)

    z = np.arange(1,upper+1, dtype=int)
    tab = np.zeros((z.size, 3), dtype=int)

    for j, zj in enumerate(z):
        p = 1
        for k in prime_factors_of(zj):
            p *= k

        tab[j, 0] = zj
        tab[j, 1] = p
    
    print(tab)

