import pathlib as pl
import itertools

import numpy as np

def read_grids(filename:pl.Path):
    with open(filename, 'r') as f:
        txt = f.readlines()
    i = 1      # first grid starts at 1
    stride = 9 # each grid 9 lines long
    
    txt_grid = []
    while i < len(txt):
        sl = slice(i, i+stride)
        grid = [[int(j) for j in k] for k in (s.replace('\n','') for s in txt[sl])]
        txt_grid.append(np.asarray(grid))

        i += 1 + stride
    return txt_grid  

class SudokuGrid:
    def __init__(self, grid:np.ndarray):
        self._given = grid.copy()
        self._working = self._given.copy()
        self._valid_set = {1,2,3,4,5,6,7,8,9}

        self._possible_cell_values = {(i,j):[1,2,3,4,5,6,7,8,9] for i, j in itertools.product(range(9), repeat=2)}
        self._empty_cells = []

        for r, c in itertools.product(range(9), range(9)):
            if (v:= self._given[r,c]) != 0:
                self.__helper_remove_from_possibles(r,c,v)
            else:
                self._empty_cells.append((r,c))

        # print(self._given)
        # print(self.possibles)

    def __helper_remove_from_possibles(self, row, col, value):
        cells = self.cells_in_same_section(row, col)
        cells.extend((row, j) for j in range(9))
        cells.extend((i, col) for i in range(9))

        for key in set(cells):
            if value in self._possible_cell_values[key]:
                self._possible_cell_values[key].remove(value)

    def section_slices(self, section_row, section_col):
        return slice(sr:=3*section_row, sr+3), slice(sc:=3*section_col, sc+3)

    def get_section(self, section_row, section_col):
        sl_r, sl_c = self.section_slices(section_row, section_col)
        return self._working[sl_r, sl_c]

    @staticmethod
    def print_grid(grid):
        g = ''
        for i, row in enumerate(grid):
            r = ''
            if i % 3 == 0:
                g += '|---|---|---|\n'
            for j, cell in enumerate(row):
                if j % 3 == 0:
                    r += '|'
                r += str(int(cell))
            r += '|\n'
            g += r
        return g.replace('0', ' ')
        

    @staticmethod
    def cells_in_same_section(row, col):
        r = SudokuGrid.sxn2sl(row//3)
        c = SudokuGrid.sxn2sl(col//3)
        return [(i,j) for i,j in itertools.product(range(r.start, r.stop), range(c.start, c.stop))]

    @staticmethod
    def sxn2sl(section_idx):
        match section_idx:
            case 0:
                return slice(0,3)
            case 1:
                return slice(3,6)
            case 2:
                return slice(6,9)
    
    @staticmethod
    def idx2sxn(idx):
        if 0 <= idx < 3:
            return 0
        elif 3 <= idx < 6:
            return 1
        elif 6 <= idx < 9:
            return 2


    def get_section_from_row_col(self, row, col):
        v = self.get_section(row//3, col//3)
        return v

    def get_row(self, i):
        return self._working[i, :]
    
    def get_col(self, i):
        return self._working[:,i]
    
    def valid_section(self, section_row, section_col):
        s = self.get_section(section_row, section_col)
        return set(s.flatten()) == self._valid_set

    def valid_cell_value(self, row, col, value):
        if value in self.get_row(row):
            print(self._working)
            print(f'{value=} in {row=}')
            return False
        if value in self.get_col(col):
            # print(f'{value=} in {col=}')
            return False
        if value in self.get_section_from_row_col(row, col):
            # print(f'{value=} in same section as {(row,col)=}')
            return False

        return True

    def possibles(self, row, col):
        return self._possible_cell_values[(row, col)]
        
    def is_solved(self):
        if any((set(self._working[row, :])!=self._valid_set) for row in range(10)):
            return False
        if any((set(self._working[:, col])!=self._valid_set) for col in range(10)):
            return False

        if not all (self.valid_section(i,j) for i,j in itertools.product((0,1,2), repeat=2)):
            return False

        return True

    def solve(self):
        remaining = self._empty_cells
        s = self.__helper_recursive(remaining)
        return s

    def __helper_recursive(self,remaining):

        for r,c in remaining:
            self._working[r,c] = 0
            

        # if len(remaining)<=1:
        #     print(self._working)

        row, col = remaining[0]
        # print(row, col,self.possibles(row, col))
        for val in self.possibles(row, col):
            s = f'test {val} for {(row,col)}'
            # print(s)
            b = self.valid_cell_value(row, col, val)
            print(f'{(row, col)=}={val} {b}')
            if self.valid_cell_value(row, col, val):
                self._working[row,col] = val
                # print(self._working)
                if self.is_solved():
                    return self._working.copy()

                new_remain = remaining[1:]
                self.__helper_recursive(new_remain)
            # else:
            #     st = f'{(row, col)=} can not be {val}'

if __name__ == '__main__':
    f = pl.Path(r'./python/p096_grids.txt')
    grids = read_grids(f)

    s = SudokuGrid(grids[0])
    print(s.print_grid(s._given))
    # print(s._possible_cell_values)
    # solved = s.solve()
    # print(solved)