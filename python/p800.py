import numpy as np
from sympy.functions.elementary.exponential import LambertW

from primes.primelist import PrimesList
import matplotlib.pyplot as plt


# define global variable
K = 800800

def max_pair(q:int, k:int, max_int:int=15704555):
    """
    (p**q) * (q**p) < k**k, return maximum p possible for given q and k
    """

    # know that for q=2, solution is less than 15704554
    x = np.arange(q+1, max_int)
    # c = k*np.log(k)/np.log(q)

    t = np.log(q)*x + q+np.log(x) <= k*np.log(k)

    if not np.any(1-t):
        return None
    
    p = x[t]
    return p[-1]


maxint = 15704555
prm = PrimesList(maxint)
ct = 0


count_primes = prm.list.size
i_lo = 0
i_high = count_primes -1
Ck = K * np.log(K)

while i_high > 1:
    q = prm.list[i_high]
    p = prm.list[0:i_high]

    t = np.log(q)*p + q*np.log(p) <= Ck
    ct += t.sum()
    print(ct)

    i_high -= 1
