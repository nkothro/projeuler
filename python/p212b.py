import bisect
import pathlib as pl
import sqlite3

from itertools import product

import numpy as np

from p212 import lagged_fibo_gen, cuboid_params, Cuboid, range_merge


def slice_sum(cuboids_list: list[Cuboid], slice_shape, x):
    # assumes input list is sorted
    slice_mask = np.zeros(slice_shape, dtype=bool)
    # cidx = bisect.bisect_left(cuboids_list, (x, ))
    cidx = 0

    while cuboids_list[cidx].x0 <= x:
        c = cuboids_list[cidx]
        cx1 = c.x0 + c.dx
        if c.x0 <= x and cx1 > x:
            ysl = slice(c.y0, c.y0 + c.dy)
            zsl = slice(c.z0, c.z0 + c.dz)
            slice_mask[ysl, zsl] = 1
        elif c.x0 < x:
            break

        cidx += 1

    return np.sum(slice_mask)


if __name__ == "__main__":
    # import multiprocessing as mp

    max_cuboid_index = 100
    upper = 10 * (max_cuboid_index + 1)
    s = lagged_fibo_gen(upper)
    s_mod399 = np.mod(s, 399).astype(np.ulonglong)
    s_mod10k = np.mod(s, 10_000).astype(np.ulonglong)

    cuboids = sorted(
        cuboid_params(n, s_mod399=s_mod399, s_mod10k=s_mod10k)
        for n in range(1, max_cuboid_index + 1)
    )
    max_x = max(c.x0 + c.dx for c in cuboids)
    max_y = max(c.y0 + c.dy for c in cuboids)
    max_z = max(c.z0 + c.dz for c in cuboids)

    slice_shape = (max_y, max_z)
    floor = np.zeros(slice_shape)

    for c in cuboids:
        floor[c.x0 : c.x0 + c.dx, c.y0 : c.y0 + c.dy] = 1
    print(np.sum(floor) / np.size(floor))

    nonzero_x, nonzero_y = np.nonzero(floor)
    # for xi, yi in zip(nonzero_x, nonzero_y):
    #     print("hi", xi, yi)

    cubesarr = np.zeros((len(cuboids), 2, 3), dtype=np.uint16)

    for j, c in enumerate(cuboids):

        #  cube, (start/stop), (x/y/z)
        cubesarr[j, 0, 0] = c.x0
        cubesarr[j, 1, 0] = c.x0 + c.dx

        cubesarr[j, 0, 1] = c.y0
        cubesarr[j, 1, 1] = c.y0 + c.dy

        cubesarr[j, 0, 2] = c.z0
        cubesarr[j, 1, 2] = c.z0 + c.dz

    tot = 0
    for j,(xi, yi) in enumerate(zip(nonzero_x, nonzero_y)):
            x_mask = np.logical_and(cubesarr[:, 0, 0] <= xi, xi < cubesarr[:, 1, 0])
            y_mask = np.logical_and(cubesarr[:, 0, 1] <= yi, yi < cubesarr[:, 1, 1])
            mask = np.logical_and(x_mask, y_mask)

            if mask.any():
                # print(xi, yi)
                z_starts = cubesarr[mask, 0, 2]
                z_stops = cubesarr[mask, 1, 2]
                # print(z_starts, z_stops)
                mrgd = range_merge([(int(z0), int(z1)) for z1, z0 in zip(z_stops, z_starts)])
                tot += int(sum(z1 - z0 for z0, z1 in mrgd))
            if j % 10000 == 0:
                print(j/len(nonzero_x))

    print(tot)
    known_correct_answer = 723581599
    print(f'diff between my answer and correct answer: {known_correct_answer - tot}')
