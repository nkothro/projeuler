import numpy as np
from math import comb
import matplotlib.pyplot as plt

def f(a, n):
    """
    (a+1)**n + (a-1)**n can be expanded using binomial theorem
    use rule that (a+b)%n == ((a%n) + (b%n)) % n
    and (a*b) % n = [(a%n) * (b%n)] % n
    """
    k = 0
    s = 0
    a2 = a**2

    # while k <= n:
    #     p = (a**(n-k)) % a2
    #     c = comb(n, k) % a2
    #     s += (p*c) % a2
    #     k += 2
    # return ((2%a2)*(s%a2)) % a2

    term_sub = ((a-1) % a2)**n % a2
    term_add = ((a+1) % a2)**n % a2
    tot = (term_sub % a2) + (term_add % a2)
    return tot % a2

def rmax(a):
    # k = cycle_length(a) 
    # x = []
    # for n in range(k, 2*k + 1):
    #     x.append(f(a, n))
    
    # return max(x)


    # for what value of n is f(a, n) maximized, holding a constant
    z = a % 4

    if z == 0:
        nmax =  (a-2)//2
    elif z == 1:
        nmax = 3 * (a-1) // 2
    elif z == 2:
        nmax = a - 1
    else:
        nmax = (a-1) // 2
        
    return f(a, nmax)


def cycle_length(a):
    """
    Keeping a constant and varying n, values for r(a, n) are periodic.
    This determines the period T
    """
    if a < 3:
        return None
    elif a % 2 == 1:
        return 2*a
    elif a % 4 == 0:
        return a//2
    else:
        return a

# x = []
# for a in range(3, 500):
#     y = []
#     for n in range(a*5):
#         y.append(f(a, n))
#     y = np.asarray(y)
#     idx = np.arange(y.size)[y == y.max()]
#     print(a, idx[0])
#     i = np.diff(idx)
#     # if i.size > 0:
#     #     print(a, i)
#     # else:
#     #     print(a, None)
#     x.append((a, idx[0]))
#     # plt.show()

# x = np.asarray(x)
# print(x)
# plt.plot(x[:,0], x[:,1])
# plt.show(),

def mod_exp_check(x, k, n):
    """
    use of his function has showed that the two are always equal for x, k, n > 0
    """
    norm = (x**k) % n
    p = ((x%n)**k)%n
    return (norm==p, norm,p)

if __name__ == '__main__':
    y=[]
    for a in range(3, 1000):
        y.append(rmax(a))
        print(a, rmax(a))

    # x = np.arange(3, 50)
    # y = np.asarray([rmax(xi) for xi in x])

    # plt.plot(x, y)
    plt.plot(y)
    plt.show()

    print(sum(y))

