import numpy as np

from primes.primelist import PrimesList

def prime_series_sum(plist, jstart, maxnum):
    """

    :param plist: The list of prime numbers
    :param jstart: Index of number to start with
    :param maxnum:
    :return: The largest prime number less than maxnum possibly produced by adding all consecutive numbers in plist
            after and including plist[jstart]
    """
    # when no numbers have yet been considered, the series sums to zero
    lenplist = len(plist)
    sum = 0
    maxprime = None
    maxrange = None
    j = 0
    while sum <= maxnum:
        if jstart + j < lenplist:
            sum += plist[jstart + j]
            if sum in plist and sum <= maxnum:
                maxprime = sum
                maxrange = (jstart, jstart + j)

            j += 1
        else:
            break

    return maxprime, maxrange

def np_running_window_sum(x:np.ndarray, w:int):
    # s = x.cumsum()
    # return s[w:] - s[:-w]
    return np.convolve(x, np.ones(w), 'valid').astype(int)

def longest_consecutive_primes_sum(starting_window_len, upper_bound):
    upper_bound = 10**6
    plist = PrimesList(upper_bound)

    # sum of plist[0] + plist[1] + ... plist[546] is first greater than 10**6
    # 546 is largest window to check

    j = starting_window_len
    while j > 0:
        k = np_running_window_sum(plist.list, j)
        k = k[k < upper_bound]
        for ki in k:
            if plist.is_prime(ki):
                return ki
        j -= 1
        print(j)

    return None

if __name__ == '__main__':
    z = longest_consecutive_primes_sum(550, 10**6)
    print(z)

