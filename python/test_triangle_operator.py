import numpy as np
import itertools

import math

from primes.primelist import PrimesList

def triangle_iterator(M, count=2, start=0):
    z = [start] * count
    final = [M] * count

    while z < final:
        z[-1] += 1
        for j in range(count-1):
            if z[~j] > z[~(j+1)]:
                z[~(j+1)] += 1
                z[~j] = start
            else:
                break
        yield tuple(z)


maxM, maxC = 10,10

ct = 3

z = []
j=0
while len(z) < 8:
    c = math.comb(j, ct) -1
    if c > -1:
        z.append(c)
    j+=1

x = []
for m in range(len(z)):
    x.append(len(list(triangle_iterator(m, count=ct))))

print(', '.join(str(int(k)) for k in z))
print(', '.join(str(int(k)) for k in x))
print(x==z)