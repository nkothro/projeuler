"""
NE diagonal is always (2n+1)**2 for 1 <= n <= 500
NW diagonal is always (2n+1)**2 - (2n-1) for 1 <= n <= 500
SW diagonal is always (2n+1)**2 - 2(2n-1) for 1 <= n <= 500
SE diagonal is always (2n+1)**2 - 3(2n-1) for 1 <= n <= 500

Sum, including center tile is 1 + sum(NE + NW + SW + SE)
  = 1 + sum from 1 to 500 of (4*(2n+1)**2 - 6*(2n-1))
"""

diagsum = 1
for n in range(1, 501):
    diagsum += 4 * (2*n + 1)**2 - 6*(2*n - 1)

print(diagsum)

