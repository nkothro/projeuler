import sympy
import itertools
import multiprocessing 

import sympy.utilities

_letter_map = {}

def construct_general_masked_gcd_matrix(N:int, element_letter='m'):
    d = dict()
    M = sympy.ones(N,N)
    for j in range(1,N):
        for i in range(1,j+1):
            if i != j:
                sy = sympy.Symbol(f'{element_letter}{i}_{j}')
                M[i,j] = sy
                M[j,i] = sy
            else:
                M[i,j] = 0

    return M

def construct_general_sym_diag_0_matrix(N:int, element_letter='m'):
    M = sympy.zeros(N,N)
    for j in range(N):
        for i in range(j+1):
            if i==j:
                M[i,j] = 0
            else:
                sy = sympy.Symbol(f'{element_letter}{i}_{j}')
                M[i,j] = sy
                M[j,i] = sy
    M[0,0] = 1
    return M


def mat_power_by_square(M, n, expand:bool=False):
    # y = sympy.eye(M.shape[0])
    return _matpow2(1, M, n, expand=expand)

def _matpow(y, M, n, dot_prod_sim=False):
    print(M)
    print(f'going into n={n}')
    if n == 0:
        return y
    elif not (n & 1): # n is even
        MM = M.multiply(M, dotprodsimp=dot_prod_sim)
        m = _matpow(y, MM, n>>1)
        print(f'exiting n={n}')
    else: # n is odd
        My = M.multiply(y, dotprodsimp=dot_prod_sim)
        MM = M.multiply(M, dotprodsimp=dot_prod_sim)
        # MM = M * M
        m = _matpow(My, MM, (n-1)>>1)
        print(f'exiting n={n}')

    return m

def _matpow2(y, M, n, expand=False):
    print(f'going into n={n}')
    if n == 0:
        print(f'exiting n={n}')
        return y
    elif not (n & 1): # n is even
        # MM = M * M
        MM = parallelize_matmul(M, M, expand_each_step=False, simplify_each_step=True)
        if expand:
            MM.expand()

        m = _matpow2(y, MM, n>>1)
        print(f'exiting n={n}')
    else: # n is odd
        if isinstance(y, sympy.ImmutableMatrix) or isinstance(y, sympy.Matrix):
            MM = parallelize_matmul(y, M, expand_each_step=False, simplify_each_step=True)
        else:
            My = M * y
        MM = parallelize_matmul(M, M, expand_each_step=False, simplify_each_step=True)
        # MM = M * M
        if expand:
            MM.expand()
            My.expand()
        m = _matpow2(My, MM, (n-1)>>1)
        print(f'exiting n={n}')

    if expand:
        m.expand()

    return m

def sym_mat_pow(M,n, expand:bool=False):
    return _sym_matpow2(1, M, n, expand=expand) 

def _sym_matpow2(y, M, n, expand=False):
    print(f'going into n={n}')
    if n == 0:
        print(f'exiting n={n}')
        return y
    elif not (n & 1): # n is even
        # MM = M * M
        MM = parallelize_symmetric_matmul(M, M, expand_each_step=False, simplify_each_step=True)
        if expand:
            MM.expand()

        m = _sym_matpow2(y, MM, n>>1)
        print(f'exiting n={n}')
    else: # n is odd
        if isinstance(y, sympy.ImmutableMatrix) or isinstance(y, sympy.Matrix):
            print('y is a matrix')
            My = parallelize_symmetric_matmul(y, M, expand_each_step=False, simplify_each_step=True)
        else:
            print(y)
            My = M * y
        MM = parallelize_symmetric_matmul(M, M, expand_each_step=False, simplify_each_step=True)
        # MM = M * M
        if expand:
            MM.expand()
            My.expand()
        m = _sym_matpow2(My, MM, (n-1)>>1)
        print(f'exiting n={n}')

    if expand:
        m.expand()

    return m

def mat_mul_dot(tup):
    Ai, Bj, i,j, expand, simplify = tup
    r = Ai.dot(Bj)
    if expand:
        r.expand()
    elif simplify:
        r.simplify()
    return i, j, r

def parallelize_matmul(A, B, expand_each_step=False, simplify_each_step=True,proc=20, chunksize=1)->sympy.ImmutableMatrix:
    rows = A.shape[0]
    cols = B.shape[1]
    gen = ( (A[i,:], B[:,j], i, j, expand_each_step, simplify_each_step) for i, j in itertools.product(range(rows), range(cols)) )

    R = sympy.zeros(rows, cols)
    with multiprocessing.Pool(processes=proc) as pool:
        for i, j, r in pool.imap_unordered(mat_mul_dot, gen, chunksize=chunksize):
            print(f'  ({i:>2},{j:>2})', end='')
            R[i,j] = r
    print('mat multiply done') 
    return sympy.ImmutableMatrix(R)

def parallelize_symmetric_matmul(A, B, expand_each_step=False, simplify_each_step=False,proc=20, chunksize=1):
    # product of two symmetric matrices
    rows = A.shape[0]
    cols = A.shape[1]

    itr = ((i, j) for i in range(A.shape[0]) for j in range(i+1))
    gen = ((A[i,:], B[:,j], i, j, expand_each_step, simplify_each_step) for i,j in itr)

    R = sympy.zeros(rows, cols)
    with multiprocessing.Pool(processes=proc) as pool:
        for i, j, r in pool.imap_unordered(mat_mul_dot, gen, chunksize=chunksize):
            print(f' ({i:>2},{j:>2})')
            R[i,j] = r
            R[j,i] = r
    print('symmetric mat multiply done') 
    return sympy.ImmutableMatrix(R)



if __name__ == '__main__':

    N = 3
    A = construct_general_sym_diag_0_matrix(N, element_letter='a')
    M = construct_general_sym_diag_0_matrix(N, element_letter='m')

    expand = True
    A = sympy.ImmutableMatrix(A)
    M = sympy.ImmutableMatrix(M)
    # Ap = mat_power_by_square(A, 10, expand=False)
    # print(A.charpoly())
    # Ap = sym_mat_pow(A, 2, expand=False)
    # print(Ap[0,0])

    syms_a = {(i,j):A[i,j] for i,j in itertools.product(range(A.shape[1]), repeat=2) if (A[i,j]!=1 and A[i,j]!=0)}
    syms_m = {(i,j):M[i,j] for i,j in itertools.product(range(M.shape[1]), repeat=2) if (M[i,j]!=1 and M[i,j]!=0)}

    M = M.pow(2)
    M2 = M # save for later
    M.expand()

    Q = sympy.ImmutableMatrix([1]*N)
    print(Q)
    qAqT = Q.T * A * Q
    print(qAqT)

    # currently, M = A**2
    p = 2
    while p < 32:
        print(f'exponent is {p}, working on squaring to {p*2}')
        # substitute M symbols for a
        for k in syms_m.keys():
            aij = syms_a[k]
            mij = syms_m[k]
            M = M.subs(mij, aij)
        # print('expanding 1...', end='')
        # M.expand()
        # print(M[0,0].expand())

        # sustitute m2 expression for each a
        for k in syms_a.keys():
            a = syms_a[k]
            i, j = k
            M = M.subs(a, M2[i,j])
        # print('expanding 2...')
        # M.expand()
        # print(M[0,0].expand())
        p *= 2


    # for i, j in itertools.product(range(A.shape[0]), repeat=2):
    #     A[i,j].subs()

    # now at 34
    M = Q.T * M * Q
    m00 = M[0,0]
    with open('srtout.txt', 'w') as f:
        f.write(str(m00))
    print(m00)

    # if expand:
    #     print('expanding...')
    #     m00.simplify()
    #     with open('srtout_expanded.txt', 'w') as f2:
    #         f2.write(str(m00))

    # print(s)

    # Ap = A
    # while n <= 34:
    #     # print(n, Ap[0,0].simplify())
    #     print(n)
    #     Ap = Ap*A
    #     n += 1