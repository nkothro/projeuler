import math
from itertools import combinations
from collections import defaultdict
from multiprocessing import Pool


def gen_factorial_list(n):
    # list up to n!
    f = [1]
    j = 1
    while len(f) < n:
        f.append(f[-1] * j)
        j += 1

    return f


class CoprimeHelper:
    def __init__(self, upper):
        x = list(range(upper + 1))
        self._upper = max(x)
        self._indiv = defaultdict(lambda: [])
        self._pairwise = defaultdict(lambda: False)

        for a, b in combinations(x, 2):
            if math.gcd(a, b) == 1:
                self._indiv[a].append(b)
                self._indiv[b].append(a)

                # force a > b
                if a < b:
                    a, b = b, a
                self._pairwise[(a, b)] = True

        for k, v in self._indiv.items():
            self._indiv[k] = sorted(v)
    
    def __call__(self, a, b): 
        return self.is_coprime(a, b)

    def is_coprime(self, a, b):
        if a > self._upper or b > self._upper:
            raise ValueError("Not available in this range")
        return self._pairwise[(a, b)] if a > b else self._pairwise[(b, a)]

    def is_coprime_permutation(self, p: tuple):
        for j in range(len(p) - 1):
            if not self.is_coprime(p[j], p[j + 1]):
                return False, j

        return True, -1

    def coprime_with(self, x):
        if x > self._upper:
            raise ValueError(f"x={x} is too large")
        return self._indiv[x]


class FactoradixCounter:
    def __init__(self, initial_value, places):
        self._f_ref = gen_factorial_list(places)
        self._v = self.from_int(initial_value, self._f_ref)

    @staticmethod
    def from_int(n: int, factorials: list = None):
        if factorials is None:
            factorials = [1]
            j = 1
            while factorials[-1] < n:
                factorials.append(factorials[-1] * j)
                j += 1

        ln = len(factorials)
        r = n
        factoradix = []
        for j in range(ln):
            q, r = divmod(r, factorials[~j])
            factoradix.append(q)

        # while factoradix[0] == 0:
        #     del factoradix[0]

        return list(reversed(factoradix))

    def to_decimal(self):
        return sum(val * self._f_ref[j] for j, val in enumerate(self._v))

    @property
    def list_repr(self):
        return self._v

    def __str__(self):
        return ":".join(str(k) for k in reversed(self.list_repr))

    def __repr__(self):
        return ":".join(str(k) for k in reversed(self.list_repr))

    def increment(self):
        # increment by one
        l = len(self.list_repr)
        self.list_repr[0] += 1
        for j, v in enumerate(self.list_repr):
            if v > j:
                self.list_repr[j] = 0
                if l <= j + 1:
                    self.list_repr.append(0)
                self.list_repr[j + 1] += 1
            else:
                break

    def apply_as_permuation(self, iter):
        digits = len(self.list_repr)
        d = list(iter)
        perm = []
        j = digits
        # while j > 0:
        #     j -= 1
        #     k = self.list_repr[j]
        #     perm.append(d[k])
        #     del d[k]

        for j in list(reversed(self.list_repr)):
            perm.append(d[j])
            del d[j]

        return tuple(perm)

    def roll_to_even_place(self, index_to_roll):
        # at specific place in factoradix representation, increment and zero out
        # all following, less significant values in their place
        self._v[~index_to_roll] += 1
        j = ~index_to_roll
        j += 1
        while j < len(self._v):
            self._v[j] = 0
            j += 1

        l = len(self.list_repr)
        for j, v in enumerate(self.list_repr):
            if v > j:
                self.list_repr[j] = 0
                if l <= j + 1:
                    self.list_repr.append(0)
                self.list_repr[j + 1] += 1
            else:
                break
from eulerlib import FactoradixCounter


def iteratethru(starting_list, cph: CoprimeHelper, perm_len):
    if len(starting_list) == perm_len:
        # print(starting_list)
        return 1

    gen = (
        n
        for n in cph.coprime_with(starting_list[-1])
        if n != 1 and n not in starting_list
    )
    count = 0
    for j in gen:
        l2 = starting_list + [j]
        count += iteratethru(l2, cph, perm_len=perm_len)

    return count


def count_coprime_perms(upper):
    x = tuple(range(2, upper + 1))
    cp = CoprimeHelper(upper)

    c = 0
    for j in x:
        print(f'working on {j}')
        s = [j]
        c += iteratethru(s, cp, upper - 1)

    return c


def count_coprime_perms_pooled(upper, proc=6):
    tot = 0
    cp = CoprimeHelper(upper)
    x = tuple(range(2, upper + 1))

    def wrap(j):
        return iteratethru([j], cp, upper-1)

    gen = ((xi, cp, upper) for xi in x)

    with Pool(processes=proc) as p:
        for ct in p.starmap(iteratethru, gen):
            tot += ct

    return tot


if __name__ == "__main__":
    from itertools import combinations
    from collections import defaultdict

    upper = 4
    initial_value = 0
    ct = 0

    x = tuple(range(2, upper + 1))
    fx = FactoradixCounter(0, len(x))

    cp = CoprimeHelper(upper)

    # while fx.to_decimal() < 100 * fx._f_ref[-1]:
    #     xperm = fx.apply_as_permuation(x)
    #     b, j = cp.is_coprime_permutation(xperm)
    #     if b:
    #         print(fx, fx.to_decimal(), xperm)
    #         fx.increment()
    #     else:
    #         fx.roll_to_even_place(j + 1)
    #         print(f"jumping to {fx.to_decimal()}")
    #         print(f"  not coprime at {j} <{xperm[j]},{xperm[j+1]}> {xperm}")
    #         print(f"  {fx.list_repr}")

    #     if fx.to_decimal() == 0:
    #         break

    # for j in range(3, 34):
    #     k = count_coprime_perms_pooled(j)
    #     print(j, k)

    k = count_coprime_perms(13)
    print(k)