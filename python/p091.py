import itertools
from collections import Counter

"""
Given 3 points --O, P, and Q-- there are thre vectors or interest: PQ, OQ, and
OP. Coordinates for O are always (0,0), let P = (x1,y1) and Q = (x2,y2)
Vector OP is <x1, y1>
Vector OQ is <x2, y2>
Vector PQ is <x2-x1, y2-y1>

Dot product for right angle is always 0.
Three dot products to test: OP.OQ, OP.PQ, and PQ.OQ
"""

def is_right_triangle(x1, y1, x2, y2):
    """
    Assumes (x1,y1) != (x2,y2)
    Assumes (x1, y1) != (0,0)
    Assumes (x2, y2) != (0,0)
    """
    opoq = x1*x2 + y1*y2
    if not opoq:
        return True
    oppq = x1*(x2-x1) + y1*(y2-y1)
    if not oppq:
        return True
    pqoq = x2*(x2-x1) + y2*(y2-y1)
    if not pqoq:
        return True
    
    return False

# generate pairs
max_grid_coord =2 
coords = [(x,y) for x,y in itertools.product(range(max_grid_coord+1), repeat=2)]
coords.remove((0,0)) # origin already a triangle point

right_triangles_found = 0
for (x1, y1), (x2, y2) in itertools.combinations(coords, 2):
    if is_right_triangle(x1,y1,x2,y2):
        # print(x1, y1, x2, y2)
        right_triangles_found += 1
print(right_triangles_found)

import itertools
import numpy as np

allowed_points = [k for k in itertools.product(range(0,51), repeat=2)]

def forms_right_triangle(P, Q):
    px, py = P
    qx, qy = Q

    # PQ is the hypotenuse
    if (px==0 and qy==0) or (qx==0 and py==0):
        return 1

    sq_len_PQ = (px-qx)**2 + (py-qy)**2
    sq_len_OP = (px)**2 + (py**2)
    sq_len_OQ = qx**2 + qy**2
    #OP is hypotenuse
    if(sq_len_OP == sq_len_OQ + sq_len_PQ):
        return 1
    #OP is hypotenuse
    if(sq_len_OQ == sq_len_OP + sq_len_OQ):
        return 1

    return 0
    

ct = 0
for P, Q in itertools.combinations(allowed_points, 2):
    ct += forms_right_triangle(P,Q)

print(ct)