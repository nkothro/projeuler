from primes.primelist import PrimesList

def truncates(num: int):
    rlist = list(str(num))
    llist = list(str(num))
    trunks = set()

    while len(rlist) >= 1:
        r = "".join(rlist)
        rlist.pop(0)

        l = "".join(llist)
        llist.pop(-1)

        trunks.add(int(r))
        trunks.add(int(l))

    return trunks

if __name__ == '__main__':
    maxprime = 10**6
    parr = PrimesList(maxprime)
    p = parr.list
    print(type(p))
    # plist = [x for x in p if x > 10]
    pset = set(p)
    plist = list(p)

    truncatable_primes = []
    magic_sum = 0
    count = 0
    for prime in plist:
        if truncates(prime) <= pset:
            print(prime)

            magic_sum += prime
            truncatable_primes.append(prime)
            count += 1

    truncatable_primes = [p for p in truncatable_primes if p > 7]
    print("The sum is {}, and there are {} truncatable primes below {}".format(sum(truncatable_primes), len(truncatable_primes), maxprime))
    print(truncates(3797))
    print(parr.is_prime(3797))
