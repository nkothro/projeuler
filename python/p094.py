import numpy as np

def herons(a, b, c):
    s = (a + b + c)/2
    A = (s*(s-a)*(s-b)*(s-c))**0.5
    return A

def single_herons(a):
    """
    Return area of triangle with side lengths a, a, and a +/- 1
    """
    const = 3*(a**4) - 2*(a**2) - 1
    sq_area_minus = (1/16)*(const - 4*a*(a**2 - 1))
    sq_area_plus = (1/16)*(const + 4*a*(a**2 - 1))
    return sq_area_plus, sq_area_minus

def iterator_almost_equilateral_triangles(max_perimiter):
    if max_perimiter < 5:
        raise ValueError("smallest 'near equilateral triangle' is (2, 2, 1)")
    s_max = (max_perimiter // 3) + 1

    yield (2, 2, 1)
    if s_max == 2:
        return

    n = 3
    while n <= s_max:
        yield (n, n, n-1)
        yield (n, n-1, n-1)
        n += 1

def partial_herons(a,b,c):
    return (a+b+c)*(a+b-c)*(a-b+c)*(-a+b+c)

def case_A_check(n):
    """
    Case A: (n, n-1, n-1) triangle.
    According to Heron's formula
    AREA^2 = (1/16)(a+b+c)(a+b-c)(a-b+c)(-a+b+c)
    For (a, b, c) = (n, n-1, n-1) this becomes
    AREA^2 - (1/16) {(3*n - 2)(n**2)(n-2)}

    This function outputs the above expression in curly braces
    """
    return (3*n - 2)*(n**2)*(n-2)

def case_B_check(n):
    """
    Case B: (n, n, n-1) triangle.
    According to Heron's formula
    AREA^2 = (1/16)(a+b+c)(a+b-c)(a-b+c)(-a+b+c)
    For (a, b, c) = (n, n, n-1) this becomes
    AREA^2 - (1/16) {(3*n - 1)(n-1)**2 (n+1)}

    This function outputs the above expression in curly braces
    """
    return (3*n - 2)*(n+1)*(n-1)**2


if __name__ == "__main__":
    # Generate a list of squares
    squares = np.arange(10**8, dtype=np.dtype('u8'))**2
    n = np.arange(10**7)

    # Case A: (n,n,n+1)
    poly_A = np.polynomial.polynomial.Polynomial([-1,-4,-2,+4,+3]) 
    qA = poly_A(n)
    mod16_mask = (qA % 16 == 0)

    qA_distilled = qA[mod16_mask]
    n_distilled = n[mod16_mask]
    inserts = np.searchsorted(squares, qA_distilled)

    # fix indexing
    i = len(squares)
    inserts[inserts==i] = -1

    z = (squares[inserts] == qA_distilled)
    print(n_distilled[z], qA_distilled[z])
    print(z)
    



    # Case A: (n,n,n-1)
    poly_B = np.polynomial.polynomial.Polynomial([-1,+4,-2,-4,+3]) 