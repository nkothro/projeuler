# p886e.py

from itertools import product

from p886b import create_gcd_mat
import sympy

def matrix_exponentiation_binary_syms(A, n):
    # declare/allocate return matrix
    R = sympy.zeros(A.cols, A.rows)
    A = A.expand()
    for i, j in product(range(A.cols), range(A.rows)):
        R[i,j] = simplify_binary_variables_expression(A[i,j].expand())

    if n < 0 or int(n) != n:
        raise ValueError('expect a non-negative, integer exponent')
    
    elif n == 0:
        return sympy.eye(A.cols)

    elif n==1: 
        return R

    else:
        is_odd = n & 1
        if is_odd:
            t = R * matrix_exponentiation_binary_syms(R*R, (n-1)//2, )
        else:
            t = matrix_exponentiation_binary_syms(R*R, n//2)

        return R
        

    

def simplify_binary_variables_expression(exp):
    # assume all sympy variables 'bi' are a bool int (ie either 0 or 1)
    # bi**n = b, in either case
    terms = sympy.Add.make_args(exp)
    collected = 0
    for t in terms:
        args = [t.as_coeff_mul()[0]] # pull coeff
        args += list(t.free_symbols) # get all binary symbols in term
        collected += sympy.Mul(*tuple(args))

    return collected

if __name__ == '__main__':
    N = 5
    exp = 4
    binsym = {}
    for i,j in product(range(N), repeat=2):
        if i <= j:
            binsym[(i,j)] = sympy.Symbol(f'b{i}_{j}')

    M = sympy.zeros(N,N)
    for i,j in product(range(N), repeat=2):
        if i==0 or j==0:
            M[i,j] = 1
        elif i==j:
            M[i,j] = 0
        elif i < j:
            M[i,j] = binsym[(i,j)]
        else:
            M[i,j] = binsym[(j,i)]

    by_normal_exponentiation = M**exp
    fancy = matrix_exponentiation_binary_syms(M, exp)

    print(by_normal_exponentiation[0,0])
    print(fancy[0,0])
