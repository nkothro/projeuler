def digital_powers(base: int):
    return False

def is_digital_power(base:int, exp:int):
    num = base**exp
    if len(str(num))==exp:
        return True
    else:
        return False

if __name__ == '__main__':
    """
    Only one-digit numbers canbe raised to a power n and produce an n-digit number
    10**2 is already 3 digits. Any n larger than 10, when squared, will be more than 2 digits.
    1**m is always 1.
    Check 2 through 9.
    """
    count = 0
    for base in range(2,10):
        for power in range(2,80):
            if is_digital_power(base, power):
                count+=1
                print('{}^{}={} is {} digits'.format(base, power, base**power, len(str(base**power))))

    print('There are {} of these numbers'.format(count))
