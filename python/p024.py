from itertools import permutations

f = open('./p024permutations.txt', 'w')
for j, perm in enumerate(permutations('0123456789')):
    if (j-1) == 10**6:
        print('{}{}{}{}{}{}{}{}{}{}'.format(*perm))

