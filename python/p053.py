import math

if __name__ == '__main__':
    list_of_greater_than_million = []
    for n in range(1, 100 + 1):
        for r in range(0, n+1):
            num = math.comb(n, r)
            if num > 10**6:
                list_of_greater_than_million.append(num)

    print(len(list_of_greater_than_million))

