import itertools

probs = [.2, .4, .6, .8]
ct = 0
tot = 0
for b in itertools.product([True,False], repeat=4):
    p = 1.0
    for bi, pi in zip(b, probs):
        if bi:
            p *= (1-pi)
        else:
            p *= pi
    
    tot += p

p *= len(probs)

print(p)
