from collections import Counter
def charsort(s):
    c = list(str(s))
    c.sort()
    ch = ''.join(c)
    return ch

if __name__ == '__main__':
    cubes_list = [j**3 for j in range(10**5)]
    sorted_cubes = [charsort(st) for st in cubes_list]
    print(sorted_cubes)
    c = Counter(sorted_cubes)

    for j, string in enumerate(sorted_cubes):
        if c[string] == 5:
            print('{}**3 = {}, {}'.format(j, j**3, c[string]))
