from fractions import Fraction as Frac
import math as m
main_value = Frac(3,7)

min_bound = Frac(0, 1)

for d in range(2, 10**6):
    max_num = m.ceil(d * 3/7)
    for n in range(0, max_num):
        test_num = max_num - n
        test_frac = Frac(test_num, d)
        if (min_bound < test_frac) and (test_frac < main_value):
            min_bound = test_frac
            break
        elif test_frac <= min_bound:
            break

print(min_bound)

