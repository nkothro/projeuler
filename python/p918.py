from collections.abc import MutableSequence

def recur_indices(n, max_lut_idx):

    indxs = [n]

    while any(j > max_lut_idx for j in indxs):
        x = []
        for j in indxs:
            halfj = j >> 1
            x.append(halfj)
            if j & 1:
                x.append(halfj + 1)
        
        indxs = x.copy()
        print(indxs)

    return list(set(indxs))

class SequenceHolder(MutableSequence):
    def __init__(self, initial_length):
        if initial_length <=2:
            raise ValueError('len must be larger thaGn 2')
        self._a = [None] * (initial_length + 1)
        self._a[0] = 0
        self._a[1] = 1

    def __len__(self):
        len(self._a)

    def __setitem__(self, index, value):
        self._a[index] = value

    def __delitem__(self, index):
        self._a[index] = None

    def __getitem__(self, index):
        return self.get_term(index)

    def insert(self, index, value):
        return self._a.insert(index, value)

    def get_term(self, n):
        while len(self._a) < (n+1):
            self._a.append(None)
        
        if self._a[n] is not None:
            return self._a[n]
        
        half_n = n >> 1
        # if odd
        if n & 1:
            self._a[n] = self.get_term(half_n) - 3*self.get_term(half_n + 1)
        else:
            self._a[n] = 2*self.get_term(half_n)
        
        return self._a[n]


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import numpy as np

    # N = 2**18
    # s = SequenceHolder(N)
    # for j in range(1,N+1):
    #     s.get_term(j)

    # x = np.asarray(s._a)
    # xsum = x.cumsum()
    # plt.plot(xsum, label='$\Sigma a_n$')
    # plt.plot(x, label='$a_n$')

    # z = np.arange(x.size)
    # z = z[xsum == 0]
    # print(z)

    # L = int(np.log2(N))
    # powsof2 = [2**n for n in range(L+1)]
    # powsof2 = np.asarray([x for x in powsof2 if x < N])

    # print(xsum[powsof2])

    # plt.vlines(z, x.min(), x.max())

    # plt.legend()
    # plt.show()

    # x = np.arange(2**32 + 1, dtype=np.uint64)
    # print(x.size)
    # print(np.log2(10**12))
    
    i = 1
    j = 0
    N = 10**12
    while i < N:
        print(f'2^{j:>3} = {i}')
        i *= 2
        j += 1

    i //= 2
    j -= 1
    
    print(j, N - i)
    print(np.log2(N - i))

    l = recur_indices(10**12 + 1, 2**32)
    print(l)