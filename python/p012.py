import numpy as np
# Make function to determine factors of a number
def factors_list(num):
    factors = [1]  #initalize list, 1 is always a factor 
    for i in range(2, num+1):
        if num % i == 0:
            factors.append(i)
    return(factors)

def countfactors(num):
    # returns number of whole-number factors for num
    count=0
    for i in range(1, num+1):
        if num % i == 0:
            count = count+1
    return(count)

def triangularnumber(j):
    # returns the jth triangular number
    sum=0
    for i in range(0,j+1):
        sum=sum+i
    return(sum)

def divisors_list(num):
    # returns list of all divisors of num (including num and 1
    div_list = [1]
    upper = int(np.sqrt(num) + 1)
    for i in range(2, upper):
        if num % i == 0:
            div_list.append(int(i))
            if int(num / i) != int(i) and num != int(num/i):
                div_list.append(int(num/i))
    return div_list

count = 1
j=2
while count<=500:
    count= len(divisors_list(triangularnumber(j)))
    print(j, triangularnumber(j) , count)
    j=j+1

