import numpy as np
import matplotlib.pyplot as plt

from math import factorial

#global for time saving
digit_factorials = np.asarray([factorial(j) for j in range(10)])

def factoradix_to_decimal(f:list)->int:
    """
    [0, 1] = 0*0! + 1*1! = 1
    [0, 0, 0, 2] = 0*0! + 0*1! + 0*2! + 2*3! = 12
    per wikipedia:
    [0,1,0,1,4,3] -> 463
    """
    s = 0
    for place, value in enumerate(f):
        if value > place:
            raise ValueError('invalid list, value of nth element (place) in factoradix representation must be less than n')
        s += value*digit_factorials[place]
    return s

def digit_factorial_sum(num: int):
    num = int(num)
    num_str = str(num)
    return sum(digit_factorials[int(d)] for d in num_str)


class Factoradix:
    def __init__(self, decimal):
        self._list_repr = self.from_decimal(decimal)
    
    @staticmethod
    def from_decimal(num):
        list_repr = []

        div = 1
        while num > 0:
            num, rem = divmod(num, div)
            list_repr.append(rem)
            div += 1

        return list_repr
    
    def to_decimal(self):
        return sum(value * factorial(place) for place, value in enumerate(self._list_repr))

    @property
    def list_repr(self):
        return self._list_repr

    def expression(self):
        s = f'{self.to_decimal()} = '
        rhs = []
        for place, value in enumerate(self._list_repr):
            rhs.append(f'{value}*{place}!')
        s += ' + '.join(rhs)
        return s


    def increment(self):
        self._list_repr[0] += 1
        l = len(self._list_repr)
        for j, v in enumerate(self._list_repr):
            if v > j:
                self._list_repr[j] = 0 
                if l <= j+1:
                    self._list_repr.append(0)
                self._list_repr[j+1] += 1
            else:
                break
        

if __name__ == '__main__':
    # f = [0, 0, 0, 2]
    # f2 = [0,0,0,0,0,0,0,3,2]
    # print(factoradix_to_decimal(f2))

    x = Factoradix(9999)
    # x.increment()
    # s = x.to_decimal()
    # print(s)
    print(x.list_repr)

    # x = Factoradix(40585)
    # print(x.list_repr)
    # print(x.expression())

    # max_mag = 5
    # x = []
    # y = []

    # j = 1 
    # as_factoradix = Factoradix(j)
    # while j < 10**max_mag:
    #     x.append(j)
    #     y.append(sum(as_factoradix.list_repr))

    #     j += 1
    #     as_factoradix.increment()


    # plt.plot(x,y, 'k')
    # plt.vlines([10**j for j in range(max_mag+1)], ymin=0, ymax=max(y),)
    # plt.show()

    x = np.arange(1,10)
    Smax = factorial(9) * x / (np.power(10,x) - 1)
    Smin = x / np.power(10, (x-1))
    print(np.power(10,x) - 1)

    plt.plot(x, Smax, label='$S_{max}$')
    plt.plot(x, Smin, label='$S_{min}$')
    plt.legend()
    # plt.yscale('log')
    plt.show()
