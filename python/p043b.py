def has_substring_divisibility_property(n: int or str):
    if isinstance(n, int):
        d = str(n)
    else:
        d = n

    if int(d[7:]) % 17:
        return False

    if int(d[6:9]) % 13:
        return False

    if int(d[5:8]) % 11:
        return False

    if int(d[4:7]) % 7:
        return False

    if int(d[3:6]) % 5:
        return False

    if int(d[2:5]) % 3:
        return False

    if int(d[1:4]) % 2:
        return False
    
    return True

if __name__ == '__main__':
    from itertools import permutations

    good_ones = []
    for p in permutations('0123456789'):
        if p[0] == '0':
            pass
        else:
            n = ''.join(i for i in p)
            if has_substring_divisibility_property(n):
                good_ones.append(int(n))

    print(sum(good_ones))