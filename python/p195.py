import matplotlib.pyplot as plt
import eulerlib

valids = []
for a in range(2, 1000):
    valid_b = []
    for b in range(1, a):
        csq = a**2 + b**2 - (a*b)
        if eulerlib.is_perfect_square(csq):
            valid_b.append(csq)
    valids.append((a,valid_b))

for a, vb in valids:
    x = len(vb) * [a]
    plt.plot(x, vb, '.', label=a)
plt.show()
