import re
from enum import IntEnum
from collections import Counter

_val_map = {
    '2':2,
    '3':3,
    '4':4,
    '5':5,
    '6':6,
    '7':7,
    '8':8,
    '9':9,
    'T':10,
    'J':11,
    'Q':12,
    'K':13,
    'A':14
}

class Rank(IntEnum):
    HIGH_CARD = 0
    ONE_PAIR = 1
    TWO_PAIRS = 2
    THREE_OF_KIND = 3
    STRAIGHT = 4
    FLUSH = 5
    FULL_HOUSE = 6
    FOUR_OF_KIND = 7
    STRAIGHT_FLUSH = 8
    ROYAL_FLUSH = 9

class Card():
    def __init__(self, string_repr):
        v, s = tuple(string_repr)        
        self._value = _val_map[v]
        self._suit = s
    
    @property
    def value(self):
        return self._value
    
    @property
    def suit(self):
        return self._suit


class Hand():
    def __init__(self, string_repr):
        self._cards = []
        for m in re.finditer('[AKTQJ\d][HSCD]', string_repr):
            self._cards.append(Card(str(m.group())))

        self._cards.sort(key=lambda x: x.value, reverse=True)
        self._values = [c.value for c in self._cards]

        self._rank = None

        self._flush = None
        self._straight = None
        self._straight_flush = None
        self._royal_flush = None

        self._counter = None
        self._num_pairs = None
        self._three_of_kind = None
        self._four_of_kind = None

        self._tripple_val = None
        self._pair_val = None
        self._four_of_kind_val = None
        
    @property
    def card_values(self):
        return self._values
        
    def isFlush(self):
        if self._flush is None:
            suits = [c.suit for c in self._cards]
            self._flush = all(x==suits[0] for x in suits)

        return self._flush

    def isStraight(self):
        if self._straight is None:
            if [c.value - self._cards[-1].value for c in self._cards] == [4, 3, 2, 1, 0]:
                self._straight = True
            else:
                self._straight = False
        return self._straight

    def isStraightFlush(self):
        if self._straight_flush is None:
            if self.isFlush() and self.isStraight():
                self._straight_flush = True
            else:
                self._straight_flush = False
        
        return self._straight_flush


    def isRoyalFlush(self):
        if self._royal_flush is None:
            if self.isStraightFlush() and self.card_values == [14, 13, 12, 11, 10]:
                    self._royal_flush = True
            else:
                self._royal_flush = False

        return self._royal_flush

    def counts(self):
        if self._counter is None:
            self._counter = Counter(self.card_values)

            self._num_pairs = 0

            self._three_of_kind = False
            self._four_of_kind = False
            self._pair_val = []

            for v, c in self._counter.items():
                if c == 2:
                    self._num_pairs += 1
                    self._pair_val.append(v)
                elif c == 3:
                    self._three_of_kind = True
                    self._tripple_val = v
                elif c == 4:
                    self._four_of_kind = True
                    self._four_of_kind_val = v


        return self._counter
    
    @property
    def pair_vals(self):
        return self._pair_val if self.numPairs > 0 else [0]

    @property
    def tripple_val(self):
        return self._tripple_val if self.hasThreeOfKind() else 0

    @property
    def quad_value(self):
        return self._four_of_kind_val if self.hasFourOfKind() else 0

    @property
    def numPairs(self):
        if self._num_pairs is None:
            self.counts()
        return self._num_pairs

    def hasThreeOfKind(self):
        if self._three_of_kind is None:
            self.counts()
        return self._three_of_kind

    def hasFourOfKind(self):
        if self._four_of_kind is None:
            self.counts()
        return self._four_of_kind
    
    def isFullHouse(self):
        return self.hasThreeOfKind() and self.numPairs==1

    def highCard(self):
        return max(self.card_values)

    @property
    def rank(self):
        if self._rank is None:
            if self.isRoyalFlush():
                self._rank= Rank.ROYAL_FLUSH
            
            elif self.isStraightFlush():
                self._rank= Rank.STRAIGHT_FLUSH
            
            elif self.hasFourOfKind():
                self._rank= Rank.FOUR_OF_KIND

            elif self.isFullHouse():
                self._rank= Rank.FULL_HOUSE
            
            elif self.isFlush():
                self._rank= Rank.FLUSH

            elif self.isStraight():
                self._rank= Rank.STRAIGHT
            
            elif self.hasThreeOfKind():
                self._rank= Rank.THREE_OF_KIND

            elif self.numPairs == 2:
                self._rank= Rank.TWO_PAIRS
            
            elif self.numPairs == 1:
                self._rank= Rank.ONE_PAIR
            
            else:
                self._rank= Rank.HIGH_CARD
        return self._rank
        

def compare(A:Hand, B:Hand):
    tA = ('player1', 1, A.rank)
    tB = ('player2', 0, B.rank)
    if A.rank != B.rank:
        return tA if A.rank > B.rank else tB
    
    elif A.rank == Rank.STRAIGHT_FLUSH:
        return tA if A.highCard > B.highCard else tB
    
    elif A.rank == Rank.FOUR_OF_KIND:
        return tA if A.quad_value > B.quad_value else tB
    
    elif A.rank == Rank.FULL_HOUSE or A.rank==Rank.THREE_OF_KIND:
        return tA if A.tripple_val > B.tripple_val else tB
    
    elif A.rank == Rank.FLUSH or A.rank == Rank.STRAIGHT:
        return tA if A.card_values > B.card_values else tB
    
    elif A.rank==Rank.TWO_PAIRS or A.rank==Rank.ONE_PAIR:
        if A.pair_vals != B.pair_vals:
            return tA if sorted(A.pair_vals, reverse=True) > sorted(B.pair_vals, reverse=True) else tB
        else:
            return tA if A.card_values > B.card_values else tB
    
    else:
        return tA if A.highCard() > B.highCard() else tB



    

samples = [
    '5H 5C 6S 7S KD 2C 3S 8S 8D TD',
    '5D 8C 9S JS AC 2C 5C 7D 8S QH',
    '2D 9C AS AH AC	3D 6D 7D TD QD',
    '4D 6S 9H QH QC	3D 6D 7H QD QS', 
    '2H 2D 4C 4D 4S 3C 3D 3S 9S 9D',
]

if __name__ == '__main__':
    #     A = Hand(l[0:15])
    #     B = Hand(l[16:])
    #     s += compare(A, B)

    f = open('./python/p054_hands.txt', 'r')
    s = 0
    for l in f.readlines():
    # for l in samples:
        player1 = l[0:15]
        player2 = l[15:]
        A = Hand(player1)
        B = Hand(player2)

        # print(player1, A.rank)
        # print(player2, B.rank)
        winner, sw, r = compare(A, B)
        s += sw

        print(s)

