import eulerlib
import math
sqrt2 = math.sqrt(2)


def nth_triangular(n):
    return n*(n+1) // 2

# for n in range(1,10**9):
#     n_sqrt2 = sqrt2 * n
#     klo = math.floor(n_sqrt2) - 1
#     khi = math.ceil(n_sqrt2) + 2
#     n_side = 2 * (n) * (n-1)
#     for k in range(klo, khi):
#         if n_side == k**2 - k:
#             s = f'{n:>8} blue discs, {k-n:>8} red discs, {k:>8} total'
#             if k > 10**12:
#                 s += ' | exceeds 10^12'
#             print(s)
    
def triangular_numbers_divisible_by_four(min_num=None, max_num=None):
    if min_num is None:
        min_j = 1
    else:
        tri_floor = ((0.5)*(math.sqrt(8*min_num + 1) - 1))
        min_j = math.ceil(tri_floor / 4)
    
    j = min_j
    Ta = nth_triangular(4*j - 1)
    Tb = nth_triangular(4*j)

    while max_num is None or Ta < max_num:
        if Ta >= min_num:
            yield Ta
        if (max_num is None or Tb < max_num) and Tb >= min_num:
            yield Tb
        j += 1
        Ta = nth_triangular(4*j - 1)
        Tb = nth_triangular(4*j)
        
def is_triangular(n):
    t = 8*n + 1
    return math.isqrt(t)**2 == t
    # return eulerlib.is_perfect_square(8*n + 1)

def inverse_triangular(n):
    return (0.5)*(math.sqrt(8*n + 1) - 1)



if __name__ == '__main__':
    # size of bag is (k+1)
    # want k+1 > 10**12
    # smallest Tk is T(10**12 - 1)

    t0 = nth_triangular(10**12 - 1)
    t0 = 1
    
    x = []
    y = []
    for tri_by_four in triangular_numbers_divisible_by_four(t0, 10**10): 
        test_num = tri_by_four//2
        if is_triangular(test_num):
            n = inverse_triangular(test_num)
            k = inverse_triangular(tri_by_four)

            x.append(tri_by_four)
            y.append()

            p = n*(n+1)
            p /= k*(k+1)
            print(p, k+1)
