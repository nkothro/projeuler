import itertools
import math

def digit_sum(n):
    return sum([int(c) for c in str(n)])


if __name__ == '__main__':
    k = 100
    ct = 0
    lst = []
    for base, exponent in itertools.product(range(k), range(k)):
        pwr = base**exponent
        dsum = digit_sum(pwr)
        if base == dsum and pwr > 9:
            ct += 1
            lst.append(pwr)
            print(f'{ct:>3}: {base:>3}^{exponent:>3} = {pwr:>5}')

    lst.sort()
    print(lst[30-1])