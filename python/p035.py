from primes.primelist import PrimesList, primesfrom2to
from bisect import bisect_left

def rotate(l:list, n:int):
    return l[n:] + l[:n]

def circle_number(num: int):
    digilist = list(str(num))
    circled = set()

    for j in range(len(digilist)):
        circled.add(int("".join(rotate(digilist, j))))

    return circled



if __name__ == "__main__":
    max_num = 10**6
    plist = list(PrimesList(max_num).list)
    # pset = set(plist)

    circlular_primes = []

    pcount = len(plist)
    test_idx = [True] * pcount

    for j, p in enumerate(plist):
        if test_idx[j]:
            circled = circle_number(p)
            d = len(circled)

            ct = 0
            for c in circled:
                k = bisect_left(plist, c)
                try:
                    if plist[k] == c:
                        test_idx[k] = False
                        ct += 1
                except:
                    pass

            if ct==d:
                print(circled)
                circlular_primes.extend(circled)
    print(len(circlular_primes))




