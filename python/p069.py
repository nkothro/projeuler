from primes.primelist import PrimesList
from math import gcd
from sympy.ntheory import totient

p = PrimesList(10**6 + 1)
plist = p.list

"""
def totient(n):
    if n in plist:
        return (n-1)
    else:
        count = 0
        for j in range(1, n):
            if gcd(j, n) == 1:
                count += 1
        return count
"""



if __name__ == '__main__':
    max_value = 1
    max_n = 2
    for n in range(2, 10**6+1):
        phi = n/totient(n)
        if phi > max_value:
            max_value = phi
            max_n = n
            print(n)
        if n % 10**4 == 0:
            print('Reached {}%'.format(n/10**4))

    print('Value of n producing maximum n/phi(n): {}'.format(max_n))

