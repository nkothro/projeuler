import numpy as np
from typing import Iterable
from itertools import permutations

def triangular(n):
    return n*(n+1) // 2

def square(n):
    return n**2

def pentagonal(n):
    return n*(3*n - 1)//2

def hexagonal(n):
    return n*(2*n - 1)

def heptagonal(n):
    return n*(5*n - 3)//2

def octagonal(n):
    return n*(3*n - 2)

def lst_of_figurates_of_given_digits(num_digits, f:callable):
    upper = 10**(num_digits)
    lower = 10**(num_digits - 1)

    j = 1
    n = f(j)
    lst = []
    while n < upper:
        if n >= lower and n < upper:
            lst.append(n)
        j+=1
        n = f(j)
    
    return lst

def cyclic_permutations(I:Iterable):
    i0 = I[0]
    for p in permutations(I[1:]):
        yield i0, *p


def is_cyclic_link(a, b)->bool:
    A = str(a)[-2] + str(a)[-1]
    B = str(b)[0]  + str(b)[1]
    if A == B:
        return True
    else:
        return False

if __name__ == '__main__':

    figuratives = {}
    figuratives['triangular'] = lst_of_figurates_of_given_digits(4, triangular)
    figuratives['square'] = lst_of_figurates_of_given_digits(4, square)
    figuratives['pentagonal'] = lst_of_figurates_of_given_digits(4, pentagonal)
    # figuratives['hexagonal'] =lst_of_figurates_of_given_digits(4, hexagonal)
    # figuratives['heptagonal'] = lst_of_figurates_of_given_digits(4, heptagonal)
    # figuratives['octagonal'] = lst_of_figurates_of_given_digits(4, octagonal)

    k_figuratives = list(figuratives.keys())
    chain_maps = {(i,j):[] for i,j in permutations(k_figuratives, 2)}

    for b, f in chain_maps.keys():
        for i, i_num in enumerate(figuratives[b]):
            for j, j_num in enumerate(figuratives[f]):
                if is_cyclic_link(i_num, j_num):
                    chain_maps[(b,f)].append((i,j))

    for k0, k1, k2 in cyclic_permutations(k_figuratives):

        print(k0, k1, k2)
        
        all_paths = []
        for m in chain_maps[(k0, k1)]:
            all_paths.append(list(m))

        holding = all_paths.copy()
        all_paths = []
        for m0, m1 in chain_maps[(k1, k2)]:
            for item in holding:
                if item[-1] == m0:
                    v = item.copy()
                    v.append(m1)
                    all_paths.append(v)

        holding = all_paths.copy()
        all_paths = []
        for m0, m1 in chain_maps[(k2, k0)]:
            for item in holding:
                # if item[-1] == m0:
                #     v = item.copy()
                #     v.append(m1)
                #     all_paths.append(v)
                if m1 == item[0]:
                    v = item.copy()
                    v.append(m1)
                    all_paths.append(v)

        
        for it in all_paths:
            a = figuratives[k0]
            b = figuratives[k1]
            c = figuratives[k2]
            ia = it[0]
            ib = it[1]
            ic = it[2]
            idd = it[3]
            print(a[ia], b[ib], c[ic], a[idd] ,it)
        
