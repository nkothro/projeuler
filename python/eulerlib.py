
"""
Collection of tools and such that end up being useful in solving Project
Euler problems
"""

import itertools
import typing as t
from math import ceil
from collections import Counter

import numpy as np


class PrimesList:

    def __init__(self, num):
        self.list = self.primesfrom2to(num)

    @staticmethod
    def primesfrom2to(n):
        """Input n>=6, Returns a array of primes, 2 <= p < n"""
        sieve = np.ones(n // 3 + (n % 6 == 2), dtype=bool)
        for i in range(1, int(n**0.5) // 3 + 1):
            if sieve[i]:
                k = 3 * i + 1 | 1
                sieve[k * k // 3 :: 2 * k] = False
                sieve[k * (k - 2 * (i & 1) + 4) // 3 :: 2 * k] = False
        return np.r_[2, 3, ((3 * np.nonzero(sieve)[0][1:] + 1) | 1)]

    def __len__(self):
        return len(self.list)

    def __getitem__(self, j):
        return self.list[j]

    def nthprime(self, n):
        """
        Find the vale of the nth prime
        :param n:
        :return: value of the `n`th prime.
        """
        return self.list[n - 1]

    def less_than(self, num):
        # THIS MIGHT NOT ACTUALLY BE ALL THAT USEFUL
        """

        :param num:
        :return: List of all primes strictly less than `num`, exclusive of `num` should it be prime
        """
        # create index for place to cut in half
        midl = ceil(self.list.size() * 0.5)
        low = 0
        high = self.list.size() - 1
        while high - low > 5:
            if num < self.list[midl]:
                low = low
                high = midl
                midl = ceil((midl - low) * 0.5)
            elif num > self.list[midl]:
                low = midl
                high = high
                midl = ceil((high - midl) * 0.5)
            elif num == self.list[midl]:
                indx = midl - 1  # since want all exclusively less than num
            else:
                break

        for i in self.list[low:high]:
            if self.list[i] < num and num < self.list[i + 1]:
                indx = i

        less_than_list = self.list[:indx]
        return less_than_list

    def is_prime(self, n):
        """
        For a given n, is n prime?
        :param n: Number to test for prime
        :return: Boolean, True if n is prime, False if n is not prime
        """
        if not (self.list[0] < n < self.list[-1]):
            return ValueError(
                f"{n} is not testable given the range of numbers this instance of PrimeList was constructed with"
            )

        k = np.searchsorted(self.list, n, side="left")
        return n == self.list[k]


def prime_factors_of(num: int, primelist: PrimesList = None):
    """
    Find the prime factors of a given number
    :param num: the number to find prime factors of
    :param primelist: optional argument, assumes providing a list of at least all prime numbers
     less than `num`. Use this to only generate one primeslist externally, as opposed
     to generating a new list of primes each time this function is called
    :return: list of prime factors of `num`
    """
    if not primelist:
        # If no primelist is given in the function input,
        # generate a list of all prime numbers less than or equal to `num`
        primelist = PrimesList(num)

    # List for storing prime factors
    prime_factors = []
    for p in primelist:
        # If p is a factor of `num`, append it to the lis
        if num % p == 0:
            prime_factors.append(p)

    # Return the list of prime factors
    return prime_factors


def prime_factorization(num: int, primelist: PrimesList = None):
    # upper = int(np.floor(np.sqrt(num)))
    if not primelist:
        primelist = PrimesList(num + 1)

    # if primelist.is_prime(num):
    #     return [(num,1)]

    prime_base_exp = Counter() 
    i = num
    idx = 0
    while i > 1:
        p = primelist.list[idx]
        exp = 0
        d, r = divmod(i, p)
        while r == 0:
            i = d
            exp += 1
            d, r = divmod(i, p)

        if exp > 0:
            prime_base_exp[p] = exp

        if i <= 1:
            break

        idx += 1
    return prime_base_exp

def prime_factorization_with_lut(
        num: int,
        pf_lut: dict,
        primelist: PrimesList = None,
):
    if not primelist:
        primelist = PrimesList(num + 1)
    
    # check if already exists in lut
    try:
        return pf_lut[num]
    except KeyError:
        pass

    pf_counter = Counter()
    if primelist.is_prime(num):
        pf_counter[num] = 1

    else:
        i = num
        idx = 0
        while i > 1:
            p = primelist.list[idx]
            # print(i, idx, p,  pf_counter)
            d, r = divmod(i, p)
            while r == 0:
                pf_counter[p] += 1
                try:
                    pf_counter += pf_lut[d]
                    pf_lut[num] = pf_counter
                    return pf_counter
                except KeyError:
                    i = d
                    pf_lut[num//i] = pf_counter.copy()
                    d, r = divmod(i, p)
            idx += 1
            if i <= 1:
                break
    
    pf_lut[num] = pf_counter
    return pf_counter




def polygonal(s, n: int or np.ndarray):
    """
    Polygonal numbers. For example, triangular numbers are given when s=3, hexagonal when s=6.
    """
    return ((s - 2) * np.square(n) - (s - 4) * n) // 2


def cyclic_permutations(I: t.Iterable):
    """
    Cyclic permutations being permutations where order matters, but there is no
    "first" or "last" element.  For example: "ABCDEF" and "DEFABC" are
    considered the same permutaion because the order is the same when you
    consider wrapping.
    """
    i0 = I[0]
    for p in itertools.permutations(I[1:]):
        yield i0, *p


def next_permutation(seq, pred=None):
    """Like C++ std::next_permutation() but implemented as
    generator. Yields copies of seq."""

    def reverse(seq, start, end):
        # seq = seq[:start] + reversed(seq[start:end]) + \
        #       seq[end:]
        end -= 1
        if end <= start:
            return
        while True:
            seq[start], seq[end] = seq[end], seq[start]
            if start == end or start+1 == end:
                return
            start += 1
            end -= 1
    if not pred:
        def pred(a, b):
            if a < b:
                return -1
            return 0

    if not seq:
        raise StopIteration

    try:
        seq[0]
    except TypeError:
        raise TypeError("seq must allow random access.")

    first = 0
    last = len(seq)
    seq = seq[:]

    # Yield input sequence as the STL version is often
    # used inside do {} while.
    yield seq
    
    if last == 1:
        raise StopIteration

    while True:
        next = last - 1

        while True:
            # Step 1.
            next1 = next
            next -= 1
            
            if pred(seq[next], seq[next1]) < 0:
                # Step 2.
                mid = last - 1
                while not (pred(seq[next], seq[mid]) < 0):
                    mid -= 1
                seq[next], seq[mid] = seq[mid], seq[next]
                
                # Step 3.
                reverse(seq, next1, last)

                # Change to yield references to get rid of
                # (at worst) |seq|! copy operations.
                yield seq[:]
                break
            if next == first:
                return
                raise StopIteration
    raise StopIteration

def gen_factorial_list(n):
    # list up to n!
    f = [1]
    j = 1
    while len(f) < n:
        f.append(f[-1] * j)
        j += 1

    return f

class FactoradixCounter:
    def __init__(self, initial_value, places):
        self._f_ref = gen_factorial_list(places)
        self._v = self.from_int(initial_value, self._f_ref)

    @staticmethod
    def from_int(n: int, factorials: list = None):
        if factorials is None:
            factorials = [1]
            j = 1
            while factorials[-1] < n:
                factorials.append(factorials[-1] * j)
                j += 1

        ln = len(factorials)
        r = n
        factoradix = []
        for j in range(ln):
            q, r = divmod(r, factorials[~j])
            factoradix.append(q)

        # while factoradix[0] == 0:
        #     del factoradix[0]

        return list(reversed(factoradix))

    def to_decimal(self):
        return sum(val * self._f_ref[j] for j, val in enumerate(self._v))

    @property
    def list_repr(self):
        return self._v

    def __str__(self):
        return ":".join(str(k) for k in reversed(self.list_repr))

    def __repr__(self):
        return ":".join(str(k) for k in reversed(self.list_repr))

    def increment(self):
        # increment by one
        l = len(self.list_repr)
        self.list_repr[0] += 1
        for j, v in enumerate(self.list_repr):
            if v > j:
                self.list_repr[j] = 0
                if l <= j + 1:
                    self.list_repr.append(0)
                self.list_repr[j + 1] += 1
            else:
                break

    def apply_as_permuation(self, iter):
        digits = len(self.list_repr)
        d = list(iter)
        perm = []
        j = digits
        # while j > 0:
        #     j -= 1
        #     k = self.list_repr[j]
        #     perm.append(d[k])
        #     del d[k]

        for j in list(reversed(self.list_repr)):
            perm.append(d[j])
            del d[j]

        return tuple(perm)

    def roll_to_even_place(self, index_to_roll):
        # at specific place in factoradix representation, increment and zero out
        # all following, less significant values in their place
        self._v[~index_to_roll] += 1
        j = ~index_to_roll
        j += 1
        while j < len(self._v):
            self._v[j] = 0
            j += 1

        l = len(self.list_repr)
        for j, v in enumerate(self.list_repr):
            if v > j:
                self.list_repr[j] = 0
                if l <= j + 1:
                    self.list_repr.append(0)
                self.list_repr[j + 1] += 1
            else:
                break

## IS SQUARE ROOT?
# adapted from https://stackoverflow.com/a/424936/10105931

USE_NP_LUT = False
start = [
            1, 3, 1769, 5, 1937, 1741, 7, 1451,
            479, 157, 9, 91, 945, 659, 1817, 11,
            1983, 707, 1321, 1211, 1071, 13, 1479, 405,
            415, 1501, 1609, 741, 15, 339, 1703, 203,
            129, 1411, 873, 1669, 17, 1715, 1145, 1835,
            351, 1251, 887, 1573, 975, 19, 1127, 395,
            1855, 1981, 425, 453, 1105, 653, 327, 21,
            287, 93, 713, 1691, 1935, 301, 551, 587,
            257, 1277, 23, 763, 1903, 1075, 1799, 1877,
            223, 1437, 1783, 859, 1201, 621, 25, 779,
            1727, 573, 471, 1979, 815, 1293, 825, 363,
            159, 1315, 183, 27, 241, 941, 601, 971,
            385, 131, 919, 901, 273, 435, 647, 1493,
            95, 29, 1417, 805, 719, 1261, 1177, 1163,
            1599, 835, 1367, 315, 1361, 1933, 1977, 747,
            31, 1373, 1079, 1637, 1679, 1581, 1753, 1355,
            513, 1539, 1815, 1531, 1647, 205, 505, 1109,
            33, 1379, 521, 1627, 1457, 1901, 1767, 1547,
            1471, 1853, 1833, 1349, 559, 1523, 967, 1131,
            97, 35, 1975, 795, 497, 1875, 1191, 1739,
            641, 1149, 1385, 133, 529, 845, 1657, 725,
            161, 1309, 375, 37, 463, 1555, 615, 1931,
            1343, 445, 937, 1083, 1617, 883, 185, 1515,
            225, 1443, 1225, 869, 1423, 1235, 39, 1973,
            769, 259, 489, 1797, 1391, 1485, 1287, 341,
            289, 99, 1271, 1701, 1713, 915, 537, 1781,
            1215, 963, 41, 581, 303, 243, 1337, 1899,
            353, 1245, 329, 1563, 753, 595, 1113, 1589,
            897, 1667, 407, 635, 785, 1971, 135, 43,
            417, 1507, 1929, 731, 207, 275, 1689, 1397,
            1087, 1725, 855, 1851, 1873, 397, 1607, 1813,
            481, 163, 567, 101, 1167, 45, 1831, 1205,
            1025, 1021, 1303, 1029, 1135, 1331, 1017, 427,
            545, 1181, 1033, 933, 1969, 365, 1255, 1013,
            959, 317, 1751, 187, 47, 1037, 455, 1429,
            609, 1571, 1463, 1765, 1009, 685, 679, 821,
            1153, 387, 1897, 1403, 1041, 691, 1927, 811,
            673, 227, 137, 1499, 49, 1005, 103, 629,
            831, 1091, 1449, 1477, 1967, 1677, 697, 1045,
            737, 1117, 1737, 667, 911, 1325, 473, 437,
            1281, 1795, 1001, 261, 879, 51, 775, 1195,
            801, 1635, 759, 165, 1871, 1645, 1049, 245,
            703, 1597, 553, 955, 209, 1779, 1849, 661,
            865, 291, 841, 997, 1265, 1965, 1625, 53,
            1409, 893, 105, 1925, 1297, 589, 377, 1579,
            929, 1053, 1655, 1829, 305, 1811, 1895, 139,
            575, 189, 343, 709, 1711, 1139, 1095, 277,
            993, 1699, 55, 1435, 655, 1491, 1319, 331,
            1537, 515, 791, 507, 623, 1229, 1529, 1963,
            1057, 355, 1545, 603, 1615, 1171, 743, 523,
            447, 1219, 1239, 1723, 465, 499, 57, 107,
            1121, 989, 951, 229, 1521, 851, 167, 715,
            1665, 1923, 1687, 1157, 1553, 1869, 1415, 1749,
            1185, 1763, 649, 1061, 561, 531, 409, 907,
            319, 1469, 1961, 59, 1455, 141, 1209, 491,
            1249, 419, 1847, 1893, 399, 211, 985, 1099,
            1793, 765, 1513, 1275, 367, 1587, 263, 1365,
            1313, 925, 247, 1371, 1359, 109, 1561, 1291,
            191, 61, 1065, 1605, 721, 781, 1735, 875,
            1377, 1827, 1353, 539, 1777, 429, 1959, 1483,
            1921, 643, 617, 389, 1809, 947, 889, 981,
            1441, 483, 1143, 293, 817, 749, 1383, 1675,
            63, 1347, 169, 827, 1199, 1421, 583, 1259,
            1505, 861, 457, 1125, 143, 1069, 807, 1867,
            2047, 2045, 279, 2043, 111, 307, 2041, 597,
            1569, 1891, 2039, 1957, 1103, 1389, 231, 2037,
            65, 1341, 727, 837, 977, 2035, 569, 1643,
            1633, 547, 439, 1307, 2033, 1709, 345, 1845,
            1919, 637, 1175, 379, 2031, 333, 903, 213,
            1697, 797, 1161, 475, 1073, 2029, 921, 1653,
            193, 67, 1623, 1595, 943, 1395, 1721, 2027,
            1761, 1955, 1335, 357, 113, 1747, 1497, 1461,
            1791, 771, 2025, 1285, 145, 973, 249, 171,
            1825, 611, 265, 1189, 847, 1427, 2023, 1269,
            321, 1475, 1577, 69, 1233, 755, 1223, 1685,
            1889, 733, 1865, 2021, 1807, 1107, 1447, 1077,
            1663, 1917, 1129, 1147, 1775, 1613, 1401, 555,
            1953, 2019, 631, 1243, 1329, 787, 871, 885,
            449, 1213, 681, 1733, 687, 115, 71, 1301,
            2017, 675, 969, 411, 369, 467, 295, 693,
            1535, 509, 233, 517, 401, 1843, 1543, 939,
            2015, 669, 1527, 421, 591, 147, 281, 501,
            577, 195, 215, 699, 1489, 525, 1081, 917,
            1951, 2013, 73, 1253, 1551, 173, 857, 309,
            1407, 899, 663, 1915, 1519, 1203, 391, 1323,
            1887, 739, 1673, 2011, 1585, 493, 1433, 117,
            705, 1603, 1111, 965, 431, 1165, 1863, 533,
            1823, 605, 823, 1179, 625, 813, 2009, 75,
            1279, 1789, 1559, 251, 657, 563, 761, 1707,
            1759, 1949, 777, 347, 335, 1133, 1511, 267,
            833, 1085, 2007, 1467, 1745, 1805, 711, 149,
            1695, 803, 1719, 485, 1295, 1453, 935, 459,
            1151, 381, 1641, 1413, 1263, 77, 1913, 2005,
            1631, 541, 119, 1317, 1841, 1773, 359, 651,
            961, 323, 1193, 197, 175, 1651, 441, 235,
            1567, 1885, 1481, 1947, 881, 2003, 217, 843,
            1023, 1027, 745, 1019, 913, 717, 1031, 1621,
            1503, 867, 1015, 1115, 79, 1683, 793, 1035,
            1089, 1731, 297, 1861, 2001, 1011, 1593, 619,
            1439, 477, 585, 283, 1039, 1363, 1369, 1227,
            895, 1661, 151, 645, 1007, 1357, 121, 1237,
            1375, 1821, 1911, 549, 1999, 1043, 1945, 1419,
            1217, 957, 599, 571, 81, 371, 1351, 1003,
            1311, 931, 311, 1381, 1137, 723, 1575, 1611,
            767, 253, 1047, 1787, 1169, 1997, 1273, 853,
            1247, 413, 1289, 1883, 177, 403, 999, 1803,
            1345, 451, 1495, 1093, 1839, 269, 199, 1387,
            1183, 1757, 1207, 1051, 783, 83, 423, 1995,
            639, 1155, 1943, 123, 751, 1459, 1671, 469,
            1119, 995, 393, 219, 1743, 237, 153, 1909,
            1473, 1859, 1705, 1339, 337, 909, 953, 1771,
            1055, 349, 1993, 613, 1393, 557, 729, 1717,
            511, 1533, 1257, 1541, 1425, 819, 519, 85,
            991, 1693, 503, 1445, 433, 877, 1305, 1525,
            1601, 829, 809, 325, 1583, 1549, 1991, 1941,
            927, 1059, 1097, 1819, 527, 1197, 1881, 1333,
            383, 125, 361, 891, 495, 179, 633, 299,
            863, 285, 1399, 987, 1487, 1517, 1639, 1141,
            1729, 579, 87, 1989, 593, 1907, 839, 1557,
            799, 1629, 201, 155, 1649, 1837, 1063, 949,
            255, 1283, 535, 773, 1681, 461, 1785, 683,
            735, 1123, 1801, 677, 689, 1939, 487, 757,
            1857, 1987, 983, 443, 1327, 1267, 313, 1173,
            671, 221, 695, 1509, 271, 1619, 89, 565,
            127, 1405, 1431, 1659, 239, 1101, 1159, 1067,
            607, 1565, 905, 1755, 1231, 1299, 665, 373,
            1985, 701, 1879, 1221, 849, 627, 1465, 789,
            543, 1187, 1591, 923, 1905, 979, 1241, 181,
        ]
if USE_NP_LUT:
    start = np.array( start ,dtype=np.int64) 

bad255 = np.array(
    [
        0,0,1,1,0,1,1,1,1,0,1,1,1,1,1,0,0,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,
        1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,1,1,
        0,1,0,1,1,0,0,1,1,1,1,1,0,1,1,1,1,0,1,1,0,0,1,1,1,1,1,1,1,1,0,1,
        1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,0,0,1,1,1,1,1,1,
        1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,
        1,1,1,1,1,1,0,1,1,0,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,
        1,1,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,
        1,0,1,1,1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,
        0,1,1,0,1,1,1,1,0,1,1,1,1,1,0,0,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,1,
        1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,1,1,0,
        1,0,1,1,0,0,1,1,1,1,1,0,1,1,1,1,0,1,1,0,0,1,1,1,1,1,1,1,1,0,1,1,
        1,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,0,0,1,1,1,1,1,1,1,
        1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,
        1,1,1,1,1,0,1,1,0,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,
        1,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,
        0,1,1,1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,0,
    ],
    dtype=bool,
)
def is_perfect_square(x): 
    # quickfail
    if (x<0) or (x&2) or ((x&7)==5) or ((x&11)== 8):
        return False
    if(x==0):
        return True

    y = x
    # y = (y & ((1<<128)-1)) + (y >> 128) ## ATTEMPT TO MAKE 128bit INT COMPATIBLE
    # y = (y & 18446744073709551615) + (y >> 64) ## ATTEMPT TO MAKE 64bit INT COMPATIBLE
    y = (y & 4294967295) + (y >> 32);
    y = (y & 65535) + (y >> 16);
    y = (y & 255) + ((y >> 8) & 255) + (y >> 16);

    try:
        if bad255[y]:
            return False;

    except IndexError as e:
        print(x, y, e)
        raise Exception


    # Divide out powers of 4 using binary search
    # if((x & ((1<<128)-1)) == 0): ## ATTEMPT TO MAKE 128bit INT COMPATIBLE
        # x >>= 128 
    # if((x & 18446744073709551615) == 0): ## ATTEMPT TO MAKE 64bit INT COMPATIBLE
        # x >>= 64
    if((x & 4294967295) == 0):
        x >>= 32
    if((x & 65535) == 0):
        x >>= 16
    if((x & 255) == 0):
        x >>= 8
    if((x & 15) == 0):
        x >>= 4
    if((x & 3) == 0):
        x >>= 2

    if((x & 7) != 1):
        return False

    r = start[(x >> 3) & 1023]

    # originally a do-while in the C++ code
    z = x - r*r
    if z == 0:
        return True
    if x < 0:
        return False
    t = z & (-z)
    r += (z & t) >> 1
    if(r > (t >> 1)):
        r = t-r

    while (t <= (1 << 33)):
        z = x - r*r
        if z == 0:
            return True
        if x < 0:
            return False
        t = z & (-z)
        r += (z & t) >> 1
        if(r > (t >> 1)):
            r = t-r
    
    return False

class PascalsTriangle:
    # TODO: take advantage of symmetry
    def __init__(self, rows):
        self._array =  []
        for j in range(rows):
            self.add_row()

    def add_row(self):
        if len(self._array) == 0:
            new_row = [1]
        
        else:
            new_row_len = len(self._array) + 1
            new_row_idx = new_row_len - 1

            new_row = [0] * new_row_len
            previous_row = self._array[-1]

            for i in range(new_row_len):
                if i==0 or i==new_row_idx:
                    new_row[i] = 1
                else:
                    new_row[i] = previous_row[i] + previous_row[i-1]
        
        self._array.append(new_row)
        # print(new_row)

    def get_val(self, n, k):
        if k > (n//2) + 1:
            return self._array[n][n-k]
        
        return self._array[n][ k]

class MemoryOptimizedPascalsTriangle:
    # TODO: take advantage of symmetry
    def __init__(self):
        self._row = [1]
    
    @property
    def current_row_idx(self):
        return len(self._row) -1
    
    @property
    def current_row(self):
        return self._row
    
    def next_row(self):
        new_row_idx = self.current_row_idx + 1
        new_row_len = new_row_idx + 1
        new_row = [0] * new_row_len

        for i in range(new_row_len):
            if i==0 or i==new_row_idx:
                new_row[i] = 1
            else:
                new_row[i] = self.current_row[i] + self.current_row[i-1]
            
        self._row = new_row

    def get_value(self, n, k):
        if self.current_row_idx != n:
            raise ValueError(f'Not in that row right now (current row is {self.current_row_idx}, given n is {n}, current row len is {len(self.current_row)})')
        
        return self.current_row[k]
