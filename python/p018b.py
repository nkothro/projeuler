import pathlib as pl
import numpy as np

tree = np.loadtxt(pl.Path('python/p018.csv'), dtype=float, delimiter=',')
running_row = tree[-1].copy()
decision_tree = np.zeros_like(tree)

row_ct = tree.shape[0]

for j, row in enumerate(tree[:-1,:]):
    next_row = tree[j+1]
    for k, val in enumerate(row):
        if val > 0:
            try:
                if next_row[k] > next_row[k+1]:
                    decision_tree[j,k] = k
                elif next_row[k] < next_row[k+1]:
                    decision_tree[j,k] = k+1
                else:
                    decision_tree[j,k] = k
            except IndexError:
                decision_tree[j,k] = k
        else:
            decision_tree[j,k] = np.NaN

print(tree)
print(decision_tree)

idx_lst = []

next_idx = 0
for j, row in enumerate(tree[:,:]):
    idx_lst.append(next_idx)
    next_idx = decision_tree[j, int(next_idx)]

vals = [int(tree[j, int(k)]) for j,k in enumerate(idx_lst)]
print(f'{sum(vals)} is the sum of {vals}')


