import numpy as np

from eulerlib import PrimesList

maxN = 50_000_000
max_test_divisor = 10*int(np.sqrt(2*(maxN**2) - 1))

def long_prime_test(x: int, pl:PrimesList):
    if x < pl.list[-1]:
        return pl.is_prime(x)

    # for prime in pl:
    #     if x % prime == 0:
    #         return False

    if np.any(np.mod(x, pl.list)):
        return False
    
    upper = int(np.sqrt(x))
    for j in range(pl.list[-1], upper+1):
        if x % j == 0:
            return False
    
    return True

def t(n):
    return 2*n**2 - 1

pl = PrimesList(14)

p = 1
for pi in pl:
    p *= int(pi)

print(p)

for j in range(p):
    if np.gcd(p, j) == 1:
        print(j)