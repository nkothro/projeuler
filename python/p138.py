from math import floor

def test_base_length(b):
    if b & 1:
        return False, 0,0

    h_plus = b+1
    h_minus = b-1

    b2 = (b/2)**2

    L_sq_plus = h_plus**2 + b2
    L_sq_minus = h_minus**2 + b2

    L_minus = floor(L_sq_minus**0.5)
    r_minus = L_minus if ((L_minus **2 == L_sq_minus) and h_minus > 0) else 0

    L_plus = floor(L_sq_plus**0.5)
    r_plus = L_plus if ((L_plus **2 == L_sq_plus) and h_plus > 1) else 0

    return b, r_minus, r_plus

def via_gen_fibo(count):
    #a(n) = 18*a(n-1) - a(n-2), n > 1, a(0)=1, a(1)=17.
    
    a = [1, 17]
    while len(a) < count:
        a.append(  18*a[-1] - a[-2])
    return a


if __name__ == '__main__':
    import multiprocessing

    x = via_gen_fibo(17)
    print(sum(x[1:13]))

    
    # upper = 10**8
    # with multiprocessing.Pool(processes=10) as pool:
    #     tot = 0
    #     Ls = []
    #     for j, (b, rp, rm) in enumerate(pool.imap_unordered(test_base_length, range(2,upper,2), chunksize=100)):
    #         if rp or rm:
    #             if rp:
    #                 Ls.append(rp)
    #             if rm:
    #                 Ls.append(rm)
    #             print(b)
    #         if j % 100_000 == 0:
    #             print(j/upper)
        
    #         if len(Ls) > 20:
    #             break

    #     print(Ls)
    #     print(sum(Ls[0:12]))