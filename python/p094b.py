import multiprocessing

import eulerlib


"""
using Heron's forumula, with s = (1/2)*(a+b+c):
    - for triangle with side lengths (n, n+1, n+1), A**2 = s*(s-a)*(s-b)*(s-c)
    is integer when n even. 
    - for triangle with side lengths (n, n, n+1), A**2 = s*(s-a)*(s-b)*(s-c)
    is integer when n odd
"""
def area_squared(n: int):
    if n & 1:  # ODD
        #n,n,n+1
        a2_times_16 = (3 * n + 1) * (n + 1) ** 2 * (n - 1)
    else:
        # n, n+1, n+1
        a2_times_16 = (3 * n + 2) * (n+2) * (n**2)

    # bitshift by 4 is same as divide by 16
    return a2_times_16 >> 4

def herons(a, b, c):
    s = (a + b + c)/2
    A = (s*(s-a)*(s-b)*(s-c))**0.5
    return A

def wrapper(n):
    a2 = area_squared(n)
    if eulerlib.is_perfect_square(a2):
        p = ((3*n)+1) if (n & 1) else ((3*n) + 2)
        return p
    
    return 0

if __name__ == '__main__':
    # for j in range(2,10**9):
    #     a2 = area_squared(j)
    #     if eulerlib.is_perfect_square(a2):
    #         s = f'{j=:>20_}: area is {a2:>55_} == {int(a2**0.5):>28_}^2'
    #         print(s)

    k = 5
    bs = 2**k
    test_sq = ((1<<bs)+3)**2
    print(test_sq,eulerlib.is_perfect_square(test_sq))