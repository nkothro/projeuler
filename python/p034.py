#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from math import factorial as fact


def digit_factorial_sum(num: int):
    num = int(num)
    numlist = list(str(num))
    tot = 0
    for d in numlist:
        tot += fact(int(d))

    return tot

def ratios_all_ones(ndigits) -> list:
    denom = int(''.join(['1']*ndigits))
    return ndigits / denom

def ratios_all_same(number, max_digits, invert=False) :
    strnum = str(number)
    f = fact(number)

    num = 0
    str_denom = ''

    x = []
    l = []
    while len(l) < max_digits:
        str_denom += strnum
        num += f

        x.append(len(str_denom))
        if invert:
            l.append(num / int(str_denom))
        else:
            l.append(int(str_denom) / num)

    return x, l



if __name__ == "__main__":
    pass_list = []
    for num in range(3, 10**8):
        if num == digit_factorial_sum(num):
            print(num, num / 10**8 * 100)
            pass_list.append(num)

    print(pass_list)

    # m = 25

    # for digit in range(1, 10):
    #     x, y = ratios_all_same(digit, m, invert=False)
    #     plt.plot(x, y, label = f'all {digit}')

    # plt.legend()
    # plt.yscale('log')
    # plt.show()
