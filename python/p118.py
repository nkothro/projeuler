
# from primes.primelist import PrimesList
from eulerlib import PrimesList
from itertools import combinations, permutations, product
import numpy as np

import sqlite3
import pathlib as pl

from itertools import product, combinations
from collections import Counter

from eulerlib import PrimesList


# p = PrimesList(10**9)
# print(p.list.size)

def increasing_combos(I, r):
    iterlen = len(I)
    r2 = r - 1
    first_upper = I[r]

    for k in range(first_upper, iterlen):
        i0 = I[k]
        for i in combinations(I[:k], r2):
            yield i0, *i

def test_pandigital_prime_set(primeset):
    s = []
    for p in primeset:
        s += list(str(p))
    return ''.join(sorted(s)) == '123456789'

def value_in_sorted_nparr(v, arr):
    k = np.searchsorted(arr, v)
    try:
        return arr[k] == v
    except IndexError:
        return False

if __name__ == '__main__':
    plist = PrimesList(10**9)
    print('orgiginal plist: ', len(plist.list))

    b = np.zeros_like(plist.list)

    filtered = []
    for j, pi in enumerate(plist):
        s = str(pi)
        if '0' not in s:
            if len(s)==len(set(s)):
                filtered.append(pi)
    filtered = np.asarray(filtered, dtype=int)
                
    
    print('filtered: ', len(filtered))


    from datetime import datetime
    t0 = datetime.now()
    z = []
    perm_string = '123456789'

    for perm in permutations(perm_string):
        for b in product(['', ','], repeat=len(perm_string)-1):
            s = ''
            s += perm[0]
            for j, bi in enumerate(b):
                s += bi
                s+= perm[j+1]
            n = set(int(si) for si in s.split(','))
            if all(value_in_sorted_nparr(ni, filtered) for ni in n):
                z.append(n)

    print(z)
    print(len(z))
    with open('psets.txt', 'w') as f:
        for s in z:
            f.write(str(s) + '\n')
    elapsed = datetime.now()-t0

def unique_digits_no_zeros(num):
    s = str(num)
    if '0' in s:
        return False

    return len(s) == len(set(s))

def generate_primes_unique_digits_db(dbname:pl.Path, upper_bound:int):
    db = sqlite3.connect('./unique_digit_primes.db')
    cur = db.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS primes(num PRIMARY KEY, contains1, contains2, contains3, contains4, contains5, contains6, contains7, contains8, contains9, digits);')

    p = PrimesList(upper_bound)
    filtered = [n for n in p if unique_digits_no_zeros(n)]

    for n in filtered:
        s = str(n)
        digicount = len(s)
        d = {f'contains{j}': str(j) in s for j in range(1,10)}
        insert_st = f"""
        INSERT INTO primes(num, contains1, contains2, contains3, contains4, contains5, contains6, contains7, contains8, contains9, digits)
        VALUES
        ({n},
        {d['contains1']},
        {d['contains2']},
        {d['contains3']},
        {d['contains4']},
        {d['contains5']},
        {d['contains6']},
        {d['contains7']},
        {d['contains8']},
        {d['contains9']}, {digicount}); """
        cur.execute(insert_st)
    
    db.commit()
    db.close()

def generate_partitions(n):
    # I'm sure there is a more efficient generation scheme for paritions, 
    # but I'm on a plane and don't have wifi, this will suffice
    partitions = []
    breaks = product([True, False], repeat=8)
    for pattern in breaks:
        part = []
        s = 1
        for b in pattern:
            if b:
                part.append(s)
                s = 1
            else:
                s += 1

        partitions.append(tuple(sorted(part)))

    return list(set(partitions))

def iterate_through_partition(connection, partition):
    # p0 = partition[0]    
    connection.row_factory = sqlite3.Row
    cur = connection.cursor()

    # TODO: use itertools.combinations instead of iterating over entire product,
    # less repeats to test that way
    counter = Counter(partition)
    views = {}
    it = []
    for key, ct in counter.items():
        sql = f"SELECT * FROM primes WHERE digits = {key};"
        res = cur.execute(sql).fetchall()
        views[key] = [r['num'] for r in res]
        it += list(combinations(views[key], ct))
        print(it)
    
    unique_prime_sets = []
    for possible_set in product(*it):
        print(possible_set)
        if pandigital_set_of_nums(possible_set): 
            unique_prime_sets.append(set(possible_set))
    # print(len(unique_prime_sets)) 
    return unique_prime_sets
    

def pandigital_set_of_nums(numlist):
    bigstr = ''.join(str(i) for i in numlist)
    big_len = len(bigstr)
    if big_len == 9:
        return big_len == len(set(bigstr)) 
    return False

if __name__ == '__main__':
    dbname = pl.Path('./unique_digit_primes.db')
    upper = 10**9

    if not dbname.exists():
        generate_primes_unique_digits_db(dbname, upper)
    db = sqlite3.connect(dbname)
    db.row_factory = sqlite3.Row

    # partitions
    # 8 spots to put a stick in 9 ball string
    partitions = generate_partitions(9)

    # cur = db.cursor()
    # sql = "SELECT * FROM primes;"
    # for row in db.cursor().execute(sql):
    #     print(row['num'])

    prime_sets = []
    for p in partitions:
        print(p)
        prime_sets += iterate_through_partition(db, p)
        break