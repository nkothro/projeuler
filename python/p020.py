import math
def sum_of_digits(n):
    s = list(str(n))
    return sum([int(k) for k in s])

if __name__ == '__main__':

    s = 1
    for n in range(1,101):
        s *= n
    print(sum_of_digits(s))
    

    
