# PROBLEM STATEMENT:
#The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17
#
#Find the sum of all primes below two million

from primes import primelist
k = primelist.PrimesList(2_000_000).list
print(sum(k))