# I HAVE NO IDEA HOW THE FCK THIS WORKS. BUT IT COMES FROM THE OEIS PAGE FOR A051626
import numpy as np

from sympy import multiplicity, n_order
def A051626(n): 

    return 0 if (m:=(n>>(~n & n-1).bit_length())//5**multiplicity(5, n)) == 1 else n_order(10, m)

l = [0]
l.extend(  [A051626(d) for d in range(1,1000)])

print(np.argmax(np.asarray(l)))
