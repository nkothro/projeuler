from math import comb, floor, log10
from itertools import permutations, product

def figurate_number(r, n):
    return comb(n+(r-1),r)

def triangle(n):
    return int(n*(n+1)/2)

def square(n):
    return int(n**2)

def pentagonal(n):
    return int(n*(3*n - 1)/2)

def hexagonal(n):
    return int(n*(2*n - 1))

def heptagonal(n):
    return int(n*((5*n) -3)/2)

def octagonal(n):
    return int(n*((3*n)-1))

def is_cyclic_link(a, b)->bool:
    A = str(a)[-2] + str(a)[-1]
    B = str(b)[0]  + str(b)[1]
    if A == B:
        return True
    else:
        return False

def is_cyclic(ordered_list:list)->bool:
    """
    Takes list of integers, test if they are cyclic
    as described in Problem 61
    """
    count = len(ordered_list)
    cyclic = True
    test_list = [str(num) for num in ordered_list]
    for j in range(count):
        a = test_list[j-1]
        b = test_list[j]
        if (a[-2]+a[-1]) != (b[0] + b[1]):
            return False
    return cyclic


if __name__ == '__main__':
    # Want list of all 4 digit numbers in each number type
    triangulars = []
    squares = []
    pentagonals = []
    hexagonals = []
    heptagonals = []
    octagonals = []
    for i in range(1,1000):
        if len(str(triangle(i))) == 4:
            triangulars.append(triangle(i))
        if len(str(square(i))) == 4:
            squares.append(square(i))
        if len(str(pentagonal(i))) == 4:
            pentagonals.append(pentagonal(i))
        if len(str(hexagonal(i))) == 4:
            hexagonals.append(hexagonal(i))
        if len(str(heptagonal(i))) == 4:
            heptagonals.append(heptagonal(i))
        if len(str(octagonal(i))) == 4:
            octagonals.append(octagonal(i))

    superlist = [ triangulars, squares, pentagonals, hexagonals, heptagonals, octagonals ]
    idx_src = list(range(6))
    circulars = [idx_src[:1]+list(perm) for perm in permutations(idx_src[1:])]

    not_found = True
    circulars_iterator = 0
    while not_found == True:
        i, j, k, l, m, n = tuple(circulars[circulars_iterator])

        for num0 in superlist[i]:
            for num1 in superlist[j]:
                if not_found and is_cyclic_link(num0, num1):
                    for num2 in superlist[k]:
                        if not_found and is_cyclic_link(num1, num2):
                            for num3 in superlist[l]:
                                if not_found and is_cyclic_link(num2, num3):
                                    for num4 in superlist[m]:
                                        if not_found and is_cyclic_link(num3, num4):
                                            for num5 in superlist[n]:
                                                if not_found and is_cyclic_link(num4, num5):
                                                    if not_found and is_cyclic_link(num5, num0):
                                                        not_found=True
                                                        print(num0, num1, num2, num3, num4, num5, 'sum is: ', sum([num0, num1, num2, num3, num4, num5]))

        circulars_iterator += 1

    for i in range(20):
        print(triangle(i+1)- triangle(i))