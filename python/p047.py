import primes.primelist

from collections import Counter


def prime_factors(num: int, primeslist: list):
    if num == 0 or  num == 1:
        return []

    j = 0
    pfactors = []

    while num != 1:
        if num % primeslist[j] == 0:
            pfactors.append(primeslist[j])
            num = num / primeslist[j]
        else:
            j += 1

    return pfactors

def prime_factors3(n:int, plist):
    j = 0 
    factors = []
    pj = plist[j]
    while pj *pj <= n:
        if n % pj:
            j += 1
            pj = plist[j]
        else:
            n //= pj
            factors.append(pj)
    if n > 1:
        factors.append(n)
    
    return Counter(factors)

def prime_factors2(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return Counter(factors)

def test_distinct(set_list: list):
    set0 = set_list[0]
    for setj in set_list[1:]:
        if (set0 & setj) == set():
             set0 = set0 | setj
        else:
            return False
    return True

def distinct_primes_counter(n:int, max_p:int):
    """
    :param n: number of distinct prime factors
    :param max_p: max prime number
    """

    ps = primes.primelist.PrimesList(max_p)
    

if __name__ == '__main__':
    import numpy as np
    import timeit

    num = 2007835831#*3*5*7
    # num = 100
    limit = 10**6
    setup = """import primes.primelist
from p047 import prime_factors, prime_factors2, prime_factors3

plist = primes.primelist.PrimesList({})
num={}
    """.format(limit, num)
    # ct = 10**4
    # t2 = timeit.timeit('prime_factors2(num)',setup=setup, number=ct)
    # print(f't2: {1000*t2:>5.2f}ms')
    # t1 = timeit.timeit('prime_factors(num, plist)',setup=setup, number=ct)
    # print(f't1: {1000*t1:>5.2f}')
    # t3 = timeit.timeit('prime_factors3(num, plist)',setup=setup, number=ct)
    # print(f't3: {1000*t3:>5.2f}ms')

    # plist = primes.primelist.PrimesList(limit)
    # print(prime_factors3(num, plist))


    n = 4
    compare = [n] * n

    trial_nums = list(range(n))
    trial_factors = [prime_factors2(k) for k in trial_nums]

    j = 2 * 3 *  5 # smallest number with 4 distince prime factors
    # might be easier to iterate over prime factorizations
    #   2 * 3 * 5 * 7
    #   2^2 * 3 * 5 * 7
    #   2 * 3^2 * 5 * 7
    while True:
        trial_nums.pop(0)
        trial_factors.pop(0)

        m = trial_nums[-1] + 1
        trial_nums.append(m)
        trial_factors.append(prime_factors2(m))

        if [len(k) for k in trial_factors] == compare:
            print(trial_nums, trial_factors)
            break
        j += 1




        






