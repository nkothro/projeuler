from sympy.ntheory import totient

def is_permutation(a, b):
    astr = list(str(a))
    bstr = list(str(b))
    astr.sort()
    bstr.sort()

    return (a==b)

if __name__ == '__main__':
    min_ratio =100 
    for n in range(1, 10**7 +1):
        phi = totient(n)
        r = n/phi
        if is_permutation(n, phi):
            print('Found one: {}'.format(n))
            best_n = n
            min_ratio = r



