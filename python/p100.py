"""
The computation comes down to  (n/k) * (n-1)/(k-1) = 1/2.  For very large k,
each (n/k) and (n-1)/(k-1) are approximately 1/sqrt(2).
"""

import math as m
import fractions as f

import matplotlib.pyplot as plt

half = f.Fraction(1, 2)
inverse_sqrt2 = 2**(-0.5)

def check_nk_pair(n, k):
    return (f.Fraction(n**2 - n, k**2 - k) == half)

if __name__ == '__main__':
    # width = 4
    # k = 10**12 + 1
    # found = False
    # print(inverse_sqrt2)
    sqrt2 = m.sqrt(2)

    # while not found:
    #     if k > 10**12 + 4*10**11:
    #         found = True
    #     n = m.floor(k / sqrt2)
    #     while f.Fraction((n**2 - n),(k**2 -k)) <= half:
    #         if check_nk_pair(n, k):
    #             found = True
    #             print('Found one at n={}, k={}'.format(n, k))
    #             break;
    #         n+=1
    #         # print((n**2 - n)/(k**2-k))

    #     k+=1
    #     if k % 10**5 == 0:
    #         print(k)

    x = []
    xlo = []
    xprod = []
    xhi = []
    for k in range(10**12,10**12+1000,1):

        n = m.floor(sqrt2 * (k+10)) + 10
        k2 = (k**2 -k)
        while((n**2 - n)/ k2 < 1/2):
            print(k,n)
            n += 1

        x.append(k)
        xlo.append((n-1)/(k-1))
        xhi.append(n/k)
        xprod.append((n**2 - n)/ k2)
        
    plt.plot(x,xlo,label='lo')
    plt.plot(x,xhi,label='hi')
    plt.plot(x,xprod, label='prod')
    plt.legend()
    plt.show()


