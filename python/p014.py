# this version takes 30.310482263565063 seconds to run
import numpy as np

def collatz(n):
    '''

    :param n: first number of Collatz
    :return: the next integer in a Collatz sequence that starts with `n`
    '''
    if n % 2 == 0:
        return int(n/2)
    elif n == 1:
        return None
    else:
        return int(3*n +1)


def collatz_sequence(n):
    seq = [n]
    while n != 1:
        n = collatz(n)
        seq.append(n)

    return seq


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import time

    t0 = time.time()

    lengths = []
    for j in range(2, 10**6):
        lengths.append(len(collatz_sequence(j)))


    print(lengths.index(max(lengths)), max(lengths))

    plt.plot(lengths)

    t1 = time.time()
    print(t1 - t0)

    plt.show()

