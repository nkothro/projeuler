import fractions as f
from itertools import product

digits = [str(j) for j in range(1,10)]
results = []
nums = []
denoms = []

for a, b, c, d in product(digits, repeat=4):
    main_frac = f.Fraction(int(a+b), int(c+d))

    bool_ac = (a == c and main_frac == f.Fraction(int(b), int(d)))
    bool_ad = (a == d and main_frac == f.Fraction(int(b), int(c)))
    bool_bc = (b == c and main_frac == f.Fraction(int(a), int(d)))
    bool_bd = (b == d and main_frac == f.Fraction(int(a), int(c)))

    if (bool_ac or bool_ad or bool_bc or bool_bd) and (a+b) < (c+d):
        results.append('{}{}/{}{}'.format(a, b, c, d))
        nums.append(int(a+b))
        denoms.append(int(c+d))
        print(results[-1])

ans = 1
for j in denoms:
    ans *= j

print(ans)
