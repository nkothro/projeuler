import numpy as np

def gen_prim_pyth_trips(limit=None):
    u = np.mat(' 1  2  2; -2 -1 -2; 2 2 3')
    a = np.mat(' 1  2  2;  2  1  2; 2 2 3')
    d = np.mat('-1 -2 -2;  2  1  2; 2 2 3')
    uad = np.array([u, a, d])
    m = np.array([3, 4, 5])
    while m.size:
        m = m.reshape(-1, 3)
        if limit:
            m = m[m[:, 2] <= limit]
        yield from m
        m = np.dot(m, uad)

# def pythagorean_triples_generator():
#     # https://en.wikipedia.org/wiki/Tree_of_primitive_Pythagorean_triples
#     A = np.mat(' 1 -2  2;  2 -1  2;  2 -2  3')
#     B = np.mat(' 1  2  2;  2  1  2;  2  2  3')
#     C = np.mat('-1  2  2; -2  1  2; -2  2  3')


def pythagorean_trips_from_perimeter(perimeter: int):
    pyth_trips = []
    for c in range(1, perimeter):
        Q = perimeter - c
        for b in range(1, Q):
            a = Q - b
            triplet = tuple(sorted([a, b, c]))
            if triplet[0] ** 2 + triplet[1] ** 2 == triplet[2] ** 2:
                if triplet not in pyth_trips:
                    pyth_trips.append(triplet)
    return pyth_trips


if __name__ == "__main__":

<<<<<<< HEAD
    for k in gen_prim_pyth_trips(1000):
        print(k)

    # maxlen = 0
    # maxlen_index = 0
    # for j in range(3, 1000):
    #     z = pythagorean_trips_from_perimeter(j)
    #     if len(z) > maxlen:
    #         maxlen_index = j
    #         maxlen = len(z)

    # print(maxlen_index, maxlen)
=======
    pmax = 1001
    lookup = {p:[] for p in range(pmax)}

    for k in gen_prim_pyth_trips(pmax):
        p = sum(k)
        # print(k, p)
        mult = 1
        while p*mult <= pmax:
            lookup[p*mult].append(sorted(tuple(a*mult for a in k)))
            mult += 1 
    
    r = [(len(v), k) for k, v in lookup.items()]
    print(sorted(r))

>>>>>>> bf57cf5a67e1d92b8944158865c08e92106b3441
