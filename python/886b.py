def gen_factorial_list(n):
    # up to n!
    f = [1]
    j = 1
    while j < n:
        f.append(f[-1] * j)
        j += 1
    return f

class FactoradixCounter:
    def __init__(self, initial_value, init_max_factorial=None):
        self._factorial_repr = self.from_int(initial_value)
        self._fact_ref = gen_factorial_list(init_max_factorial)

    @staticmethod
    def from_int(n: int, factorial_list=None):
        if factorial_list is None:
            factorial_list = [1]
            j = 1
            while factorial_list[-1] < n:
                factorial_list.append(j * factorial_list[-1])
                j += 1

        r = n
        digits = len(factorial_list)
        factorial_representation = [0] * digits

        for j in range(digits):
            q, r = divmod(r, factorial_list[~j])
            factorial_representation[~j] = q

        while factorial_representation[-1] == 0:
            del factorial_representation[-1]

        return factorial_representation

    def to_int(self):
        return sum(k*self._fact_ref[j] for j, k in enumerate(self._factorial_repr))

    def __repr__(self):
        return ':'.join(str(s) for s in reversed(self._factorial_repr))

    def __str__(self):
        return ':'.join(str(s) for s in reversed(self._factorial_repr))

    def increment(self, amount=1):

        as_int = self.to_int()
        next_fact = self._fact_ref[-1] * len(self._fact_ref)
        if as_int + amount > next_fact:
            self._fact_ref.append(next_fact)
        
        self._factorial_repr = self.from_int(as_int + amount)

        # self._factorial_repr[1] += amount
        # for j in range(1, len(self._factorial_repr)):
        #     if self._factorial_repr[j] > j:
        #         q, self._factorial_repr[j] = divmod(self._factorial_repr[j], j)
        #         if j+1 == len(self._factorial_repr):
        #             self._factorial_repr.append(0)
        #             self._fact_ref.append(self._fact_ref[-1] * len(self._fact_ref))
        #         self._factorial_repr[j+1] += q



if __name__ == "__main__":
    f = FactoradixCounter(463, 10)
    print(f, f.to_int())
    f.increment(100)
    print(f, f.to_int())
