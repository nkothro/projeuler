from math import comb, log10

from eulerlib import PrimesList, PascalsTriangle, MemoryOptimizedPascalsTriangle

def z(
        p_n, 
        k,
        n,
        pt:PascalsTriangle or MemoryOptimizedPascalsTriangle,
        p_n_squared=None
        ):
    """
    An individual term in the sum is: 
        (n choose k) * p_n^(k) * [(-1)**(n-k) + (+1)**(n-k)]
    This goes to zero when (n-k) is odd (the negative and positive one cancel)

    Overall, the entire sum is modulo'd p_n**2.
    If a term contains a multiple of p_n**2, then it will go to zero in the
    modulo'd sum.

    In my notes/work on paper, I called the summands z_{n,k}, which is why this
    function is named z
    """
    if (n-k) % 2 == 1:
        return 0

    prime_exponent = k % 2
    if k >= 2 and (prime_exponent == 0):
        return 0

    c = 2 * pt.get_val(n,k)
    p = p_n ** prime_exponent
    
    if not p_n_squared:
        p_n_squared = p_n**2
    
    return (c * p) % p_n_squared



def mod_sum(p, n, pt: MemoryOptimizedPascalsTriangle):
    # calculate ((p + 1)**n + (p-1)**n) % p**2

    p_square = p**2
    s = 0
    for k in range(n+1):
        # if not is_zero_term(n, k):
        #     # z = 2 * comb(n, k) 
        #     z = 2 * pt.get_value(n, k)
        #     # z %= p_square
        #     # z *= p
        #     # z %= p_square
        #     print(n,k,z)
        #     s += z
        #     # s %= p_square

        # # z = 2 * comb(n, k) 
        z = 2 * pt.get_value(n, k)
        z %= p_square
        z *= p
        z %= p_square
        s += z
        s %= p_square

    s %= p_square
    return s

if __name__ == '__main__':
    plist = PrimesList(10**9).list
    pt = MemoryOptimizedPascalsTriangle()

    # while pt.current_row_idx < 3:
    #     pt.next_row()

    # print(pt.current_row)
    # s = mod_sum(5, 3, pt)
    # print(s)

    for j in range(len(plist)):
        prime = plist[j]
        if prime > 10**5:
            # print(f'current binomial row is {pt.current_row_idx} | iterating now')
            while pt.current_row_idx < j+1:
                pt.next_row()

            # print(f'current binomial row is {pt.current_row_idx}')
            s = mod_sum(plist[j], j+1, pt)
            # print(j+1, s, )
            print(j+1, plist[j+1], s, log10(s))

            if s > 10**10:
                print(j+1, plist[j+1], s, log10(s))
                break
