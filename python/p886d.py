from collections import defaultdict

import numpy as np
from numpy import linalg

from multiprocessing import Pool

from p886b import odd_numbers_gen, create_gcd_mat, mask_indices_from_num, mat_to_repr_int

def numeric_symmetric_mat_power(M, n):
    w, q = linalg.eig(M) 

    try:
        D = np.diag(w**n)
        k = np.matmul(q, D)
        res = np.matmul(k, linalg.inv(q))
        return res
    except Exception as e:
        # print(e.args)
        # print(q)
        return linalg.matrix_power(M,n)


def numeric_coprime_perms_count(n):
    A = create_gcd_mat(n, dtype=np.double)
    s = 1
    r = 0

    _lookup = defaultdict(lambda: None)

    while s <= (2**n)-1:

        intrepr = mat_to_repr_int((A,s))
        if _lookup[intrepr] is None:
            b = mask_indices_from_num(s)
            M = A[b][:,b]
            M = numeric_symmetric_mat_power(M,n)
            # float_v = M[0,0]
            v = int(round(np.real(M[0,0]), 1))
            d = M.shape[0]
            _lookup[intrepr] = (v, d)

        else:
            v, d = _lookup[intrepr]

        r += (-1)**(n-d)*v
        s += 2

    return r

if __name__ == '__main__':
    for j in range(20):
        print(numeric_coprime_perms_count(j))