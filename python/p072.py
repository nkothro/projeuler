from math import gcd, sqrt

def divisors_list(num):
    # returns list of all divisors of num (including num and 1
    div_list = [1]
    upper = int(sqrt(num) + 1)
    for i in range(2, upper):
        if num % i == 0:
            div_list.append(int(i))
            if int(num / i) != int(i) and num != int(num/i):
                div_list.append(int(num/i))
    return div_list

def count_reduced_proper_fractions(d):
    count = 0
    for j in range(1, d):
        if gcd(j, d) == 1:
            count += 1

    return count

def farey_sequence(n: int, descending: bool = False) -> None:
    """Print the n'th Farey sequence. Allow for either ascending or descending."""
    lst = []
    (a, b, c, d) = (0, 1, 1, n)
    if descending:
        (a, c) = (1, n - 1)
    # print("{0}/{1}".format(a, b))
    while (c <= n and not descending) or (a > 0 and descending):
        k = (n + b) // d
        (a, b, c, d) = (c, d, k * c - a, k * d - b)
        lst.append(f'{a}/{b}')
        # print("{0}/{1}".format(a, b))
    
    return lst

if __name__ == '__main__':
    f = farey_sequence(10**5)[:-1]
    print(len(f), f)



