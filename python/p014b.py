# this version takes 2.03 seconds to run

import time

def collatz(n):
    if n == 1:
        return None
    elif not n & 1:
        return int(n/2)
    else:
        return int(3*n + 1)

def collatz_len(n:int, d:dict):
    if n == 1:
        return 1, d

    if n not in d.keys():
        n2 = collatz(n)
        s, d = collatz_len(n2, d)
        d[n] = s+1
    
    return d[n], d

if __name__ == '__main__':
    import matplotlib.pyplot as plt

    t0 = time.time()

    d = {1:0, }
    n_longest = 0
    maxlen = 0

    x = []
    y = []

    for n in range(2, 1000000):
        s, d = collatz_len(n, d)
        if s > maxlen:
            n_longest = n
            maxlen = s
            # print(n, s)

        x.append(n)
        y.append(s)

    t1 = time.time()
    print(t1 - t0)
    print(n_longest, maxlen)

    plt.plot(x, y, '.')
    plt.show()