import numpy as np

def angle_between_circle_centers(ri, rj, rk):
    # find angle (in radians) at circle with radius ri
    num = 2*(rk*ri + ri*rj - rk*rj)
    denom = (ri+rj)*(ri+rk)
    return np.acos(num/denom)

def area