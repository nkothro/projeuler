def concatenate(
        numbers
)->int:
    return int("".join([str(n) for n in numbers]))

def tuple_concatenate_multiplication(num, tup):
    return concatenate([(num*j) for j in tup])

def has_unique_digits(num):
    s = list(str(num))
    # per the problem statement, 0 is not an allowed digit
    if '0' in s:
        return False
    return len(list(set(s))) == len(s)

def test_9digit_num(int):
    as_str = str(int)
    if len(as_str) != 9 or '0' in as_str:
        return False
    


if __name__ == '__main__':
    # Define the range to check for each n
    range_2_digits = range(5123 , 10000)
    range_3_digits = range(123  , 330)
    range_4_digits = range(12   , 100)
    range_5_digits = range(5    , 10)
    range_6_digits = range(3    , 5)
    range_9_digits = [1]

    ranges = [
        range_2_digits,
        range_3_digits,
        range_4_digits,
        range_5_digits,
        range_6_digits,
        range_9_digits
    ]

    tupls = [
        (1, 2),
        (1, 2, 3),
        (1, 2, 3, 4),
        (1, 2, 3, 4, 5),
        (1, 2, 3, 4, 5, 6),
        (1, 2, 3, 4, 5, 6, 7, 8, 9),
    ]

    pandigits = []
    for r, t in zip(ranges, tupls):
        for n in r:
            if has_unique_digits(n):
                z = tuple_concatenate_multiplication(n, t)
                if has_unique_digits(z):
                    pandigits.append(z)
                    print(z, n, t)

    print(tuple_concatenate_multiplication(192, (1,2,3)))
    print("The max is {}".format(max(pandigits)))



