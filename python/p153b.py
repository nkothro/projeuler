import itertools
import matplotlib.pyplot as plt

def gaussian_divisors(n ):
    lst = []
    for a, b in itertools.product(range(1, n), range (n)):
        z = n / complex(a, b)
        zreal_int = int(z.real)
        zimag_int = int(z.imag)
        if zreal_int == z.real and zimag_int == z.imag:
            quad = [z, z.conjugate(), n/z, n/z.conjugate()]
            # print('({:>3}+{:>3}j)({:>3}+{:>3}j) : {}'.format( int(z.real), int(z.imag), int((n/z).real), int((n/z).imag), n**2 % (zreal_int**2 + zimag_int**2) == 0))
            for zi in quad:
                lst.append(zi)
                # print('{:>3} + {:>3}j divides {:>3}'.format(zreal_int, zimag_int, n))
                # print(n % (zimag_int**2 + zreal_int**2), (zimag_int**2 + zreal_int**2) % n)
    return list(set(lst))

n = 12
def print_flip(n):
    gd = gaussian_divisors(n)
    for z in gd:
        a1 = z.real
        b1 = z.imag
        z2 = complex(b1, a1)
        if b1 > 0 and a1 > 0:
            contained = z2 in gd
            print(z, z2, contained)
            if not contained:
                return False
    
    return True

def is_gaussian_integer(z):
    return z.real == int(z.real) and z.imag == int(z.imag)

if __name__ == '__main__':
    for j in range(45):
        l = gaussian_divisors(j)
        for z in l:
            if  is_gaussian_integer(j/z):
                print('{} / {}'.format(j,z))
