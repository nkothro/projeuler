import math
import numpy as np

from eulerlib import is_perfect_square

ref_squares = np.arange(10**8)**2
def minimal_x_solution(D):
    """
    See Project Euler problem 66.
    """
    y = 1
    while True:
        try:
            x2test = int(1 + D*ref_squares[y])
        except IndexError:
            x2test = int(1 + D*y**2)

        if is_perfect_square(x2test):
            return int(np.sqrt(x2test)), y
        y += 1
        # print(D, y)

    return None

if __name__ == '__main__':
    solns = []
    D_values = [D for D in range(1001) if not is_perfect_square(D)]
    Darr = np.asarray(D_values)

    for D in Darr:
        x, y = minimal_x_solution(D)
        print(D, x, y,  x**2 - D*y**2 == 1)