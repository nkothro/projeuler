import numpy as np
from numpy.linalg import matrix_power
from numpy import linalg

import sympy

from collections import defaultdict

from multiprocessing import Pool

MODULO = 83_456_729

"""
GETTING CLOSE!
TODO:
 - memoize M ** n computation, lots of masks will produce same M
 - memoize 'inner_loop' function. Lots of values of A, n, s will produce same number
 - memoize mask_indices from num
"""


def odd_numbers_gen(upper):
    # s = 1
    # while s <= upper:
    #     yield s
    #     s += 2
    # for s in range(upper+1):
    #     if s & 1:
    #         yield s

    yield from (s for s in range(upper+1) if s & 1)


def create_gcd_mat(n):
    x, y = np.meshgrid(np.arange(1, n + 1), np.arange(1, n + 1))
    g = np.gcd(x, y) == 1
    A = np.mat(g, dtype=np.dtype("uint32"))
    return A

def mat_to_repr_int(tup):
    A, s = tup
    b = mask_indices_from_num(s)
    M = A[b][:,b]
    st = ''.join(str(int(i)) for i in np.asarray(M).flatten())
    return st
    # return int(st, 2)


_A_ = create_gcd_mat(34)
# def mat_to_repr_str(tup, base=2):
def mat_to_repr_str(s):
    b = mask_indices_from_num(s)
    M = _A_[b][:,b]
    st = ''.join(str(i) for i in np.asarray(M).flatten())
    return hex(int(st,base=2))

def mask_indices_from_num(s):
    b = list(j for j, i in enumerate(np.binary_repr(s)) if i == "1")
    return np.asarray(b)


def coprimepermscount(n, modulo=None):
    A = create_gcd_mat(n)
    s = 1
    r = 0
    while s <= (2**n) - 1:
        # b = list(int(i) for i in np.binary_repr(s))
        # while len(b) < n:
        #     b.append(0)
        # b = np.arange(n)[np.asarray(b, dtype=bool)]
        b = mask_indices_from_num(s)
        M = sympy.Matrix(A[b][:, b])
        z = extract_powered_matrix(M,n)
        print(z)
        d = M.shape[0]
        r += (-1) ** (n - d) * z
        if modulo:
            r %= modulo
        s += 2

    return r


_lookup = defaultdict(lambda: None)
def extract_powered_matrix(M:sympy.Matrix,n):
    # create an integer representation of the matrix
    # since matrix is all ones and zeros, concatenate into single line and interpret as integer
    int_repr = int(''.join(str(i) for i in M),2)
    # use this as key to memoize
    if _lookup[(int_repr, n)]:
        print('repeat')
        return _lookup[(int_repr, n)]
    
    P,D = M.diagonalize()
    # print('diagonlize done ', M)
    for i in range(D.shape[0]):
        D[i,i] = D[i,i]**n

    P.simplify()
    print(P**-1)
    raised_mat = P * D * P**-1
    raised_mat.simplify()
    v = raised_mat[0,0]
    _lookup[(int_repr, n)] = v
    return v

def inner_loop(tup):
    n, A, s = tup
    # b = list(int(i) for i in np.binary_repr(s))
    # while len(b) < n:
    #     b.append(0)
    # b = np.arange(n)[np.asarray(b, dtype=bool)]

    b = mask_indices_from_num(s)
    M = A[b][:, b]
    # M = matrix_power(M,n)
    M = symmetric_mat_power(M, n)
    d = M.shape[0]

    r = (-1) ** (n - d) * M[0, 0]
    return r


def coprimecounter_pooled(n, nproc=8):
    x, y = np.meshgrid(np.arange(1, n + 1), np.arange(1, n + 1))
    g = np.gcd(x, y) == 1
    A = np.mat(g, dtype=np.int64)  # force native python 'int' objects

    gen = ((n, A, s) for s in odd_numbers_gen(2**n - 1))

    tot = 0

    with Pool(nproc) as p:
        for ri in p.imap_unordered(inner_loop, gen, 10**6):
            tot += ri
            # tot %= MODULO

    return tot


def symmetric_mat_power(A, n):
    w, q = linalg.eigh(A)

    D = np.diag(w**n)
    qi = linalg.inv(q)
    k = np.matmul(q, D)
    return np.matmul(k, qi)
    # print(D)
    # print(B, np.matmul(k, qi))

def mat_to_str(A):
    return ''.join(str(i) for i in np.nditer(A))

def matrix_str_repr(tup):
    A, s = tup
    b = mask_indices_from_num(s)
    M = A[b][:,b]
    return mat_to_str(M)


if __name__ == "__main__":

    N = 34
    
    A = create_gcd_mat(34)
    r = 0
    for s in odd_numbers_gen(2**N):
        pass
#     from eulerlib import next_permutation
    # should follow https://oeis.org/A086595/list
    # import time
    # for j in range(1, 35):
    #     t0 = time.perf_counter_ns()
    #     d = coprimecounter_pooled(j)
    #     t1 = time.perf_counter_ns()
    #     st = f'{j:>3} | {(t1-t0)/1_000_000:>10.3f} | {d}'
    #     print(st)

    # import sympy

    # sympy.init_printing(use_unicode=True, pretty_print=True)
    # expr_set = set()
    # mat_set = set()
    # for j in range(33, 35):
        # A = create_gcd_mat(j)
        # w, q = linalg.eigh(A)
        # D = np.diag(w**8)
        # qi = linalg.inv(q)
        # k = np.matmul(q, D)
        # lut = defaultdict(lambda: 0)
        # for m in range(2, 2**j - 1):
            # if m & 1:
                # b = mask_indices_from_num(m)
                # as_arr = A[b][:, b]
                # as_str = ''.join(str(i) for i in np.nditer(as_arr))
                # lut[as_str] += 1
                # M = sympy.Matrix(as_arr)
                # x = sympy.symbols("x")
                # p = M.charpoly(x)
                # det = M.det()
                # s = ''.join(str(i) for i in M)
                # print(s)
                # if M.shape[0] > 1:
                #     try:
                #         print(M)
                #         P, D = M.diagonalize()
                #         for ii in range(D.shape[0]):
                #             D[ii,ii] = D[ii,ii]**j
                #         k = P * D * (P**-1)
                #         k.simplify()
                #         print(j, m, k)
                #     except Exception as e:
                #         print(j, m, 'not diagonalizeable', M)
                #         print(as_arr)
                #         print(e)

    # from multiprocessing import Pool
    
    # N = 20
    # with Pool(6) as p:
    #     A = create_gcd_mat(N)
    #     gen = ((A,s) for s in odd_numbers_gen((2**N) - 1))

    #     lut = defaultdict(lambda: int())
    #     for j, st in enumerate(p.imap_unordered(matrix_str_repr, gen, chunksize=10**6)):
    #         lut[st] += 1

    #         if j % 10**6 == 0:
    #             print(j)
        
    #     print(lut.values())

    N = 34
    A = create_gcd_mat(N)
    print(A)
    # lo = int('10000000000001',2)

    # onesct = 6
    # permer = ['0'] * (N-onesct) + ['1'] * onesct

    # import matplotlib.pyplot as plt

    # ctr = defaultdict(lambda:int())
    # for t in next_permutation(permer):
    #     bn = int(''.join(t),2)
    #     b = mask_indices_from_num(bn)
    #     M = A[b][:,b]

    #     z = []
    #     for k in range(1,15):
    #         if M.size > 1:
    #             z.append(matrix_power(M,k)[0,0])

    #     ctr[tuple(z)] += 1
    #     if ctr[tuple(z)] == 1:
    #         print('----------')
    #         print(z)
    #         print(M)
    #         print(matrix_power(M,2))

    #     diffs = {}
    #     diffs[1] = list(np.diff(z))
        # print(z)
        # print(diffs[1])
        # for i in range(2,13):
        #     diffs[i] = list(np.diff(diffs[i-1]))
        #     print(diffs[i])
    # print(len(ctr))
    # print(ctr)
        
    k = 20
    B = np.zeros((k,k),dtype=int)
    B[0] = 1
    B[:, 0] = 1
    B[k//2,k//2] = 1

    Bn = B
    z = [1,int(Bn[0,0])]
    for j in range(15):
        if j <= 1:
            print(Bn)
        Bn = np.matmul(B, Bn)
        z.append(int(Bn[0,0]))
    print(z)

    zf = [1,1]
    for j in range(15):
        zf.append(zf[-1] + (k-1)*zf[-2])

    print(zf)

