import numpy as np
# a solution here: https://math.stackexchange.com/questions/176363/keep-getting-generating-function-wrong-making-change-for-a-dollar/176397#176397

# coins = [1, 5, 10, 25, 50, 100]
# total_amount = 100
coins = [1, 2, 5, 10, 20, 50, 100, 200]
total_amount = 200

c = np.zeros(total_amount + 1, dtype=int)
c[0] = 1

for k in coins:
    for i in range(0, total_amount - k + 1):
        c[i+k] += c[i] 

print(c[total_amount])