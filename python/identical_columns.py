from collections import defaultdict, Counter
import numpy as np

from p886b import create_gcd_mat, mask_indices_from_num, odd_numbers_gen

def col_to_string(A, j):
    s = ''.join(str(int(i)) for i in np.nditer(A[j]))
    return s

def pick_out_from_indices(string, mask):
    return ''.join(string[j] for j in mask)

def pick_out_from_num(string, num):
    m = mask_indices_from_num(num)
    return pick_out_from_indices(string, m)

bigs = "ABCBDEFBCGHEIJKBLEMGNOPEDQCJRSTBUV"
def wrapper(n):
    return ''.join(bigs[j] for j, i in enumerate(np.binary_repr(n)) if i == "1")
    
    # return pick_out_from_num(s,n)

A = create_gcd_mat(34)


if __name__ == '__main__':
    import multiprocessing
    import datetime
    import sqlite3
    import pickle
    import pathlib as pl


    #db stuff
    con = sqlite3.connect('./test.db')
    cur = con.cursor()

    cur.execute("CREATE TABLE IF NOT EXISTS selections(code TEXT PRIMARY KEY);")

    d = defaultdict(lambda: False)
    j = 0
    bigstr = ''
    for col in range(34):
        s = col_to_string(A, col)
        if not d[s]:
            d[s] = chr(j + ord('A'))
            j+=1
        bigstr += d[s]

    print(bigstr)
    print(Counter(bigstr))
    N = 34
    tot = 2**N
    ctr = Counter()
    # gen = (n for n in odd_numbers_gen(tot))

    t0 = datetime.datetime.now()

    pickles_dir = pl.Path('./pkls')
    if not pickles_dir.exists():
        pickles_dir.mkdir()

    with multiprocessing.Pool(8) as pool:
        insert_list = set()
        pickle_counter = 0
        for j, s in enumerate(pool.imap_unordered(wrapper, range(1,2**N,2), chunksize=2**10)):
            ctr[s] += 1
            # insert_list.add(s)
            if j % 10_000_000 == 0 and j > 0:
                # cur.executemany("INSERT OR IGNORE INTO selections(code) VALUES (?)", [(k,) for k in insert_list])
                # con.commit()
                # insert_list = set()
                
                dt = datetime.datetime.now() - t0
                percent = 2*j/tot 
                eta = t0+(dt/percent)
                s =  f'{j:>10} '
                s += f'{percent:>9.3%} '
                s += f'eta: {eta.isoformat()}'
                print(s)

            if j % 1_000_000==0:
                fn = pickles_dir / f'counter{pickle_counter:0>8}.pickle'
                with open(fn, 'wb') as f:
                    pickle.dump(ctr, f)

                del ctr
                ctr = Counter()
                pickle_counter += 1
    
        # print('finishing up')
        # cur.executemany("INSERT OR IGNORE INTO selections(code) VALUES (?)", [(k,) for k in insert_list])
        # con.commit()

    with open('out.dict', 'w') as f:
        for k, v in ctr.items():
            f.write(f'{k:>34}:{v:>10}\n')