import numpy

def pentagonal(n):
    return int((3*n**2 - n)/2)

if __name__ == '__main__':
    pents = [pentagonal(k) for k in range(4)]
    res = [0, 0, 0]

    found = False
    while not found:
        for k in range(1, len(pents)):
            print(k)
