from itertools import product as iterproduct

from primes.primelist import PrimesList

def binary_mask_counter(n:int):
    gen = iterproduct([False, True], repeat=n)
    all_true = ((True), )*n
    next(gen) # don't want all 0's tuple
    for k in gen:
        if k < all_true:
            yield k

def mask_digits_replace(num, mask):
    str_repr = str(num)

    if len(str_repr) != len(mask):
        raise ValueError('Mask must include bit for each digit')

    idx = [j for j, b in enumerate(mask) if b]
    out = []
    for d in range(0,10):
        cpy = list(str_repr)
        for i in idx:
            cpy[i] = str(d)
        out.append(int(''.join(cpy)))
    
    return out

if __name__ == '__main__':
    import numpy as np
    primeslist = PrimesList(10**8)
    plist = primeslist.list

    z = mask_digits_replace(56003, (False, False, True, True, False))
    z = [j for j in z if primeslist.is_prime(j)]
    print(len(z), z)

    num_digits = 2
    while num_digits < 8:
        digit_masks = [m for m in binary_mask_counter(num_digits)]
        subset = plist[np.logical_and(plist > 10**(num_digits-1), plist < 10**num_digits) ]

        for n in subset:
            # print(n)
            for mask in digit_masks:
                substituted = mask_digits_replace(n, mask)
                if n in substituted:
                    b = [j for j in substituted if primeslist.is_prime(j) and j >= subset[0]]
                    if len(b) == 8:
                        print(n, b)
                        break

        num_digits += 1