def palindromic_dual(num: int):
    digilist = list(str(num))
    digilist.reverse()
    return int("".join(digilist))

def test_palindrome(num: int):
    if num == palindromic_dual(num):
        return True
    else:
        return False


def test_lychrel(num: int):
    count = 0
    while (count <= 50):
        num += palindromic_dual(num)
        count += 1

        if test_palindrome(num):
            return True
    return False



if __name__ == '__main__':
    j = 1
    c = 0
    while j < 10_000:
        # print('Testing: ' + str(j))
        if not test_lychrel(j):
            c += 1
            print(str(j) + ' is lychrel')
        j += 1
    
    print(c)

