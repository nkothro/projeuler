from collections import defaultdict
from math import gcd
import itertools

import numpy as np
import sympy.ntheory

from eulerlib import PrimesList, prime_factors_of

def test_coprime(a: int, b: int):
    return gcd(a, b) == 1


def hidden_in_row(n):
    # this is effectively the cototient function 
    return sum(1 for j in range(n) if not test_coprime(j, n))

def hidden_in_slice(n, printout=False):
    # this is a partial sum of the cototient function
    if n < 2:
        return 0

    s = 0
    for j in range(2, n+1):
        if printout and (j % 5000)==0:
            print(j)
        s += hidden_in_row(j)
    return s

def recur_totient(num:int, plist:PrimesList, totient_lut):
    if plist.is_prime(num):
        totient_lut[num] = 1
        return 1

    if totient_lut[num] != 0:
        print(num, totient_lut[num])
        return totient_lut[num]

    # find first prime divisor of num
    j = 0
    while num % plist.list[j] != 0:
        j += 1

    low_p = plist.list[j]

    testnum = num / low_p
    # print(num, testnum, low_p, low_p*testnum == num)

    testnum = int(testnum)
    if totient_lut[testnum] == 0:
        # print(num)
        totient_lut[testnum] = recur_totient(testnum, plist, totient_lut)

    d = gcd(testnum, low_p) 
    totient_lut[num] = totient_lut[testnum] * totient_lut[low_p] * d / totient_lut[d]
    return totient_lut[num]


def generate_totient_values(max_arg, plist:PrimesList=None):
    if not plist:
        plist = PrimesList(max_arg)

    totient_lut = np.zeros(max_arg + 1, dtype=np.int32)
    totient_lut[plist.list] = 1
    totient_lut[1] = 1 # because this isn't taken care of by primes list index

    while np.sum(totient_lut == 0) > 0:
        for tot in range(1,totient_lut.size):
            if totient_lut[tot] == 0:
                i = 0
                p = plist[i]
                while p*tot < max_arg:
                    totient_lut[p*tot] 
                    p = plist[i]



    # for j in range(1, max_arg+1):
    #     recur_totient(j, plist, totient_lut)
    
    return totient_lut

    # for p in plist:
    #     j = 1
    #     testval = p
    #     while testval < max_arg:
    #         j += 1
    #         testval += p
    #         # now, testval is j*p
    #         if totient_lut[j] == 0:
    #             totient_lut[j] = totient(j, plist=plist)

    #         # use identity that totient[j*p] = totient[j]*totient[p] * gcd(j,p) / totient[gcd(j,p)]

    #         if totient_lut[testval] == 0:
    #             d = gcd(j,p)
    #             t = totient_lut[j] *totient_lut[p] * d / totient_lut[d]
    #             totient_lut[testval] = t

    #         if j % 100 ==0:
    #             print(p,j, totient_lut[testval])




def totient(n, plist:PrimesList):
    # if plist.list[-1] < n:
    #     raise ValueError(f'n={n} is less than plist max of {plist.list[-1]}')

    if plist.is_prime(n):
        return 1
    
    pf = prime_factors_of(n)
    t = n
    for p in pf:
        t //= p
        t *= (p-1) 
    
    return t


if __name__ == '__main__':
    import sympy
    N = 100_000_000
    sieve = sympy.ntheory.Sieve()
    k = sieve.totientrange(1,N+1)

    s = 0
    for j, ki in enumerate(k):
        if j+1 >= 2:
            s += j+1 - ki

        if j+1 % 1000 == 0:
            print(f'{(j+1)/N:>.2f}')
    
    print(6*s)

    # plist = PrimesList(N)
    # tlut = generate_totient_values(N, plist=plist)

    # for j, t in enumerate(tlut):
    #     print(j,t)

    # for n in range(1,N):
    #     t = totient(n, plist=plist)
    #     if n%10_000==0:
    #         print(f'{n/N:>.2%}')
