import sqlite3
import pathlib as pl

from itertools import product, combinations
from collections import Counter

import pandas as pd

from eulerlib import PrimesList

def recur_search(
        numlist,
        contains:dict,
        conn,
        totallist
    ):
        digits_needed = sum(1 for k, v in contains.items() if not v)
        if digits_needed == 0:
            print(numlist)
            totallist.append(numlist)

        min_num = numlist[-1]
        sql = """SELECT * FROM primes """
        sql += f"WHERE digits <= {digits_needed} AND num > {min_num} "

        for k, v in contains.items():
            if v:
                 sql += f"AND {k} = 0 "
        sql += ';'
        for row in conn.cursor().execute(sql):
            print(len(numlist))
            keys = [f'contains{k}' for k in range(1,10)]
            d = {k:row[k] for k in keys}

            totallist += recur_search(
                 numlist + [row['num']],
                 d,
                 conn,
                 totallist
                 )
        
        return totallist



if __name__ == '__main__':
    dbname = pl.Path('./unique_digit_primes.db')
    upper = 10**9

    # if not dbname.exists():
    #     generate_primes_unique_digits_db(dbname, upper)
    # db = sqlite3.connect(dbname)

    source = sqlite3.connect(dbname)
    db = sqlite3.connect(':memory:')
    source.backup(db)
    db.row_factory = sqlite3.Row

    sql = """SELECT * FROM primes"""
    complete_reference = db.cursor().execute(sql).fetchall()

    totals = []
    keys = [f'contains{j}' for j in range(1,10)]
    for row in complete_reference:
        numlist = [row['num']]
        contains = {k:row[k] for k in keys}

        totals += recur_search(numlist, contains, db, totals)
