from math import acos, pi

class Vector2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def length(self):
        return (self.x**2 + self.y**2)**0.5

    def __add__(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
            return Vector2D(self.x - other.x, self.y - other.y)

    def __mul__(self, other:float or int):
        return Vector2D(other*self.x, other*self.y)

    def __neg__(self):
        return Vector2D(-1* self.x, -1*self.y)

    def __truediv__(self, other):
        return self * (1/other)

    def dot(self, other):
        return (self.x * other.x) + (self.y * other.y)

    def angle_between(self, other):
        return acos(self.dot(other)/ (self.length() * other.length()))

class Triangle:
    def __init__(self, A:Vector2D, B:Vector2D, C:Vector2D):
        self.A = A
        self.B = B
        self.C = C

    def sides(self):
        # A -> B
        ab = self.B - self.A
        # B -> C
        bc = self.C - self.B
        # C -> A
        ca = self.A - self.C

        return ab, bc, ca

    def perimeter(self):
        return sum([x.length() for x in self.sides()])

    def area(self):
        side_lengths = [x.length() for x in self.sides()]
        semiperimeter = 0.5 * sum(side_lengths)
        area_sqd = semiperimeter
        for side in side_lengths:
            area_sqd *= semiperimeter - side
        return area_sqd**0.5

    def angles(self):
        ab, bc, ca = self.sides()
        # angle at A
        alpha = ab.angle_between(-ca)
        # angle at B
        beta = bc.angle_between(-ab)
        # angle at C
        gamma = ca.angle_between(-bc)

        return alpha, beta, gamma

    def is_inside(self, p: Vector2D):
        # vectors for the origin to each vertex
        p_to_vertices = [x - p for x in (self.A, self.B, self.C)]
        n = len(p_to_vertices)

        angle_sum = 0

        for j in range(n):
            u = p_to_vertices[j]
            v = p_to_vertices[(j+1) % n]
            angle_sum += u.angle_between(v)

        if angle_sum <= pi:
            return False
        else:
            return True

if __name__ == '__main__':
    from p102_triangles import triangles_coordinates_list

    triangles_lst = []
    for tup in triangles_coordinates_list:
        A = Vector2D(tup[0], tup[1])
        B = Vector2D(tup[2], tup[3])
        C = Vector2D(tup[4], tup[5])
        triangles_lst.append(Triangle(A, B, C))

    # main loop
    origin = Vector2D(0, 0)
    count = 0
    for triangle in triangles_lst:
        if triangle.is_inside(origin):
            count += 1

    print(count)


