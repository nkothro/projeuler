from fractions import Fraction as F

# want to represent sqrt(2) as continued (i.e. iterated) fraction.

def sqrt_two_fraction(iteration_count: int):
    if iteration_count == 1:
        f = F(numerator=1, denominator=2)
    else:
        f = F(1, F(2, F(1/2)))
        for j in range(iteration_count):
            f = F(1, 2+f)
    return 1 + f


def generate_numerators(stop_idx:int=100):
    """
    per OEIS A00133
    a(n) = 2a(n-1) + a(n-2)
        with a[0] = 1 and a[1] = 1
    OR
    a(n) = ((1-sqrt(2))^n + (1+sqrt(2))^n)/2

    """
    a = []
    a.append(1) # a[0] = 1
    a.append(1) # a[1] = 1

    j = 2
    while j < stop_idx:
        aj = 2*a[-1] + a[-2]
        a.append(aj)
        j+=1

        yield aj

def generate_pell_numbers(stop_idx:int=100):
    """
    Similar recursion relationship to numerators
    actually is the pell numbers
    """

    a = []
    a.append(0) # a[0] = 0
    a.append(1) # a[1] = 1

    j = 2
    while j < stop_idx:
        aj = 2*a[-1] + a[-2]
        a.append(aj)
        j+=1

        yield aj


ct = 0
# 1003 instead of 1000 because of how problem statement counts vs how these generators are indexed:
#  a[2] is 'first' in problem statement
for j, (n, d) in enumerate(zip(generate_numerators(1003), generate_pell_numbers(1003))):
    if len(str(n)) > len(str(d)):
        ct += 1

    print(j)
print(ct)