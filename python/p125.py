import numpy as np

def gen_palindromic_numbers(num_digits):
    half = num_digits // 2
    oddlen = num_digits & 1

    for j in range(10**(half-1),10**half):
        s = str(j)
        if oddlen:
            for i in range(10):
                yield int(s + str(i) + s[::-1])
        else:
            yield int(s + s[::-1])

def consecutive_sum_square(n, m):
    # assumes n < m
    # find sum n^2 + (n+1)^2 + ... + m^2
    return (m * (1 + m)*(1 + 2*m) + n*(-1 + (3 - 2*n)*n))//6

if __name__ == '__main__':
    upper = 10**8
    n = 0
    test_set = set()
    for window in range(2,680):
        j = 0
        s = 0
        while s < upper:
            test_set.add(s) 
            s = consecutive_sum_square(j, j+window)
            j+=1

    print(test_set)

