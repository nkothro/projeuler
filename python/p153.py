import itertools
from numbers import Complex
from cmath import phase

import matplotlib.pyplot as plt

def is_square(x):
    if(x < 0 or (x&2) or (x & 7 == 5) or (x & 11 == 8)):
        return False
    if (x==0):
        return True

    y = x
    y = (y & 4294967295) + (y >> 32);
    y = (y & 65535) + (y >> 16)
    y = (y & 255) + ((y >> 8) & 255) + (y >> 16)

    bad255 = [
        0,0,1,1,0,1,1,1,1,0,1,1,1,1,1,0,0,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,
        1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,1,1,
        0,1,0,1,1,0,0,1,1,1,1,1,0,1,1,1,1,0,1,1,0,0,1,1,1,1,1,1,1,1,0,1,
        1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,0,0,1,1,1,1,1,1,
        1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,
        1,1,1,1,1,1,0,1,1,0,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,
        1,1,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,
        1,0,1,1,1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,
        0,1,1,0,1,1,1,1,0,1,1,1,1,1,0,0,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,1,
        1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,0,1,1,1,0,
        1,0,1,1,0,0,1,1,1,1,1,0,1,1,1,1,0,1,1,0,0,1,1,1,1,1,1,1,1,0,1,1,
        1,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,0,0,1,1,1,1,1,1,1,
        1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,
        1,1,1,1,1,0,1,1,0,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,
        1,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,
        0,1,1,1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,0]
    
    if bad255[y]:
        return False

    if((x & 4294967295) == 0):
        x >>= 32
    if((x & 65535) == 0):
        x >>= 16
    if((x & 255) == 0):
        x >>= 8
    if((x & 15) == 0):
        x >>= 4
    if((x & 3) == 0):
        x >>= 2
    
    if((x & 7) != 1):
        return False

    return True
    # t = 4;
    # r = 1;
    # t <<= 1; r += ((x - r * r) & t) >> 1;
    # t <<= 1; r += ((x - r * r) & t) >> 1;
    # t <<= 1; r += ((x - r * r) & t) >> 1;

# for k in range(40):
#     print(k, is_square(1500**2))

n = 5
lst = []
for a, b in itertools.product(range(1, n), range (n)):
    z = n / complex(a, b)
    zreal_int = int(z.real)
    zimag_int = int(z.imag)
    if zreal_int == z.real and zimag_int == z.imag:
        quad = [z, z.conjugate(), n/z, n/z.conjugate()]
        # print('({:>3}+{:>3}j)({:>3}+{:>3}j) : {}'.format( int(z.real), int(z.imag), int((n/z).real), int((n/z).imag), n**2 % (zreal_int**2 + zimag_int**2) == 0))
        for zi in quad:
            lst.append(zi)
            # print('{:>3} + {:>3}j divides {:>3}'.format(zreal_int, zimag_int, n))
            # print(n % (zimag_int**2 + zreal_int**2), (zimag_int**2 + zreal_int**2) % n)
        plt.plot([zi.real for zi in quad], [zi.imag for zi in quad], '-o')
lst = list(set(lst))
rootn = n**0.5
outer = len([k for k in lst if abs(k) > rootn])
inner = len([k for k in lst if abs(k) < rootn])
eq = len([k for k in lst if abs(k) == rootn])
# print(outer, inner, eq)
for zi in lst:
    a1 = zi.real
    b1 = zi.imag
    print(n/ complex(b1, a1))

plt.show()

lst = list(set(lst))

i = []
r = []
for z in lst:
    k = z * z.conjugate()
    kn1 = k.real / n
    kn2 = n / k.real

    # if z.imag == 0:
    #     print('real', z, k)
    # elif int(kn1) == kn1:
    #     print('kn1', z, kn1)
    # elif int(kn2) == kn2:
    #     print('kn2', z, kn2)
    # else:
    #     print('!ERR', z, k.real)

    if z.imag >= 0:
        r.append(z.real)
        i.append(z.imag)
    
    # print(n**2 / (abs(z))**2)
# plt.plot(r, i, 'o')
# plt.show()