from primes.primelist import PrimesList
from math import gcd
import math

# Using euclid's formula
def euclids_formula(m, n):
    if m == n:
        ValueError('Inputs must not be equal')

    if m < n:
        i = m
        m = n
        n = i

    a = m**2 - n**2
    b = 2*m*n
    c = m**2 + n**2

    return a, b, c



if __name__ == '__main__':
    L = int(1.5*10**6)
    primitive_triples = []

    """
    a + b + c <= L must be true. Using euclid's formula:
    a + b + c = (m^2 - n^2) + 2*m*n + (m^2 + n^2) 
              = 2m*(n+m) <=L
    Use this to determine the bounds on n,m
    """

    upper_m =  math.floor((1/2)*(math.sqrt(2*L + 1) - 1))
    for m in range(2, upper_m+1):
        # Now that m is determined, determine max n to iterate to
        # m < n, and solvinf 2*m(m+n) <=L for a given m
        upper_n = min(m, math.ceil((L - 2*m**2)/m))
        for n in range(1, upper_n+1):
            if (m % 2) != (n % 2) and gcd(m, n) == 1:
                a, b, c = euclids_formula(n, m)
                if a+b+c <= L:
                    primitive_triples.append((a,b,c))

    print(len(primitive_triples))
    # now have all base triples.

    triples = []
    for pt in primitive_triples:
        k = 1
        peri = pt[0] + pt[1] + pt[2]
        while k*peri <= L:
            triples.append((k*pt[0], k*pt[1], k*pt[2]))
            k+=1

    perimeters = [sum(x) for x in triples]
    singles_idx = []

    for j, p in enumerate(perimeters):
        if perimeters.count(p) == 1:
            singles_idx.append(triples[j])
            print('Found one: {}'.format(triples[j]))

    print(singles_idx)
    print(len(singles_idx))





