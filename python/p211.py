import bisect
from math import gcd
from collections import defaultdict, Counter
from itertools import product as iterproduct
import decimal

import numpy as np
import matplotlib.pyplot as plt

from eulerlib import PrimesList, prime_factorization


def is_square(squares_list: np.ndarray, x):
    i = np.searchsorted(squares_list, x, side="left")
    return squares_list[i] == x


def divisors_list(num):
    # returns list of all divisors of num (including num and 1
    div_list = [1]
    upper = int(np.sqrt(num) + 1)
    for i in range(2, upper):
        if num % i == 0:
            div_list.append(int(i))
            if int(num / i) != int(i) and num != int(num / i):
                div_list.append(int(num / i))
    return div_list + [num]


def sigma2(n):
    return sum(k**2 for k in divisors_list(n))

def sigma2_from_prime_factors(d:dict):
    # prod = 1
    num = int(1)
    den = int(1)
    for p, a in d.items():
        p = int(p)
        a = int(a)
        p2 = int(p)**2
        num *= int((p2*p**(2*a)) -1)
        den *= int(p2 - 1)

    if num < 0:
        print(d)
    
    return int(num // den)

def sigma2_from_prime_factors_is_square(d: dict, squares):
    sn = sigma2_from_prime_factors(d)
    # if sn < 0:
    #     print(sn)
    return sn, bisect_contains(squares, sn)
    

def remove_from_sorted(arr, x):
    if len(arr) == 0:
        return

    i = bisect.bisect_left(arr, x)
    if arr[i] == x:
        del arr[i]


def bisect_contains(arr, x):
    i = bisect.bisect_left(arr, x)
    if i < 0 or i >= len(arr) or len(arr) < 0:
        return False
    return arr[i] == x


def pairwise_products_less_than(int_key_list, min, upper):
    int_key_list.sort()
    for j, num1 in enumerate(int_key_list):
        for num2 in int_key_list[0:j+1]:
            jk = num1 * num2

            if jk > upper or bisect_contains(int_key_list, jk):
                break
            elif jk >= min:
                yield num1, num2, jk
        # print(num1,num2)

if __name__ == "__main__":
    import time
    upper = 64_000_000
    # upper = 1_000_000
    # squares = np.arange(upper+1, dtype=np.uint64)**2
    x = sigma2(upper)
    upper_square = int(np.sqrt(x))
    squares = np.arange(upper_square + 1, dtype=np.uint64) ** 2

    # for n in range(1, upper+1):
    #     s = sigma2(n)
    #     if is_square(squares,s):
    #         running_tot = n
    #         print(n, s)

    still_to_do = [k + 2 for k in range(upper - 1)]
    completed = np.zeros(upper+1,dtype=bool)
    completed[0] = 1

    primes_lst = PrimesList(upper)
    factor_lut = defaultdict(lambda: Counter())
    # factor_lut[1] = [1]

    # initial set of primes
    for p in primes_lst:
        factor_lut[p] = Counter({p: 1})
        completed[p] = 1
        # remove_from_sorted(still_to_do, p)
    

    new_this_loop = list(factor_lut.keys())
    print(len(primes_lst))
    t0 = time.perf_counter_ns()
    # while len(still_to_do) > 0:
    s = 0
    while s < upper:
        # for x, factors in list(factor_lut.items()):
        next_loop = []
        for x in new_this_loop:
            for p in primes_lst:
                newnum = x*p
                if newnum > upper:
                    break

                if not completed[newnum]:
                    next_loop.append(newnum)
                    d = factor_lut[x].copy()
                    d[p] += 1
                    factor_lut[newnum] = d
                    completed[newnum] = True

        new_this_loop = next_loop


        s = np.sum(completed)
        print(f'{s/upper:>.4f}')
    t1 = time.perf_counter_ns()
    print(f'{(t1-t0)/10e6:.2f}')

    tot = 0
    for n, pfact in factor_lut.items():
        sig2n, b = sigma2_from_prime_factors_is_square(pfact, squares=squares)
        if b:
            tot += n
            print(n, sig2n, tot)
        # (n, sigma2_from_prime_factors_is_square(pfact, squares=squares))

    # 6127529 (-141960, False)
    # k = sigma2_from_prime_factors_is_square(factor_lut[6127529], squares)
    # print(k)