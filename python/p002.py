# This block is a definition for functionalizing my process
def fiblist(n):
    '''
    Generates list of the first n fibonacci numbners, starting with f0 = 0
    '''
    fib = [0,1]
    for j in range(n-1):
        fib.append(fib[-1] + fib[-2])
    return fib


# the main sequence
if __name__ == "__main__":

    a = 0
    b = 1

    big_sum = 0

    while a < (4 * 10**6):
        c = a + b
        a = b
        b = c

        if c % 2 == 0:
            big_sum += c

    print(big_sum)


