import math
from decimal import *

# expansion of sqrt(x)
# m choose n can be represented using gamma functions
# 1/2 choose n, where n is an integer, is then
# = gamma(1/2)/(n!)(gamma(3/2 - n))

def taylor_sqrt(value, terms:int):
    # gamma(1/2) is a cosntant
    k = math.gamma(1/2)
    s = 0
    for n in range(terms):
        s += k / (math.factorial(n) * math.gamma(3/2 - n)) (value - 1)**n
    return s

def digital_sum(num: Decimal, digit_count):
    digits = list(str(num))
    k = 0
    j = 0
    digit_sum = 0
    while k < digit_count:
        if digits[j] != '.':
            k+=1
            digit_sum += int(digits[j])
        j+=1

    return digit_sum

if __name__ == '__main__':
    getcontext().prec=105

    nums = []
    for j in range(1, 101):
        # dont want perfect squares
        if j not in [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]:
            nums.append(Decimal(j))

    big_sum = 0
    for n in nums:
        big_sum += digital_sum(n ** Decimal(0.5), 100)

    print(big_sum)




