import numpy as np
import matplotlib.pyplot as plt
import matplotlib

c = 1/6 #global variable in this problem: center of circle is always (1/6, 1/6)

class PnetCircle:
    def __init__(self, k):
        self._k = k
    
    @property
    def k(self):
        return self._k

def pentagonal(n):
    return 1.5 * np.square(n) - n/2

def check_on_circle(x, y, k):
    return (x-c)**2 + (y-c)**2 == (k-c)**2 + c**2

def check_on_circle_arr(x:np.ndarray, y:np.ndarray, k:int):
    xx, yy = np.meshgrid(x, y)
    return (xx-c)**2 + (yy-c)**2 == (k-c)**2 + c**2

def y(x, k):
    N = pentagonal(k) - pentagonal(x)
    expr = np.sqrt(24*N + 1)
    y = (1/6) * (expr + 1)
    return np.floor(y)-1, np.ceil(y)+1

def min_x_to_check(k):
    disc = np.sqrt(6*k*(3*k - 1) + 1)
    return (disc+1)/6

def circle_radius(k):
    return np.sqrt((2/3) * pentagonal(k) + c/3)

def circle_points_polar(k, numpoints=1000):
    t = np.linspace(0, np.pi/4, numpoints)
    r = circle_radius(k)    # r = np.sqrt(0.333*pentagonal(k) + (1/18))
    x = c + r*np.cos(t)
    y = c + r*np.sin(t)

    return x, y

def circle_points_cartesian(k, x):
    N = pentagonal(k) - pentagonal(x)
    expr = np.sqrt(24*N + 1)
    y = (1/6) * (expr + 1)



if __name__ == '__main__':
    """
    P(x) + P(y) = P(k) can be re-rwitten into form:

    3\left(x-\frac{1}{6}\right)^{2}+3\left(y-\frac{1}{6}\right)^{2}=k\left(3k-1\right)+\frac{1}{6}

    Which is a circle centered on (1/6, 1/6)
    For a given k, the circle crosses the x-axis at k. Move along the circle (in
    positive theta direction) until hit intesction of circle and the line x=y.
    """

    xx = np.arange(10)
    yy = np.arange(10)

    print(check_on_circle_arr(xx, yy, 8))

    maxlen = 0
    k = 0
    # for k in range(35877, 35878+10):
    while maxlen < 100:
        k += 1
        # min integer x to check is ceil of x coordinate of intesection between circle and line x=y
        x0 = np.ceil(min_x_to_check(k))
        
        # max x to check is one less than k, since (k,0) is on the circle but is not a solution, since 0 < y <= x
        x1 = k - 1

        x = np.arange(x0, k, dtype=int)
        ylo, yhi = y0(x, k)

        pairs = []
        for xi, yloi, yhii in zip(x, ylo, yhi):
            for yi in range(int(yloi), int(yhii+1)):
                # if pentagonal(xi) + pentagonal(yi) == pentagonal(k) and xi != yi:
                if check_on_circle(xi, yi, k):
                    pairs.append((xi, yi))

        if len(pairs) > maxlen:
            maxlen = len(pairs)

            fig, ax = plt.subplots(1,1)
            cx, cy = circle_points(k)
            ax.plot(cx, cy)
            for p in pairs:
                ax.plot(max(p), min(p), 'o')
            
            plt.axis('square')


            print(f'k={k:>5} w/ r={circle_radius(k):>.4f}: {len(pairs):>3}, {pairs}')
            plt.show()



            

