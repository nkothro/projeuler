from collections import defaultdict
import multiprocessing as mp

from p886b import create_gcd_mat, mask_indices_from_num, mat_to_repr_int, odd_numbers_gen, mat_to_repr_str

import sympy
import time
import pathlib as pl
import sqlite3
import numpy as np

N = 34
_A_ = create_gcd_mat(N)
def sympy_inner_loop(s):
    b = mask_indices_from_num(s)
    # M = sympy.ImmutableMatrix(_A_[b][:,b])
    # m34 = M.pow(34, 'cayley')
    M = np.matrix(_A_[b][:,b])
    m34 = np.linalg.matrix_power(M, N)
    return s, int(m34[0,0])

if __name__ == '__main__':
    import math
    import itertools

    ctr = defaultdict(lambda: int())
    d = defaultdict(lambda: int())
    A = _A_
    for i,j in itertools.combinations(range(N), 2):
        d[i] |= (1 << i)
        d[j] |= (1 << j)
          
        if (A[i]==A[j]).all():
            d[j] |= (1 << i)
            d[i] |= (1 << j)

    s = set()
    for k, v in d.items():
        s.add(v)
    
    l = []
    for si in s:
        # print(si, si.bit_count())
        l.append(si.bit_count())

    num = math.factorial(sum(l))
    for di in l:
        num //= math.factorial(di)

    # print(num)
    
    # print(sum(l))

    import datetime
    dbfile = pl.Path(r'F:\data.db')
    con = sqlite3.connect(dbfile)
    cur = con.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS results(string_repr TEXT, modulos_res INTEGER);")

    t0 = time.perf_counter_ns()
    starttime = datetime.datetime.now()
    odds = range(1, 2**N, 2)
    tot = (2**N) - 1
    reprs = []
    with mp.Pool(12) as p:
        # gen = ((A, s) for s in odds)
        # for j, rep in enumerate(p.imap_unordered(mat_to_repr_str, gen, chunksize=2**12)):
        j = 0
        m = 0
        modular = 2**14
        for s, rep in p.imap_unordered(sympy_inner_loop, odds, chunksize=2**12):
            j += 1
            m += 1
            k = (hex(s), rep % 83456729)
            reprs.append(k)
            # if j % (1 << 18) == 1:
            if m == modular:
                cur.executemany("INSERT OR IGNORE INTO results VALUES (?, ?)", reprs)
                con.commit()
                t2 = datetime.datetime.now()
                percent = 2*j/tot
                t1 = time.perf_counter_ns()
                ddt = (t2 - starttime)/percent
                dt = t1 - t0
                dt /= j
                s = f'{j:>16} of {tot//2} ({percent}) ({dt/1000}us per) EST FINISH: {(starttime+ddt).isoformat()}'
                print(s)
                m = 0

    with open('dict.out', 'w') as f:
        for k, v in ctr.items():
            f.write(f'{k}:{v}\n')
    print(ctr)
    print(len(ctr))