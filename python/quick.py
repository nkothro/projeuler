import math

import eulerlib

def f(x):
    return 2*(2*x + 1)**2 -1

for i in range(10000):
    d = f(i)
    if eulerlib.is_perfect_square(d):
        print(i, d, d**0.5)

