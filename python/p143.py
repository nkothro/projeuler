import math

import numpy as np


SQUARES_UPPER = 120_000 + 1
SQUARES_ARR = np.arange(SQUARES_UPPER+1,dtype=np.uint64)**2
def is_square_lut(x, test_array):
    j = np.searchsorted(test_array, x)
    return x == test_array[j], j

# https://oeis.org/A351476

def partition_three(s):
    """
    quot, rem = divmod(s, 3)
    if rem==0:
        min_tripl = [quot, quot, quot]
    elif rem == 1:
        min_tripl = [quot+1, quot, quot]
    else: 
        min_tripl = [quot+1, quot+1, quot]

    tripl = [s-2, 1, 1]

    while tripl >= min_tripl:
        yield tripl[0], tripl[1], tripl[2]

        tripl[2] += 1
    """
    p = 1
    q = 1
    r = s - p - q
    while p <= q <= r:
        yield p, q, r

        r -= 1
        q += 1

        if q > r:
            p += 1
            q = p
            r = s - p - q

def test_torricelli_triangle(p,q,r):
    # lines to Fermat point are always at 120 with each other
    # law of cosines becomes a**2 = r**2 + q**2 - pq
    q2 = q**2
    r2 = r**2

    a2 = r2 + q2 + r*q

    is_square, j = is_square_lut(a2, SQUARES_ARR)
    if not is_square:
        return False, -1, -1, -1
    a = j
    
    
    p2 = p**2

    b2 = p2 + q2 + p*q
    is_square, j = is_square_lut(b2, SQUARES_ARR)
    if not is_square:
        return False, -1, -1, -1
    b = j

    c2 = p2 + r2 + r*p
    is_square, j = is_square_lut(c2, SQUARES_ARR)
    if not is_square:
        return False, -1, -1, -1
    c = j

 
    return True, a, b, c

def test_abc(a,b,c):
    if not math.gcd(a, math.gcd(b,c)) == 1:
        return False


def test_partitions_torricelli(N):
    for p,q,r in partition_three(N):
        yay, a, b, c = test_torricelli_triangle(p,q,r)
        if yay:
            print(f'{N=},{(p,q,r)=},{(a,b,c)=}')
            return True

    return False

if __name__ == '__main__':
    import multiprocessing 

    a, b, c = 
    # upper = SQUARES_UPPER+1
    # gen = range(4,upper)

    # with multiprocessing.Pool(processes=8) as pool:
    #     tot = 0
    #     for j, b in enumerate(pool.imap_unordered(test_partitions_torricelli, gen, chunksize=50)):
    #         if b:
    #             tot += 1
    #             print(f'{j} had one, tot={tot}')
    #         if j % 100 == 0:
    #             print(f'j={j}') 
