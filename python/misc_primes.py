from  multiprocessing import Pool
import time

import numpy as np
import matplotlib.pyplot as plt

from eulerlib import PrimesList, prime_factorization

plist = PrimesList(10**8)

def wrapper(n):
    return prime_factorization(n, plist)


if __name__ == '__main__':

    vals = []
    for ct in np.floor(np.logspace(4,4.5,num=15),):

        t0=time.perf_counter_ns()
        for j in range(3,int(ct)):
            factorization = wrapper(j)
        t1=time.perf_counter_ns()
        dt0 = t1-t0

        print(f'STARTING MULTIPROCESS FOR {ct=}')
        t2 = time.perf_counter_ns()
        with Pool(6) as pool:
            for d in pool.imap_unordered(wrapper, range(3,int(ct)),chunksize=1000):
                pass
        t3 = time.perf_counter_ns()
        dt1 = t3-t2

        vals.append((ct, dt0, dt1))

    counts = [ct for ct, dt0, dt1 in vals]
    ratios = [dt1/dt0 for ct, dt0, dt1 in vals]

    plt.plot(counts, ratios)
    plt.yscale('log')
    plt.show()


    p = 0

    # t0 = time.process_time_ns()
    # for j in range(10**6):
    #     d = prime_factorization(j)

    # t1 = time.process_time_ns()
    # print(t1-t0)

    
    with Pool(processes=24) as pool, open('test.txt', 'w') as f:
        for n, l in pool.imap_unordered(wrapper, range(3,10**8), chunksize=100):
            s = f'{n:>9}: '
            s += ' . '.join(f'{p}**{e}' for p,e in l)
            f.write( s + '\n' )