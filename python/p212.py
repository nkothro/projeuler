# 328968937309 is known correct answer
# but my god this is slow as all hell
import numpy as np
import itertools
import bisect

from collections import namedtuple, defaultdict, Counter
from itertools import product


from multiprocessing import Pool

Cuboid = namedtuple("Cuboid", ["x0", "y0", "z0", "dx", "dy", "dz"])

# def default_false():
#     return False
# false_dict = defaultdict(default_false)


class MergedCuboidSpace:
    def __init__(self, startcube: Cuboid):
        xmin, xmax = startcube.x0, startcube.x0 + startcube.dx + 1
        ymin, ymax = startcube.y0, startcube.y0 + startcube.dy + 1
        zmin, zmax = startcube.z0, startcube.z0 + startcube.dz + 1

        ydict = {yi: [slice(zmin, zmax)] for yi in range(ymin, ymax)}
        self._xdict = {xi: ydict.copy() for xi in range(xmin, xmax)}

    def contains(self, x, y, z):
        try:
            ydct = self._xdict[x]
            zsllst = ydct[y]
        except KeyError:
            return False
        for sl in zsllst:
            if sl.start <= z and z < sl.stop:
                return True
        return False

    def add_cuboid(self, box: Cuboid):
        x0, x1 = box.x0, box.x0 + box.dx + 1
        y0, y1 = box.y0, box.y0 + box.dy + 1
        z0, z1 = box.z0, box.z0 + box.dz + 1

        for xi in range(x0, x1):
            if xi not in self._xdict:
                self._xdict[xi] = {}
            for yi in range(y0, y1):
                # TODO: for each yi, merge zslices
                if yi not in self._xdict[xi]:
                    self._xdict[xi][yi] = [slice(z0, z1)]
                pass


def lagged_fibo_gen(upper_limit):
    if upper_limit < 100:
        upper_limit = 100
    k = 0
    s = np.zeros(upper_limit + 1, dtype=np.ulonglong)
    while k <= 55:
        s[k] = 100_003 - 200_003 * k + 300_007 * k**3
        s[k] = 100_003 - 200_003 * k + 300_007 * k**3
        k += 1
        # s[k] %= 1_000_000

    s %= 1_000_000
    while 56 <= k < upper_limit:
        s[k] = (s[k - 24] + s[k - 55]) % 1_000_000
        k += 1

    return s.astype(np.ulonglong)


def cuboid_params(n, s_mod399, s_mod10k):
    six_n = 6 * n
    x = s_mod10k[six_n - 5]
    y = s_mod10k[six_n - 4]
    z = s_mod10k[six_n - 3]

    dx = 1 + s_mod399[six_n - 2]
    dy = 1 + s_mod399[six_n - 1]
    dz = 1 + s_mod399[six_n]

    # return Cuboid(x0=int(x), y0=int(y), z0=int(z), dx=int(dx), dy=int(dy), dz=int(dz))
    # return (int(x), int(y), int(z), int(dx), int(dy), int(dz))
    return (int(x), int(y), int(z), int(x+dx), int(y+dy), int(z+dz))


def biinsertunique(a, x):
    i = bisect.bisect_left(a, x)
    if not (i != len(a) and a[i] == x):
        a.insert(i, x)

def range_merge(range_tuple_list:list):
    ranges = sorted(range_tuple_list)
    merged = [ranges.pop(0)]

    while len(ranges) > 0:
        a0, a1 = merged[-1]
        b0, b1 = ranges.pop(0)

        if a1 < a0 or b1 < b0:
            raise ValueError('invalid range, an upper was greater than its lower')

        if a1 <  b0:
            merged.append((b0, b1))
        else:
            # print(f'merge of {(a0, a1)}, {b0,b1}')
            merged[-1] = (a0, max(a1, b1))

    return merged
    
def slice_sum(tup):
    max_y, max_z, vlist = tup
    yz_plane = np.zeros((max_y, max_z), dtype=bool)
    for rect in vlist:
        y0, y1, z0, z1 = rect
        yz_plane[y0:y1, z0:z1] |= True
    return yz_plane.sum()

if __name__ == "__main__":
    max_cuboid_index = 50_000
    upper = 10 * (max_cuboid_index + 1)
    s = lagged_fibo_gen(upper)
    s_mod399 = np.mod(s, 399).astype(np.ulonglong)
    s_mod10k = np.mod(s, 10_000).astype(np.ulonglong)

    # import matplotlib.pyplot as plt
    # plt.plot(s_mod10k, label='mod10k')
    # plt.plot(s_mod399, label='mod399')
    # plt.show()

    # test_ranges = [(1,8), (9,110)]
    # x = range_merge(test_ranges)
    # print(x)
    
    max_x = 0
    max_y = 0
    max_z = 0
    with open("cuboids.txt", "w") as f:
        for n in range(1, max_cuboid_index + 1):
            x, y, z, dx, dy, dz = cuboid_params(n, s_mod399, s_mod10k)
            max_x = max(max_x, x + dx)
            max_y = max(max_y, y + dy)
            max_z = max(max_z, z + dz)
            st = f"{n:>6} <({x:>5},{y:>5},{z:>5}),({dx:>5},{dy:>5},{dz:>5})>"
            f.write(st + "\n")

    print(max_x, max_y, max_z)

    # d = Counter()
    dict_x = defaultdict(lambda: defaultdict(lambda: []))
    running_tot = 0
    print(dict_x)
    for n in range(1, max_cuboid_index + 1):
        x, y, z, dx, dy, dz = cuboid_params(n, s_mod399, s_mod10k)
        vol = dx*dy*dz
        running_tot += vol
        # print(f'starting {n=:>3}, vol={vol:>10}')
        x1 = x + dx
        y1 = y + dy
        z1 = z + dz

        # print(dx*dy*dz)

        for xi, yi in product(range(x,x1), range(y,y1)):
            # d[(xi,yi)].append((z,z1))
            # dict_x[(xi,yi)] = range_merge(dict_x[(xi,yi)] + [(z,z1)])
            # dict_x[xi][yi] = range_merge(dict_x[xi][yi] + [(z,z1)])
            dict_x[xi][yi].append( (z,z1))
            dict_x[xi][yi] = range_merge(dict_x[xi][yi])
        
        if n % 500 == 0:
            print(n)


    tot = 0
    s = set()
    for k, v in dict_x.items():
        for v2 in v.values():
            tot += sum((y-x) for x,y in v2)

            # if j % 100 == 0:
            #     print(j, j/len(d))
    
    print(tot)
    print(tot / running_tot)
