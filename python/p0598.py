import numpy as np

from eulerlib import PrimesList

def prime_factorization(n:int, plist:PrimesList):
    factorization = np.zeros_like(plist.list)
    if n == 1:
        return factorization

    for j, pj in enumerate(plist.list):
        while n % pj == 0:
            factorization[j] += 1
            n /= pj
    
    return factorization

def num_of_divisors(factorization=None):
    return np.prod(factorization+1)    

if __name__ == '__main__':
    p = PrimesList(100)
    f_tot = np.zeros_like(p.list)

    N = 10

    for n in range(2, N+1):
        f = prime_factorization(n, p)
        f_tot += f
    print(np.prod(p.list ** f_tot))

    print(N, np.sum(f_tot))
    for i,j in [(1680,2160), (1800,2016), (1890, 1920)]:
        fi = prime_factorization(i, p)
        fj = prime_factorization(j, p)
        # print(i*j, num_of_divisors(fi), num_of_divisors(fj))
        print(i*j, np.sum(fi), np.sum(fj))


    # s = ''
    # for b, p in zip(p.list, f_tot):
    #     if p > 0:
    #         s += f'{b}**{p} + '
    # print(f'prime factorization of {N}! is {s}')


    

