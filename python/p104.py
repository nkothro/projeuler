
pandigital = [str(i) for i in range(1,10)]
print(pandigital)

pandigital.sort()
def check_pandigital_tailed(num: int):
    as_list = list(str(num))
    front = as_list[0:9]
    front.sort()
    back = as_list[-9:]
    back.sort()
    return ((pandigital==front) and (pandigital==back))


if __name__ == '__main__':
    k = 0
    fibs = [0, 1]
    while k < 100000:
        i = k % 2
        fibs[i] = fibs[0] + fibs[1]
        if check_pandigital_tailed(fibs[i]):
            print(k)
        k+=1


