import numpy as np

def count_points_given_distance(distance_squared, side_length_squared):

    if (distance_squared % side_length_squared) != 0:
        return 0
    
    upper_term_under_root = 4 * distance_squared // side_length_squared

    upper = int(np.sqrt(upper_term_under_root) + 1)
    perf_squares = np.arange(upper)**2

def count_valids(N):
    x = 0
    valids = []
    while x <= N:
        if (x**2 % 3) == 0:
            valids.append(x)

def upp_downright_coords_dist(n:int, l:int, sq=True):
    d2 = n**2 + l**2 - n*l
    if sq:
        return d2

    return np.sqrt(d2)

if __name__ == '__main__':
    import matplotlib.pyplot as plt

    upper = 5 * 10**11

    # triples = []
    # x = []
    # xn = []
    # xj = []
    # xmin = []
    # j = 0
    # for n in range(1, upper):
    #     xj.append(j) 
    #     xn.append(n)
    #     for l in range(n):
    #         d = upp_downright_coords_dist(n,l)
    #         x.append(d)
    #         triples.append((n,l,d))

    #         j += 1
    #         xmin.append(n*np.sqrt(3)/2)

    # plt.plot(x, '-o')
    # plt.plot(xmin,)
    # plt.plot(xj, xn)

    # plt.ylabel(r'$\frac{L^2}{s^2}$')

    # print(triples[0:18])
    # plt.show()

    # for L in range(upper):
    #     if (L**2 / 3) % 3 == 0 and (L ):
    #         print(L)

    L = 0
    sq = []
    while L < upper:
        if L % 90_000 == 0:
            print(f'{L/upper:%}')
         
        L += 3
        sq.append(L**2)

