import itertools
from collections import defaultdict

import numpy as np

def triangle(n):
    return int(n*(n+1)/2)

def square(n):
    return int(n**2)

def pentagonal(n):
    return int(n*(3*n - 1)/2)

def hexagonal(n):
    return int(n*(2*n - 1))

def heptagonal(n):
    return int(n*((5*n) -3)/2)

def octagonal(n):
    return int(n*((3*n)-1))

def sequence_numbers_in_range(start, stop, f):
    n = 0
    Pn = f(n)
    ret = []
    while Pn < stop:
        n += 1
        Pn = f(n)
        if Pn >= start:
            ret.append(Pn)
    
    return ret

def cyclic_permutations(I):
    i0 = I[0]
    for p in itertools.permutations(I[1:]):
        yield i0, *p

if __name__ == '__main__':
    d = {}
    for j, f in enumerate([triangle, square, pentagonal, hexagonal, heptagonal, octagonal]):
        d[j+3] = np.asarray(sequence_numbers_in_range(1000, 10000, f), dtype=np.uint16)
    
    mapping = {}
    figurates = list(d.keys())
    t = 0
    for i, j in itertools.permutations(figurates, 2):
        x = defaultdict(lambda: False)
        for n, m in itertools.product(d[i], d[j]):
            if n % 100 == m // 100:
                x[n] = m
        mapping[(i,j)] = x

    for j, perm in enumerate(cyclic_permutations(figurates)):
        x0 = perm[0]
        x1 = perm[1]
        print(mapping[x0,x1])