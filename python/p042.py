import numpy as np

def triangular(n):
    return int(n*(n+1)/2)


alpha_lut = {chr(i+96) : i for i in range(27)}
def wordscore(word: str):
    return sum([ alpha_lut[char] for char in word.lower()])

def is_triangular_num(n: int):
    y = 0.5 * (np.sqrt(8*n + 1) - 1)
    if int(y) == y:
        return True
    
    return False

def make_triangular_lut(max_number):
    d = {}
    j = 1
    t = 1
    while t <= max_number:
        d[t] = True
        j += 1
        t = int(j*(j+1)/2)
    return d
        

# def check_score_in_list(n: int, list_to_check: list):
#     if n in list_to_check:
#         return True
#     else:
#         return False

if __name__ == '__main__':
    import  pathlib as pl
    f = pl.Path(r'D:\NELSON\Documents\repos\projeuler\python\p042_words.txt')
    wl = open(f, 'r').read().split(',')

    score_list = []
    count = 0
    for word in wl:
        z = wordscore(word)
        score_list.append(z)
        if is_triangular_num(z):
            print(word, z)
            count += 1

    # print(wordscore('sky'))
    print(count)