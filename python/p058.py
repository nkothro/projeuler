from eulerlib import PrimesList


def corners(n: int):
    if n == 1:
        raise ValueError("Square containing only 1 does not have corners")
    if n % 2 == 0:
        raise ValueError("Side length will always be odd in this problem")
    corners = []
    max_corner = n**2
    for j in range(4):
        corners.append(max_corner - j * (n - 1))
    return corners


def intersection(A, B):
    return list(set(A) & set(B))


def count_prime(numlist: list, primes):
    if not primes:
        primes = PrimesList(max(numlist) + 1).list

    return len(intersection(numlist, primes))


def upper_right(n, start=1):
    # upper right diagonal of spiral
    # A054554: 1, 3, 13,...
    for i in range(start, n + 1):
        yield 4 * i**2 - 10 * i + 7


def upper_left(n, start=1):
    # upper left diagonal of spiral
    # 1, 5, 17 A053755
    for i in range(start-1, n):
        yield 4 * (i**2) + 1


def bottom_left(n, start=1):
    # bottom left diagonal of spiral
    # 1, 7, 21 A054569
    for i in range(start, n + 1):
        yield 4 * i**2 - (6 * i) + 3


def bottom_right(n, start=1):
    # bottom right arm is odd perfect squares
    for i in range(start, n + 1):
        yield (2 * i - 1) ** 2


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    # j = 3      # start with a 3x3 square, going from 1 to 9 in spiral
    # ratio = 1  # Assume all corners are primes at start
    # cornlst = []
    # primelist = list(PrimesList(101 ** 2 + 1).list)
    # while ratio >= .1:
    #     cornlst.extend(corners(j))
    #     ratio = count_prime(cornlst, primelist)/len(cornlst)
    #     print(ratio)
    #     j += 2

    # print(j)
    N = 5*10000
    start=2
    fns = [upper_left, upper_right, bottom_left, bottom_right]
    lut = {fn.__name__ : [j for j in fn(N,start=start)] for fn in fns}

    max_p = max(v[-1] for k, v in lut.items())
    print(max_p)

    plist = PrimesList(max_p)

    primes_ct = 0
    ratios = []
    sides = []
    for j in range(N-start):
        side_length = 2*j + 3

        # total_numbers = side_length**2
        numbers_on_diagonal = 2*side_length - 1

        for k, v in lut.items():
            if plist.is_prime(v[j]):
                primes_ct += 1

        sides.append(side_length)
        ratios.append(primes_ct/numbers_on_diagonal)

        if numbers_on_diagonal > (10*primes_ct):
            print(f'{j=}, diagct={numbers_on_diagonal}, {primes_ct=}')
            print(f'{side_length=}')
            break
    
    plt.plot(sides, [r for r in ratios])
    plt.yscale('log')
    plt.show()

