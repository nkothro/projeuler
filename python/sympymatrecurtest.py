from itertools import product

import sympy
import numpy as np


def sliding_mat(N):
    symbols_A = sympy.symbols(' '.join(f'a{i}' for i in range(2*N-1)))

    # middle element of symbols_A has index=N
    m = []
    for i in range(N):
        row = []
        for j in range(N):
            row.append(symbols_A[N-1-j+i])
        m.append(row)

    M = sympy.Matrix(m)
    return M

def gen_first_entry_sequence(matrix, n)->list:
    z = []
    z.append(matrix[0,0])
    m = matrix
    # q = np.zeros(matrix.shape[0], dtype=int)
    # q[0]=1
    for j in range(n):
        # print(np.matmul(np.matmul(q, m), q))
        m = np.matmul(m, matrix)
        z.append(m[0,0])
    return z

def recursive_coeffs_from_seq(seq:list[int], dim):
    A = np.zeros((dim,dim))
    b = np.zeros(dim)

    for i, j in product(range(dim), repeat=2):
        idx = dim - 1 - j + i
        A[i,j] = z[idx]
        b[i] = z[dim+i]

    # want to solve for x in Ax=b here 
    x = np.linalg.solve(A, b)
    return x, z

def recursive_form_from_cayley_hamilton(A: sympy.ImmutableMatrix):
    # A is m x m matrix
    m = A.shape[0]
    poly_coeffs = A.charpoly().coeffs()
    # for n = 0 to n = m-1, need copy of matrix's (0,0) entry
    entries_00 = [0, A[0,0]]
    while len(entries_00) < m:
        A *= A
        entries_00.append(A[0,0])

if __name__ == '__main__':
    from p886b import create_gcd_mat, mask_indices_from_num
    from numpy.linalg import matrix_power

    N = 7
    A = create_gcd_mat(N)
    b = mask_indices_from_num(int('10101', 2))
    M = np.matrix(A[b][:,b], dtype=object)
    # m2 = np.matmul(M,M)

    symA = sympy.ImmutableDenseMatrix(A)
    poly = symA.charpoly('x')
    print(poly)
    print(poly.coeffs())
    z = gen_first_entry_sequence(M, 20)
    # print(z)
    for j in range(M.shape[0]+1 ):
        x, z= recursive_coeffs_from_seq(z, j)
        # print(j, x)
        # print(z)
        
    dim = 2
    x, z = recursive_coeffs_from_seq(z, 3)
    x = np.asarray([2, 1], dtype=int)
    ztest = z[0:dim]
    for j in range(10):
        # aj = v[0]*ztest[-1] + v[1]*ztest[-2] + 
        aj = sum(x[i]*ztest[~i] for i in range(dim))
        ztest.append(aj)
    # print(ztest)
