# Trying to make a more efficient version of my solution to 12
from math import floor

from eulerlib import PrimesList, prime_factorization, prime_factorization_with_lut

def tri_generator(j):
    # makes a list of the first j triangular number
    # trilist=[1]
    running = 0
    for i in range(0, j+1):
        yield running
        running += i
        # trilist.append(trilist[-1] + i)
    

def countdivisors(num):
    # counts total number of divisors of a whole number
    # has possible off by one or two error
    count=0
    sqrt=num**(0.5)
    upperbound=int(floor(sqrt))
    for i in range(1, upperbound):
        if num % i == 0:
            count=count+1
    return(2*count)


def pf_to_number_of_divisors(pf:dict):
    ct = 1
    for v in pf.values():
        ct *= 1 + v
    return ct


if __name__ == '__main__':
    bigtrilist = tri_generator(10)
    pl = PrimesList(1_000_000)
    pf_lut = {}

    # for Tk in bigtrilist:
    #     print(Tk)


    pf_lut = {}
    n = 1

    print(prime_factorization_with_lut(4, pf_lut, pl))
    print(prime_factorization_with_lut(16*125*17, pf_lut, pl))

    while n < 25000:
        pf_n = prime_factorization_with_lut(n, pf_lut=pf_lut, primelist=pl)
        pf_n_plus_1 = prime_factorization_with_lut(n+1, pf_lut=pf_lut, primelist=pl)

        pf_tri_n = pf_n + pf_n_plus_1
        pf_tri_n[2] -= 1

        divisors_ct = pf_to_number_of_divisors(pf_tri_n)
        print(f' {n*(n+1)//2:>5}: {divisors_ct:>5} {pf_tri_n}')
        if divisors_ct >= 500:
            break
        n += 1
        
        
