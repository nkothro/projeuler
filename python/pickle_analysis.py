import collections
import pickle
import pathlib as pl

pkl_dir = pl.Path('./pkls')


# for strlen in range(2,35):
#     ctr = collections.Counter()
#     pkls = [d for d in pkl_dir.glob('*.pickle')]
#     for j, d in enumerate(pkls):
#         with open(d, 'rb') as f:
#             pkl_ctr = pickle.load(f)
        
#         for k, v in pkl_ctr.items():
#             if len(k) == strlen:
#                 pkl_ctr[k] += v
#         print(f'for {strlen=}, {j} of {len(pkls)}')
        
#     with open(f'counter_len{strlen}.pickle', 'wb') as f:
#         pickle.dump(ctr, f)
#     print('done with ', strlen)
            
pkls = [d for d in pkl_dir.glob('*.pickle')]
group = {j:collections.Counter() for j in range(1,35)}

for j, d in enumerate(pkls):

    with open(d, 'rb') as f:
        pkl_ctr = pickle.load(f)
    
    for k, v in pkl_ctr.items():
        strlen = len(k)
        # print(k)
        group[strlen][k] += v

    print('done with ', d, len(group[8]))
        
            

