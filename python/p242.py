def n_choose_k_is_odd(n:int, k:int):
    # p = n
    # q = n-k
    # while p and q:
    #     if (p & 1) and (q & 1):
    #         return False
    #     else:
    #         p >>= 1
    #         q >>= 1

    # b = 1
    # while (b <= n) or (b <= (n-k)):
    #     if (b & n) and (b & (n-k)):
    #         return False
    #     b <<= 1
    
    # return True

    q = n-k
    return bool(n & q) > 0
    # return (n & (n-k)) > 0

def f_odd_count(n, k):
    # the f(n,k) given in the problem statement
    n_odd = n & 1
    if n_odd:
        m1 = (n+1)//2
        m2 = m1 - 1

        k_odd = 1
        count_odd = 0
        count_even = 0
        while k_odd <= n:
            if n_choose_k_is_odd(m1, k_odd) and n_choose_k_is_odd(m2, k-k_odd):
                count_odd += 1
            else:
                count_even += 1
            print(k_odd)
            k_odd += 2

        return count_odd


    else:
        # n even
        m = n//2
        k_odd = 1
        count_odd = 0
        count_even = 0
        while k_odd <= n:
            if n_choose_k_is_odd(m, k_odd) and n_choose_k_is_odd(m, k-k_odd):
                count_odd += 1
            else:
                count_even += 1

            k_odd += 2
        return count_odd



if __name__ == '__main__':
    import math
    import timeit

    # for j in range(8):
    #     stmt = f"all(1 for j in range({10**j}))"
    #     t = timeit.timeit(stmt)
    #     q = f"{stmt} | {t:.5f} | {t/10**j:.5f}"
    #     print(q)
    
    print(f_odd_count(5,1))
    
    # n = 6
    # for j in range(n+1):
    #     print(math.comb(n,j), n_choose_k_is_odd(n,j))