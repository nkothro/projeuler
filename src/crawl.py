import pathlib as pl
import urllib.request as urllib2

from multiprocessing import Pool


def get_proj_euler_problem_statement(number):
    url = f"https://projecteuler.net/minimal={number}"
    request = urllib2.urlopen(url)
    # try:
    #     txt = request.read().decode('utf-8')
    # except UnicodeDecodeError as e:
    #     txt = f'ERROR: {e.args}'

    txt = request.read().decode('utf-8')
    return number, txt

if __name__ == '__main__':
    p0, plast = 1, 700

    src = pl.Path('./src/')

    with Pool(6) as pool:
        for j, txt in pool.imap_unordered(get_proj_euler_problem_statement, range(p0, plast+1), chunksize=20):
            name = src / f'prob{j:>03}.txt'
            with open(name, 'w', encoding='utf-8') as f:
                f.write(txt)
