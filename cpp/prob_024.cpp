#include <algorithm>
#include <stdint.h>

#include "eulerlib.h"

#define TIMING

#ifdef TIMING
#include <chrono>
#endif//TIMING

template<typename T>
std::vector<T> naiveImplementation(T n)
{
	//Naive implementation, using next permut
	//Produces nth lexicographic permutation of digits 0 through 9
#ifdef TIMING
	auto t1 = std::chrono::high_resolution_clock::now();
#endif//TIMING

	std::vector<unsigned int> p;
	for(unsigned int j=0; j<10; j++)
	{
		p.push_back(j);
	}

	for(unsigned int j=1; j<1000000; j++)
	{
		std::next_permutation(p.begin(), p.end());
	}
#ifdef TIMING
	auto t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double, std::milli> ms = t2 - t1;
	std::cout << ms.count() << "ms\n";
#endif//TIMING

	return p;
}

template<typename T>
std::vector<T> convertToFactorialRadix(T num)
{
	/*
	* 0th element is for radix of 0!
	* 1st element is for radix of 1!
	* nth element is for radix of n!
	*
	* radix 0 has largest digit of 0 allowed, get via divmod with 1
	* radix 1 has largest digit of 1 allowed, get via divmod with 2
	* radix 2 has largest digit of 2 allowed, get via divmod with 3
	* radix n has largest digit of n allowed, get via divmod with (n-1)
	*
	*/

	std::vector<T> factoradix;
	T j = 1;
	while(num != 0)
	{
		factoradix.push_back(num % j);
		num /= j;
		j++;
	}
	return factoradix;
}

template<typename T>
std::vector<T> getPermutation(T n, T maxDigit)
{
	// useful info: https://en.wikipedia.org/wiki/Factorial_number_system#Permutations
	std::vector<T> d, perm, factoradix;

	//there is no nth permutation if n > possible number of perumutations
	if(factorial(maxDigit) > n) { return perm; }

	//uses factoradix and lehmer code to get the nth perumtation of digits 0 through 9
	factoradix = convertToFactorialRadix(n);

	for(T j=0; j<= maxDigit; j++)
	{
		d.push_back(j);
	}

	/* convertToFactoradix function places smallest place-value in smallest
	 * index effectively making the number "written backwards" if index 0 is
	 * the "left end" of the vector
	 */
	T k;
	T j = factoradix.size();
	while(j > 0)
	{
		j--;
		k = factoradix[j];
		perm.push_back(d[k]);
		auto kth = d.begin() + k;
		d.erase(kth);
	}
	return perm;

}

int main(void)
{
#ifdef TIMING
	auto t1 = std::chrono::high_resolution_clock::now();
#endif//TIMING

	std::vector<uint32_t> f, p;
	uint32_t n = 1000000-1;
	uint32_t maxDigit = 9;
	//p = naiveImplementation(n);

	f = convertToFactorialRadix(n);
	p = getPermutation(n, maxDigit);
	printVector(p);
#ifdef TIMING
	auto t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double, std::milli> ms = t2 - t1;
	std::cout << ms.count() << "ms\n";
#endif//TIMING
	
	p = naiveImplementation(n);
	printVector(p);

}
