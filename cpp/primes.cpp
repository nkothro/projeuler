#include <stdint.h>
#include <vector>

#include "primes.h"

template  <typename T>
std::vector<T> SieveOfAtkin(T limit)
{
	// Initialise the sieve array with false values
	bool sieve[limit];
	for (T i = 0; i < limit; i++)
		sieve[i] = false;

	/* Mark siev[n] is true if one
	   of the following is true:
	a) n = (4*x*x)+(y*y) has odd number of
	   solutions, i.e., there exist
	   odd number of distinct pairs (x, y)
	   that satisfy the equation and
		n % 12 = 1 or n % 12 = 5.
	b) n = (3*x*x)+(y*y) has odd number of
	   solutions and n % 12 = 7
	c) n = (3*x*x)-(y*y) has odd number of
	   solutions, x > y and n % 12 = 11 */
	for (T x = 1; x * x < limit; x++) {
		for (T y = 1; y * y < limit; y++) {

			// Main part of Sieve of Atkin
			T n = (4 * x * x) + (y * y);
			if (n <= limit && (n % 12 == 1 || n % 12 == 5))
				sieve[n] ^= true;

			n = (3 * x * x) + (y * y);
			if (n <= limit && n % 12 == 7)
				sieve[n] ^= true;

			n = (3 * x * x) - (y * y);
			if (x > y && n <= limit && n % 12 == 11)
				sieve[n] ^= true;
		}
	}

	// Mark all multiples of squares as non-prime
	for (T r = 5; r * r < limit; r++) {
		if (sieve[r]) {
			for (T i = r * r; i < limit; i += r * r)
				sieve[i] = false;
		}
	}

	std::vector<T> primes;
	primes.push_back(2);
	primes.push_back(3);
	for (T a = 5; a < limit; a++)
	{
		if (sieve[a]) { primes.push_back(a); }
	}
	return primes;
}
