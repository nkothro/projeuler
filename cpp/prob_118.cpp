//#include "eulerlib.h"
#include <vector>
#include <stdint.h>
#include <iostream>

uint8_t grabShiftLastDigit(uint32_t bignum, uint8_t base)
{
	uint8_t digit;
	digit = bignum % base;
	bignum -= digit;
	bignum /= base;

	return digit;
}

std::vector<uint32_t> SieveOfAtkin(uint32_t limit)
{
	// Initialise the sieve array with false values
	bool* sieve = new bool[limit];
	for (uint32_t i = 0; i < limit; i++)
		sieve[i] = false;

	/* Mark siev[n] is true if one
	   of the following is true:
	a) n = (4*x*x)+(y*y) has odd number of
	   solutions, i.e., there exist
	   odd number of distinct pairs (x, y)
	   that satisfy the equation and
		n % 12 = 1 or n % 12 = 5.
	b) n = (3*x*x)+(y*y) has odd number of
	   solutions and n % 12 = 7
	c) n = (3*x*x)-(y*y) has odd number of
	   solutions, x > y and n % 12 = 11 */
	for (uint32_t x = 1; x * x < limit; x++) {
		for (uint32_t y = 1; y * y < limit; y++) {

			// Main part of Sieve of Atkin
			uint32_t n = (4 * x * x) + (y * y);
			if (n <= limit && (n % 12 == 1 || n % 12 == 5))
				sieve[n] ^= true;

			n = (3 * x * x) + (y * y);
			if (n <= limit && n % 12 == 7)
				sieve[n] ^= true;

			n = (3 * x * x) - (y * y);
			if (x > y && n <= limit && n % 12 == 11)
				sieve[n] ^= true;
		}
	}

	// Mark all multiples of squares as non-prime
	for (uint32_t r = 5; r * r < limit; r++) {
		if (sieve[r]) {
			for (uint32_t i = r * r; i < limit; i += r * r)
				sieve[i] = false;
		}
	}

	std::vector<uint32_t> primes;
	primes.push_back(2);
	primes.push_back(3);
	for (uint32_t a = 5; a < limit; a++)
	{
		if (sieve[a]) { primes.push_back(a); }
	}
	delete sieve;
	return primes;
}

bool hasUniqueDigits(uint8_t num)
{
	// No primes in this problem have more than 10 digits
	uint8_t dig[10] = {0};	
	uint8_t d, b, j, k;
	b = 10;
	k = 0;
	
	if((0 < num) && (num < 10)) { return true; }

	while((num > 0) && k < 10)
	{
		d = grabShiftLastDigit(num, b);
		//Does it have any 0s as a digit
		if(d == 0) {
			return false; 
		}

		//Has this digit appeared yet
		for(j=0; j<=k; j++)
		{
			std::cout << d << std::endl;
			if(d == dig[j]) {
				return false;
			}
		}

		dig[k++] = d;
	}
	return true;
}


int main(void)
{
	//uint32_t upper = 987654321;
	uint32_t upper = 100;
	std::vector<uint32_t> v = SieveOfAtkin(upper);
	std::vector<uint32_t> uniqs;
	for(auto i : v)
	{
		if(hasUniqueDigits(i)) 
		{
			std::cout << i << " YES" << std::endl;
		}
		else
		{
			std::cout << i << " NO" << std::endl;
		}
	}
}

