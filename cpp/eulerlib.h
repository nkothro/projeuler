#ifndef EULERLIB_H
#define EULERLIB_H

#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <cmath>
#include <vector>

template<typename T, typename V>
T integerPow(const T base, const V exponent)
{
	T retval = 1;
	for(V j=1; j<=exponent; j++)
	{
		retval *= base;
	}
	return retval;
}

template<typename T, typename V>
T getDecimalDigit(T bigNum, V k)
/* get the kth digit of bigNum as its represented in decimal notation */
{
	uint16_t powerOfTen, lower, relevant, shrunk;

	powerOfTen = integerPow(10, k);
	relevant = bigNum % powerOfTen;
	shrunk = (T)((bigNum - relevant) / powerOfTen) % 10;

	return shrunk;
}

template<typename T, typename V>
T grabShiftLastDigit(T& bignum, V base)
{
	T digit;
	digit = bignum % base;
	bignum -= digit;
	bignum /= base;

	return digit;
}

template<typename T, typename V>
T digitalSum(T num, V base)
{
	T sum = 0;
	while(num > 0)
	{
		sum += grabShiftLastDigit(num, base);
	}
	return sum;
}

template<typename T>
void printVector(const std::vector<T> v)
{
	for(T j = 0; j<v.size(); j++)
	{
		std::cout << v[j] << " ";
	}
	std::cout << std::endl;
}

template<typename T>
T factorial(T n)
{
	T prod = 1;
	while(n>=1)
	{
		prod *= n--;
	}
	return prod;
}

template<typename T>
T perm(T n, T k)
{
	T prod = 1;
	T low = n - k;
	while(n > low)
	{
		prod *= n--;
	}
	return prod;
}

//not the most efficient 
template<typename T>
T comb(T n, T k)
{
	T r = std::min(k, n-k);

	T p = perm(n, r);
	p /= factorial(r);

	return p;
}

template<typename T>
T choose(T n, T k)
{
	/*
	if(k > n || k < 0) { return 0; }
	if(k == n || k == 0) { return 1; }
	
	T r = std::min(k, n-k);

	std::vector<T> primes = SieveOfAtkin(r);

	// indices for iterating
	uint16_t iup, idn, j;

	T num[n-k] = {0};
	T denom[r] = {0};

	for(iup = n-r; iup > 0; iup--)
	{
		num[iup] = iup;
	}
	for(idn = r; idn > 0; idn--)
	{
		denom[idn] = idn;
	}

	// ToDo: loop through numerator and denominator, cancelling out common factors
	*/

	// According to stack overflow, this runs in O(k)
	if(k==0) { return 1;}
	return(n * choose (n - 1, k - 1) / k)

}

#endif//EULERLIB_H

