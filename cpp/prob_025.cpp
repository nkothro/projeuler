#include "eulerlib.h"

template<typename T>
std::vector<T> sumOfTwoVectors(std::vector<T> a, std::vector<T> b, const T base)
{
	//assumes index 0 is the "0's" place
	std::vector<T> c;
	if(a.size() != b.size()) { return c; }

	c.assign(a.size(), 0);
	
	for(T j=0; j < a.size(); j++)
	{
		c[j] += a[j] + b[j];
		if(c[j] >= base)
		{
			c[j+1]++;
			c[j] %= base;
		}
	}
	
	return c;
}

int main(void)
{
	std::vector<uint32_t> a, b, c, t;
	uint32_t base = 10;
	uint32_t digitsCt = 1001;
	uint32_t j;
	
	a.assign(digitsCt, 0);
	b.assign(digitsCt, 0);
	c.assign(digitsCt, 0);

	//first two fibonacci numbers
	a[0] = 1;
	b[0] = 1;

	j = 2;
	while(true)
	{
		c = sumOfTwoVectors(a, b, base);
		a = b; // don't need a anymore
		b = c;

		j++; // value of bis now jth fibo
		if(b[999] > 0) { break; }
	}
	printVector(b);
	std::cout <<  std::endl << j << std::endl;
}
