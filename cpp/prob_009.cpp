#include <stdint.h>
#include <stdio.h>
#include <cmath>

int main(void)
{
	uint16_t a = 0, b = 0, c = 0, m, n, k, s;
	uint16_t msq, nsq;
	
	for(m = 2; m < 20; m++)
	{
		for(n = 1; n < m; n++)
		{
			if((n % 2 == 0) || (m % 2 == 0))
			{
				msq = pow(m, 2);
				nsq = pow(n, 2);
				a = msq - nsq;
				b = 2 * m * n;
				c = msq + nsq;

				//if(a*a + b*b == c*c) { printf("(m,n)=(%d,%d) | (a,b,c) = (%d,%d,%d), a*b*c=%d\n", m, n, a, b, c, a*b*c); }

				k = 1;
				s = a + b + c;
				while(s <= 1000)
				{
					if(s == 1000) { printf("a=%d, b=%d, c=%d, sum=%d, a*b*c=%d\n", k*a, k*b, k*c, s, k*k*k*a*b*c); }
					s = (k++)*(a + b + c);
				}
			}
		}
	}
}
