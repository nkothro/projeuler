#include <iostream>
#include <cstdint>
#include <vector>

std::vector<uint64_t> SieveOfAtkin(uint64_t limit)
{
	// Initialise the sieve array with false values
	bool sieve[limit];
	for (uint64_t i = 0; i < limit; i++)
		sieve[i] = false;

	/* Mark siev[n] is true if one
	   of the following is true:
	a) n = (4*x*x)+(y*y) has odd number of
	   solutions, i.e., there exist
	   odd number of distinct pairs (x, y)
	   that satisfy the equation and
		n % 12 = 1 or n % 12 = 5.
	b) n = (3*x*x)+(y*y) has odd number of
	   solutions and n % 12 = 7
	c) n = (3*x*x)-(y*y) has odd number of
	   solutions, x > y and n % 12 = 11 */
	for (uint64_t x = 1; x * x < limit; x++) {
		for (uint64_t y = 1; y * y < limit; y++) {

			// Main part of Sieve of Atkin
			uint64_t n = (4 * x * x) + (y * y);
			if (n <= limit && (n % 12 == 1 || n % 12 == 5))
				sieve[n] ^= true;

			n = (3 * x * x) + (y * y);
			if (n <= limit && n % 12 == 7)
				sieve[n] ^= true;

			n = (3 * x * x) - (y * y);
			if (x > y && n <= limit && n % 12 == 11)
				sieve[n] ^= true;
		}
	}

	// Mark all multiples of squares as non-prime
	for (uint64_t r = 5; r * r < limit; r++) {
		if (sieve[r]) {
			for (uint64_t i = r * r; i < limit; i += r * r)
				sieve[i] = false;
		}
	}

	std::vector<uint64_t> primes;
	primes.push_back(2);
	primes.push_back(3);
	for (uint64_t a = 5; a < limit; a++)
	{
		if (sieve[a]) { primes.push_back(a); }
	}
	return primes;
}

int main(void)
{
	//uint64_t upper_limit = 775147;
	uint64_t upper_limit = 775147;
	uint64_t big_num = 600851475143 ;
	uint64_t j = 0;
	uint64_t m = 0;


	std::vector<uint64_t> k;
	//printf("made it here\n");
	k = SieveOfAtkin(upper_limit);
	for(j=1; j <= k.size(); j++)
	{
		//std::cout << k.data()[j];
		uint64_t p = k.data()[j-1];
	//	printf("%10lu", p);
		if(big_num % p == 0)
		{
			printf("%7lu\n", p);
			m++;
			if (!(m % 16)) { printf("\n"); }
		}
	}
}



