#include <stdint.h>
#include <iostream>
#include <stdio.h>
#include <vector>

#include "primes.h"

int main(void)
{
	/*
	 * (1/x) + (1/y) = (1/n)
	 * Let x = n+i, y = n+j, then n^2 = i*j Total number of integer solutions
	 * is then number of possible (i,j) pairs
	 *
	 * Let d(k) be the number of integer divisors of k.  If k is a perfect
	 * square, it will have an odd number of divisors.  Else it will have an
	 * even number of divisors.
	 *
	 * Since (i, j) and (j, i) represent same solution, if n^2 has even number
	 * of divisors, divide by 2. But d(n^2) will be odd, so want:
	 *   (d(n^2) - 1)/2 + 1
	 */
	
	uint32_t min_num_solutions = 1000;
	uint32_t n = 1000;
	uint32_t max_prime = 1 << 16;
	uint32_t sol = 0;
	uint32_t dn;
	

	std::vector<uint32_t> primes = SieveOfAtkin(max_prime);
	while(sol < min_num_solutions && n < max_prime)
	{
		dn = numberOfDivisors((uint32_t)(n*n), primes);
		sol = (dn -1)/2 + 1;
		printf("n: %u, #pairs: %u\n", n, sol);
		n++;
	}
}

