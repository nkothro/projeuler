#include <stdint.h>
#include <stdio.h>
#include <math.h>

template<typename T>
T numDivisors(const T num)
{
	T ct = 1; // 1 is always a divisor

	T upper = (T)(sqrt(num) + 1);
	for(T j=2; j<upper; j++)
	{
		if(!(num%j))
		{
			printf("%u\n",j);
			if(j*j==num) { ct++; }
			else { ct += 2; }
		}
	}
	return ct;
}

template <typename T>
T divisorSum(const T num)
{
	T s = 1; // 1 is always a divisor

	T upper = (T)(sqrt(num));
	for(T j = 2; j < upper; j++)
	{
		if(!(num%j))
		{
			s += j;
			s += num/j;
		}
	}
	if((upper * upper) == num) { s += upper; }
	return s;
	
}

int main(void)
{
	uint32_t j, a, s;
	s = 0;
	for(j=4; j<10000; j++)
	{
		a = divisorSum(j);
		if((a!=j) && (j==divisorSum(a)))
		{
			printf("%u and %u\n", a, j);
			s += j;
		}
	}

	printf("%u\n",s);
}

