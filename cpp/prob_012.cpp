#include <stdint.h>
#include <iostream>
#include <math.h>

uint32_t triang(uint16_t num)
{
	return num * (num + 1) / 2;
}

uint32_t num_divisors(uint32_t num)
{
	uint32_t upper, j, divisorCount;
	double root;

	divisorCount = 2; // 1 and number itself are always divisors
	root = sqrt(num);

	if((floor(root) == root) and (num % (uint32_t)root == 0))
	{
		//if num is a perfect square, account for its square root as a divisor
		divisorCount += 1;
	}

	upper = (uint32_t)(ceil(sqrt(num)));
	for(j=2; j < upper; j++)
	{
		if(!(num %j)) { divisorCount+=2; }
	}

	return divisorCount;
}

int main(void)
{
	uint32_t j, c, t;

	j = 0;
	while(c < 500)
	{
		j++;
		t = triang(j);
		c = num_divisors(t);
	}
	
	std::cout << "j " << j << std::endl;
	std::cout << "t " << t << std::endl;
	std::cout << "c " << c << std::endl;
}
