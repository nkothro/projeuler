#include <stdint.h>
#include <iostream>

template<typename T>
T up_right(T n)
{
	//Squares of odd numbers
	T j = (2*n + 1);
	return j * j;
}

template<typename T>
T down_right(T n)
{
	return 4*n*n - (2*n) + 1;
}

template<typename T>
T down_left(T n)
{
	T j = 2*n;
	return j*j + 1;
}

template<typename T>
T up_left(T n)
{
	return (4*n*n) + (2*n) + 1;
}

int main(void)
{
	//each diagonal follows specific sequence of numbers
	uint64_t s = 1;
	for(uint64_t j = 1; j <= 500; j++)
	{
		s += up_right(j);
		s += down_right(j);
		s += up_left(j);
		s += down_left(j);
	}
	std::cout << s << std::endl;
}
