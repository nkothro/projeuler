#include <stdint.h>
#include <iostream>
#include "primes.h"

template<typename T>
int numberOfPrimes(int a, int b, const std::vector<T> primes)
{
	int n = 0;
	int res = (n*n) + (a*n) + b;
	while(std::binary_search(primes.begin(), primes.end(), res))
	{
		n++;
		res = (n*n) + (a*n) + b;
	}
	return n;

}

int main(void)
{
	int32_t maxPrime = (2 * 1000 * 1000) + 1000;
	std::vector<int32_t> p = SieveOfAtkin(maxPrime);
	int32_t j, n, a, b, max_n, max_a, max_b;

	max_n = 0;
	max_a = 0;
	max_b = 0;

	j = 0;
	while(b < 1000)
	{
		b = p[j];	
		for(a=-999; a<1000; a++)
		{
			n = numberOfPrimes(a, b, p);
			if(n > max_n)
			{
				max_n = n;
				max_a = a;
				max_b = b;
				std::cout<< "n^2 + "<< a <<"n + "<<b <<" produces run of " << n << " consecutive primes" << std::endl;
			}
		}
		j++;
	}
	std::cout << "Product of coefficients of a, b producing largest consecutive run of primes " <<  max_a * max_b;
}
