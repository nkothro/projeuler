#include "prob_022.h"
#include <cstdio>
#include <bits/stdc++.h>

uint32_t alphaValue(std::string s)
{
	uint32_t sum = 0;
	for(int j = 0; j < s.length(); j++)
	{
		sum += (uint8_t)s[j] - 64;
	}
	return sum;
}

int main(void)
{
	std::vector<std::string> v(arr, arr  + sizeof(arr)/sizeof(arr[0]));
	uint32_t sz = v.size();
	uint64_t running_sum = 0;

	std::sort(v.begin(), v.end());
	for(int s = 0; s < sz; s++)
	{
		running_sum += (s+1)*alphaValue(v[s]);
	}
	printf("total: %u", running_sum);
}
