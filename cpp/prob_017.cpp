#include <iostream>
#include <string>
#include <algorithm>

uint16_t integerPow(const uint32_t base, const uint32_t exponent)
{
	uint16_t retval = 1;
	for(uint32_t j=1; j<=exponent; j++)
	{
		retval *= base;
	}
	return retval;
}

uint16_t getDecimalDigit(uint16_t bigNum, uint32_t k)
/* get the kth digit of bigNum as its represented in decimal notation */
{
	uint16_t powerOfTen, lower, relevant, shrunk;

	powerOfTen = integerPow(10, k);
	relevant = bigNum % powerOfTen;
	shrunk = (uint16_t)((bigNum - relevant) / powerOfTen) % 10;

	return shrunk;
}

std::string singleDigitToWord(uint16_t n)
{
	switch(n)
	{
		// base cases: 1 through 9;
		case 1: return "one";
		case 2: return "two";
		case 3: return "three";
		case 4: return "four";
		case 5: return "five";
		case 6: return "six";
		case 7: return "seven";
		case 8: return "eight";
		case 9: return "nine";
		default: return "";
	}
}

std::string digitToWordTeens(uint16_t n)
{
	if(n==11) { return "eleven"; }
	else if(n==12) { return "twelve"; }
	else if(n==13) { return "thirteen"; }
	else if(n==15) { return "fifteen"; }
	else {
		std::string val;
		switch(n)
		{
			case 14: val = "four" ; break;
			case 16: val = "six"  ; break;
			case 17: val = "seven"; break;
			case 18: val = "eight"; break;
			case 19: val = "nine" ; break;
		}
		return val + "teen";
	}
}

std::string tensPlaceToWord(uint16_t tensDigit)
{
	switch(tensDigit)
	{
		case 1: return "ten";
		case 2: return "twenty";
		case 3: return "thirty";
		case 4: return "forty";
		case 5: return "fifty";
		case 6: return "sixty";
		case 7: return "seventy";
		case 8: return "eighty";
		case 9: return "ninety";
		default: return "";
	}
}

std::string twoDigitsToWord(uint16_t n)
{
	if(99 < n) { return ""; }
	else if(n < 10) { return singleDigitToWord(n); }
	else if((n > 10) && (n < 20)) { return digitToWordTeens(n); }
	else
	{
		return tensPlaceToWord(getDecimalDigit(n, 1)) + " " + singleDigitToWord(getDecimalDigit(n, 0));

	}
}

std::string threeDigitsToString(uint16_t num)
{
	uint16_t d = getDecimalDigit(num, 2);
	if(!(num % 100 == 0))
	{
		return singleDigitToWord(d) + " hundred and " + twoDigitsToWord(num % 100);
	}
	else
	{
		return singleDigitToWord(d) + " hundred";
	}
}

int main(void)
{
	uint16_t num = 0;
	uint32_t ct = 0;
	std::string numstr;

	for(num=1; num <= 1000; num++)
	{
		if(num < 100) { numstr = twoDigitsToWord(num); }
		else if((num >= 100) && num < 1000) { numstr = threeDigitsToString(num); }
		else {numstr = "one thousand"; }

		ct += numstr.length() - std::count(numstr.begin(), numstr.end(), ' ');
		std::cout << numstr << " :" << numstr.length() << std::endl;
	}
	std::cout << "TOTAL: " << ct << std::endl;
}



