#include "fraction.h"

bool Fraction::isLessThan(Fraction other)
{
	return (numerator * other.denominator < other.numerator * denominator);
}

bool Fraction::isGreaterThan(Fraction other)
{
	return (numerator * other.denominator > other.numerator * denominator);
}

bool Fraction::isEqualTo(Fraction other)
{
	return (numerator * other.denominator == other.numerator * denominator);
}

bool Fraction::isReduced()
{
	return (std::__gcd(numerator, denominator)==1);
}
