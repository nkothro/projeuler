#include <cmath>
#include <iostream>
#include <map>

std::map<uint32_t, uint32_t> collatzVals;

uint32_t collatzNext(uint32_t n)
{
	if(n % 2) { return 3*n + 1; }
	else { return n/2; }
}

uint32_t collatzLength(uint32_t num)
{
	uint32_t j = 1; 
	uint32_t v, original;
	
	original = num;
	while(num != 1)
	{
		j++;
		v = collatzVals[num];
		if(v != 0)
		{ 
			j += v;
			break;
		}
		else
		{
			num = collatzNext(num);
		}
	}
	collatzVals[original] = j;
	return j;
}

int main(void)
{
	uint32_t s, len;
	uint32_t longest_start = 2;
	uint32_t longest_length = 1;

	for(s=2; s<1000000; s++)
	{
		len = collatzLength(s);
		//std::cout << s << std::endl;
		
		if(len > longest_length)
		{
			longest_start = s;
			longest_length = len;

			std::cout << longest_start << std::endl;
			std::cout << longest_length << std::endl <<std::endl;
		}
	}
	
}
