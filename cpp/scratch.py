import numpy as np
import matplotlib.pyplot as plt

def hyperbola(n, numpoints=50):
    x_hyperbola = np.linspace(2*n, n*(n+1), num=numpoints)
    y_hyperbola = n*x_hyperbola / (x_hyperbola - n)
    return x_hyperbola, y_hyperbola



if __name__ == "__main__":
    n = 1260
    xh, yh = hyperbola(n)

    y = []
    x = []
    for j in range(n):
        y.append(2*n - j)
        x.append(n*(2+j))

    intpairs = np.zeros((2, len(x)*len(y)))
    l = 0
    q = 0
    for xk in x:
        for j, yj in enumerate(y):
            intpairs[0, l] = xk
            intpairs[1, l] = yj
            l +=1

            if(xk*yj == n*(xk+yj)):
                q+=1
                k = ((2*n - j) / (n-j)) - 2
                s = "{:>2}: n={:>4}, j={:>4}, k={:>4}, xk={:>4}, yj={:>4}".format(q, n, j, k, xk, yj)
                print(s)


    plt.plot(xh, yh)
    plt.plot(intpairs[0], intpairs[1], 'o')
    plt.show()
