#include "prob_067triangle.h"
int main(void)
{
	int32_t j, k;
	//int32_t* this_row;
	//int32_t* next_row;
	
	//start in second to last row
	for(k=WIDTH-2; k>=0; k--)
	{
		/*
		this_row = triangle[k];
		next_row = triangle[k+1];
		*/
		//next row always has one more element than this row

		for(j=0; j<=k; j++)
		{
			triangle[k][j] += std::max(triangle[k+1][j], triangle[k+1][j+1]);
		}
	}

	printf("%u\n", triangle[0][0]);
}
