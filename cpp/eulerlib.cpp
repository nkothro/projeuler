#include <stdint.h>

#include "eulerlib.h"

uint16_t integerPow(const uint32_t base, const uint32_t exponent)
{
	uint16_t retval = 1;
	for(uint32_t j=1; j<=exponent; j++)
	{
		retval *= base;
	}
	return retval;
}

uint16_t getDecimalDigit(uint16_t bigNum, uint32_t k)
/* get the kth digit of bigNum as its represented in decimal notation */
{
	uint16_t powerOfTen, lower, relevant, shrunk;

	powerOfTen = integerPow(10, k);
	relevant = bigNum % powerOfTen;
	shrunk = (uint16_t)((bigNum - relevant) / powerOfTen) % 10;

	return shrunk;
}

uint1024_t grabShiftLastDigit(uint1024_t& bignum, uint8_t base)
{
	uint1024_t digit;
	digit = bignum % base;
	bignum -= digit;
	bignum /= base;

	return digit;
}
