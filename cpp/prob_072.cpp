#include <iostream>
#include <cmath>
#include <algorithm>
#include <stdint.h>

//#include "fraction.h"

#define LARGEST_DENOMINATOR (1000000)

#define PRINTS
//#undef PRINTS

/*
 * It is possible to generate all pairs of coprime numbers.
 * Start at (m,n) which is a known coprime pair with m > n.
 * Each of the three following are also a coprime pair:
 *   (2m - n, m)
 *   (2m + n, m)
 *   (m + 2n, n)
 * Starting with (m, n) = (2, 1) produces one of two ternary trees.
 * Starting with (m, n) = (3, 1) produces the other tree. Together 
 * they generate all coprime pairs
 * 
 * source:
 * https://en.wikipedia.org/wiki/Coprime_integers#Generating_all_coprime_pairs
 *
 **** PROBLEM ****
 * Using this method and the below funciton requires too many recursive calls
 * and produced a segfault. Damn!
 */

void next_coprime_pairs(const uint32_t m, const uint32_t n, uint32_t limit, uint64_t &counter)
{

#ifdef PRINTS
	std::cout << counter << ": ("  << m << ", " << n << ")" << std::endl;	
#endif //PRINTS

	if(m > limit) { return; }

	// First child: (2m - n, m)
	++counter;
	next_coprime_pairs(m + m - n, m, limit, counter);

	// Second child: (2m + n, m)
	++counter;
	next_coprime_pairs(m + m + n, m, limit, counter);

	// Third chile: (m + 2n, n)
	++counter;
	next_coprime_pairs(m + n + n, n, limit, counter);
}

/* The farey tree is a way of enmerating the rationals between 0 and 1.
 * Generating all rationals in a "row" of the farey tree is straightfoward, but
 * still takes quite a while
 */
uint32_t farey(const uint32_t n)
{
	uint32_t new_a, new_b, new_c, new_d;

	uint32_t a, b, c, d, k, ct;
	a = 0;
	b = 1;
	c = 1;
	d = n;
	ct = 0;

#ifdef PRINTS
	//std::cout << ct << ": " << a << "/" << b << std::endl;	
#endif //PRINT
	while(c<=n)
	{
		k = (uint32_t) floor((n + b) / d);

		new_a = c;
		new_b = d;
		new_c = k*c - a;
		new_d = k*d - b;

		a = new_a;
		b = new_b;
		c = new_c;
		d = new_d;

		ct++;
#ifdef PRINTS
		//std::cout << ct << ": " << a << "/" << b;//<< std::endl;	
		if(!(ct % 100 == 0)) { std::cout << ct << std::endl; }
#endif //PRINTS
	}
	ct--;
	return ct;
}

uint32_t farey_length()
{
}




int main(void)
{
	uint32_t limit = LARGEST_DENOMINATOR;
	uint64_t ct = 0;

	ct = farey(limit);
	std::cout << "Farey Length is " << ct << std::endl;
}
