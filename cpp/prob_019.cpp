#include <stdint.h>
#include <cstdio>

/*
 *  I am doing 0 based indexing for all time periods other than years
 *		January is month 0
 *		The first day of the month is day 0
 *		The first day of the week is Monday, which is day 0
 */

enum Month  {
	January = 0,
	February,
	March,
	April,
	May,
	June,
	July,
	August,
	September,
	October,
	November,
	December,
	MONTH_COUNT	
};

enum Weekday {
	Monday = 0,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday,
	WEEKDAY_COUNT,
};

bool isLeapYear(int year)
{
	if((year % 400)==0) {return true;} // if a year is divisible by 400, it is a leap year
	else if((year % 100)==0) { return false;} // century years are not leap years, unless its a multiple of 400
	else if((year % 4)==0) { return true; }
	else {return false;}
}

uint32_t monthLength(int year, int month)
{
	uint32_t l;
	switch(month)
	{
		case January: l =  31; break;
		case February: l =  28 + isLeapYear(year); break;
		case March: l =  31; break;
		case April: l =  30; break;
		case May: l =  31; break;
		case June: l =  30; break;
		case July: l =  31; break;
		case August: l =  31; break;
		case September: l =  30; break;
		case October: l =  31; break;
		case November: l =  30; break;
		case December: l =  31; break;
	}
	// account for 0 based indexing
	l = l-1;
	return l;
}

int main(void)
{
	uint32_t total_days = 0;
	uint32_t day = 0;
	uint32_t month = January;
	uint32_t year = 1900;
	uint32_t weekday = Monday;

	uint32_t count_sunday_is_first = 0;

	while(year < 2001)
	{
		total_days++;
		day++;

		//weekday always increments
		weekday = (weekday + 1) % WEEKDAY_COUNT;

		//if today is the last of the month, tomorrow is the first of the month
		if(day == monthLength(year, month))
		{
			day = 0;
			month = (month + 1) % MONTH_COUNT;

			// the main thing we're counting
			if(weekday == Sunday) { count_sunday_is_first++; }

			//if we just got into january, need to increment the year
			if(month == January) { year++;}
		}
	}

	printf("There were %u Sundays on the first of the month in the 20th century\n", count_sunday_is_first);
}

