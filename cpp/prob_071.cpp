#include <iostream>
#include <cmath>
#include <algorithm>
#include <stdint.h>

#include "fraction.h"

#define LARGEST_DENOMINATOR (1000000)

int main(void)
{
	uint32_t n, d, j;
	Fraction comp(3, 7);
	Fraction best(0, 1);

	j = 0;
	for(d=4; d<=LARGEST_DENOMINATOR; d++)
	{
		n = (uint32_t)ceil(comp.numerator * d / comp.denominator) + 1;
		Fraction r(n, d);

		while(r.isGreaterThan(best) && n > 0)
		{
			if(r.isLessThan(comp) && r.isReduced())
			{
				best = r;

				std::cout << best.numerator << "/" << best.denominator << "  ";
				if(j++%10==0) { std::cout<<std::endl; }
			}
			n--;
			r =  Fraction(n, d);
		}
	}
}
