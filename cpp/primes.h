#ifndef PRIMES_H
#define PRIMES_H

#include <stdint.h>
#include <vector>
#include <algorithm>
#include <math.h>

template  <typename T>
std::vector<T> SieveOfAtkin(const T limit)
{
	// Initialise the sieve array with false values
	bool* sieve = new bool[limit];
	for (T i = 0; i < limit; i++)
		sieve[i] = false;

	/* Mark siev[n] is true if one
	   of the following is true:
	a) n = (4*x*x)+(y*y) has odd number of
	   solutions, i.e., there exist
	   odd number of distinct pairs (x, y)
	   that satisfy the equation and
		n % 12 = 1 or n % 12 = 5.
	b) n = (3*x*x)+(y*y) has odd number of
	   solutions and n % 12 = 7
	c) n = (3*x*x)-(y*y) has odd number of
	   solutions, x > y and n % 12 = 11 */
	for (T x = 1; x * x < limit; x++) {
		for (T y = 1; y * y < limit; y++) {

			// Main part of Sieve of Atkin
			T n = (4 * x * x) + (y * y);
			if (n <= limit && (n % 12 == 1 || n % 12 == 5))
				sieve[n] ^= true;

			n = (3 * x * x) + (y * y);
			if (n <= limit && n % 12 == 7)
				sieve[n] ^= true;

			n = (3 * x * x) - (y * y);
			if (x > y && n <= limit && n % 12 == 11)
				sieve[n] ^= true;
		}
	}

	// Mark all multiples of squares as non-prime
	for (T r = 5; r * r < limit; r++) {
		if (sieve[r]) {
			for (T i = r * r; i < limit; i += r * r)
				sieve[i] = false;
		}
	}

	std::vector<T> primes;
	primes.push_back(2);
	primes.push_back(3);
	for (T a = 5; a < limit; a++)
	{
		if (sieve[a]) { primes.push_back(a); }
	}
	delete sieve;
	return primes;
}

template <typename T>
std::vector<T> primeFactorization(T number, const std::vector<T>& primes)
{
	/* 81 = 3^4, and so primes vector can have a number less than 81 as its
	 *       largest element
	 *       don't want this block
	if(number > primes.back())
	{
		std::vector<T> factorization (1);
		factorization[0] = 0;
		return factorization;
	}
	*/

	T j, s;
	//check if number is prime
	if(std::binary_search(primes.begin(), primes.end(), number))
	{
		auto lower = std::lower_bound(primes.begin(), primes.end(), number);
		s = std::distance(primes.begin(), lower);
		std::vector<T> factorization(s+1);
		factorization[s] = 1; //last el

		return factorization;
	}

	// largest prime factor is 2, or is some number less than the number/2
	T upper = number / 2;
	s = std::distance(primes.begin(), std::upper_bound(primes.begin(), primes.end(), upper)) +1; 
	std::vector<T> factorization (s);
	for(j=0; j<s; j++)
	{
		while(!(number % primes[j]))
		{
			number /= primes[j];
			factorization[j] += 1;
		}
	}

	return factorization;
}

template <typename T>
T numberOfDivisors(T number, const std::vector<T>& primes)
{
	std::vector<T> pf;
	pf = primeFactorization((T) number, primes);
	T total = 1;
	for(T j = 0; j < pf.size(); j++)
	{
		if(pf[j] > 0)
		{
			total *= 1+pf[j];
		}
	}
	return total;
}


#endif //PRIMES_H
