#include <boost/multiprecision/cpp_int.hpp> 
#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <cmath>

using namespace boost::multiprecision;

/* There are always going to be 20 "down" movements and 20 "accross" movements,
 * for a total of 40 movements. The number of ways to arrange it is a
 * multinomial: 40 choose(20,20), for lack of better notation.
 * 
 */




int main(void)
{
	uint1024_t steps = 40;
	uint1024_t downs = 20;
	uint1024_t accross = 20;

	std::cout << comb(steps, downs) << std::endl;

	uint1024_t n, k1, k2, res;
	n = 40;
	k1 = 20;
	k2 = 20;
	res = factorial(n) / (factorial(k1) * factorial(k2));

	std::cout << res << std::endl;
}
