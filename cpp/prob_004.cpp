#include <stdio.h>
#include <string>
#include <stdint.h>

bool check_palindrome(uint32_t num)
{
	std::string strval = std::to_string(num);
	int len = strval.length() - 1;
	int middle = 0;
	int i;

	if((len % 2) == 0) { middle = (len / 2); }
	else { middle = ((len + 1) / 2); }

	for(i=0; i<=middle; i++)
	{
		if(strval[i] != strval[len-i]) { return false; }
	}
	return true;
}

int main(void)
{
	uint32_t i, j, upper, lower, prod, maxProd;

	upper = 999;	
	lower = 100;
	maxProd = 1;

	for(i=upper; i>=lower; i--)
	{
		for(j=upper; j>=i-1; j--)
		{
			prod = i * j;
			if(check_palindrome(prod))
			{
				if(prod > maxProd)
				{
					maxProd = prod;
					printf("%8lu is %4lu * %4lu\n", prod, i, j);
				}
			}
		}
	}
}
