#include <iostream>
#include <cstdint>
#include <vector>
#include <list>
#include <algorithm>

#include "primes.h"
#include "eulerlib.h"


bool evenlyDivisible(uint32_t number, uint32_t maxDivisor)
{
	uint32_t j;
	for(j=1; j<=maxDivisor; j++)
	{
		if (!(number % j)) {return false; }
	}
	return true;
}


int main(void)
{
	uint32_t s, psize, maxFactor = 20;
	std::vector<uint32_t> p, f, r;

	p = SieveOfAtkin(maxFactor);
	psize = p.size();
	r.resize(psize);
	
	for(uint32_t factor=2; factor<=maxFactor; factor++)
	{
		f = primeFactorization(factor, p);
		for(s=0; s < psize; s++)
		{
			r[s] = std::max(r[s], f[s]);
		}
	}

	uint32_t prod = 1;
	for(s=0; s < psize; s++)
	{
		prod *= integerPow(p[s], r[s]);
	}
	
	for(s=0; s < psize; s++)
	{
		std::cout << p[s] << " " << r[s] << std::endl;
	}
	std::cout << prod << std::endl;


}
	
