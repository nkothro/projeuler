#include <vector>
#include <stdint.h>
#include <iostream>
#include <algorithm>

#include "primes.h"
#include "eulerlib.h"

int main(void)
{
	std::vector<uint32_t> pf, p, z;

	std::vector<std::vector<uint32_t>> big;
	uint32_t a, b, k, ct;

	k = 100;
	p = SieveOfAtkin(k); //largest base in problem is 100

	//smallest number wil be 2^2 = 4
	/*
	a = 4;
	pf = primeFactorization(a, p);
	big.push_back(pf);
	*/
	ct = 1;

	//now, cycle through a and b for values 2 to 100
	for(a=2; a<=100; a++)
	{
		pf = primeFactorization(a, p);
		pf.resize(p.size());
		for(b=2; b<=100; b++)
		{
			z = pf;
			for(k=0; k<pf.size(); k++)
			{
				z[k] = pf[k]*b;
			}
			if(!(std::binary_search(big.begin(), big.end(), z)))
			{
				//either lower or upper will, since no element equal to in vector yet
				auto lower = std::lower_bound(big.begin(), big.end(), z);
				//auto upper = std::upper_bound(big.begin(), big.end(), z);
				big.insert(lower, z);
				ct++;
			}
		}
	}

	std::cout << big.size() << std::endl;
}
