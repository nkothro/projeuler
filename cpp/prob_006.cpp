// #include <stdint.h>
#include <stdio.h>

#define UPPERLIMIT (100)

int main(void)
{
	unsigned long long int sum_of_sqs, sq_of_sum;
	
	for(int j =0; j<= UPPERLIMIT; j++)
	{
		sum_of_sqs += j*j;
		sq_of_sum += j;
	}

	sq_of_sum *= sq_of_sum;
	printf("Square of sum of first %d natural numbers: %llu\n", UPPERLIMIT, sq_of_sum);

	printf("Sum of Squares of first %d natural numbers: %llu", UPPERLIMIT, sum_of_sqs);
}

