#include <algorithm>
#include <stdint.h>

class Fraction {
	public:
		uint64_t numerator, denominator;
		Fraction(int num, int denom) { numerator = num; denominator = denom; };

	public:
		bool isLessThan(Fraction other);
		bool isGreaterThan(Fraction other);
		bool isEqualTo(Fraction other);
		bool isReduced();
}; //Fraction
