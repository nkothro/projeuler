#include <iostream>
#include <stdint.h>
#include <vector>

#include "primes.h"

int main(void)
{
	std::vector<uint64_t> plist;
	uint64_t limit = 1000000;
	plist = SieveOfAtkin(limit);
	std::cout << plist[0] << std::endl;
	std::cout << plist[10000] << std::endl;

}
