#include <stdint.h>
#include <iostream>
int main(void)
{
	uint32_t maxValue = 4000000;
	uint32_t f = 0;
	uint32_t s = 0;
	uint32_t fibs[2];
	uint8_t j = 0;

	fibs[0] = 0;
	fibs[1] = 1;

	while((fibs[0] < maxValue) && (fibs[1] < maxValue))
	{
		f = fibs[0] + fibs[1];
		if((f % 2 == 0) && (f<= maxValue)) { s += f; }
		fibs[j % 2] = f;
		j++;
	}

	std::cout << s << std::endl;
}
