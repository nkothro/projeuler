#include <boost/multiprecision/cpp_int.hpp> 
#include <iostream>
#include <string>

using namespace boost::multiprecision;

uint1024_t integerPow(const uint32_t base, const uint32_t exponent)
{
	uint1024_t retval = 1;
	for(uint32_t j=1; j<=exponent; j++)
	{
		retval *= base;
	}
	return retval;
}

uint1024_t getDecimalDigit(uint1024_t bigNum, uint32_t n)
/* get the nth digit of bigNum as its represented in decimal notation */
{
	uint1024_t powerOfTen, lower, relevant, shrunk;

	powerOfTen = integerPow(10, n);
	relevant = bigNum % powerOfTen;
	shrunk = (uint1024_t)((bigNum - relevant) / powerOfTen) % 10;

	return shrunk;
}

uint1024_t grabShiftLastDigit(uint1024_t& bignum, uint8_t base)
{
	uint1024_t digit;
	digit = bignum % base;
	bignum -= digit;
	bignum /= base;

	return digit;
}

int main(void)
{
	uint32_t limit = 1000;
	uint1024_t sum = 0;
	uint1024_t num = integerPow(2, limit);

	for(uint32_t j=0; j<limit+10; j++)
	{
		sum += grabShiftLastDigit(num, 10);
	}
	std::cout << sum << std::endl;

}
