#include <stdint.h>

#include "eulerlib.h"
#include "primes.h"

template<typename T>
void incrementExponentsVector(const std::vector<T> original_exp, std::vector<T>& exponents)
{
	if(original_exp.size() != exponents.size()) { return; }
	T j = original_exp.size();
	j--; //derement to get to last element

	// increment last element of "running" array
	exponents[j]++;
	while((exponents[j] > original_exp[j]) && (j > 0))
	{
		exponents[j-1] += 1; //j must > 0; else access memoy not in vector
		exponents[j] = 0;

		if(j > 0) { j--; }
	}
	return;
}


template<typename T>
T aliquotSum(T number, const std::vector<T> primes)
{
	//Proper divisors means not the number itself

	/* first, convert exponents vector and primes vector to two vectors that
	 * contain only the distict primes in the prime factorization
	 *  eg: 20 = 2^2 * 5^1, don't need to include 3, 7, 11 ...
	 */
	T j = 0;
	std::vector<T> pr, ex, s, origpf;

	T number_of_divisors = 1;
	origpf = primeFactorization(number, primes);

	for(j=0; j<origpf.size(); j++)
	{
		if(origpf[j]!=0)
		{
			pr.push_back(primes[j]);
			ex.push_back(origpf[j]);
			number_of_divisors *= (origpf[j]) + 1;
		}
	}

	T len = pr.size();
	T prod, k;
	T sum = 0;

	s.assign(len, 0);
	for(j = 0; j < number_of_divisors-1; j++)
	{
		prod = 1;
		for(k = 0; k < len; k++)
		{
			prod *= integerPow(pr[k], s[k]);
		}
		sum += prod;
		incrementExponentsVector(ex, s);
	}

	return sum;
}

template<typename T>
bool testSumOfTwoAbundants(T num, const std::vector<T>& abunds)
{
	uint32_t k, testnum;
	k=0;
	while(abunds[k] < num)
	{
		testnum = num - abunds[k];
		if(std::binary_search(abunds.begin(), abunds.end(), testnum))
		{
			printf("%u = %u + %u\n", num, abunds[k], testnum);
			return true;
		}
		else
		{
			k++;
		}
	}
	return false;
}

int main(void)
{
	std::vector<uint32_t> abundants, primes; 
	uint32_t s, j, k, maxnum;
	std::cout << "PRIMES DONE" << std::endl;

	//generate primes
	maxnum = 28123;
	primes = SieveOfAtkin(maxnum);

	//generate list of abundant numbers
	k=0;
	for(j=2; j<maxnum; j++)
	{
		if((aliquotSum(j, primes) > j))
		{
			abundants.push_back(j);
			//printf("%6u", j);
			//if(!(k % 10)){printf("\n");}
			//k++;
		}
	}

	//loop through numbers less than the max
	// problem states that smallest sum of two abundant numbers is 24
	s =1;
	for(j=2; j<maxnum; j++)
	{
		if(!testSumOfTwoAbundants(j, abundants))
		{
			s += j;
			//printf("%u cannot be writen as sum of two abundants\n", j);
		}	
	}
	printf("SUM IS: %llu", s);
}
