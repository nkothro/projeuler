"""
Some number m is multiplied by each element of the tuple (1...n).

UPPER BOUND
For a given n, the max m is determined by the number of digits.
    - n=2, m must be less than 5 x 10^5.
    - n=3, m must be less than 333, because 333*333 is 999, and any larger
      would introduce another digit, and the result of the concatenatioin must
      be 9 digits
    - n=4, m must be less than 250, else another digit is introduced.
    - n=5, m must be less than 20, else another digit is introduced.
    - and so on, m must be less than 10^10//n

"""

def tuple_concatenate(
    num: int,
    max_multiplier: int
)-> int:
    prods = [num]
    for j in range(2, max_multiplier+1):
        prods.append(j*num)
    prods.sort()

    num_string = ''
    for n in prods:
        num_string += str(n)

    return int(num_string)

if __name__ == '__main__':

    max_concatenated_value = 0
    max_tuple_val = 0
    max_input_num = 0

    for tup_max in range(2, 10):
        upper_bound = 10**(tup_max +1) // 
        

